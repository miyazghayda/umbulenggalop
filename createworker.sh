#!/bin/sh
cd ~

echo "LOCALE"
sudo locale-gen "en_US.UTF-8"
sudo update-locale LC_ALL="en_US.UTF-8"
export LC_ALL=en_US.UTF-8

sudo apt-get install -y python-software-properties nginx
sudo add-apt-repository ppa:ondrej/php
sudo add-apt-repository ppa:nijel/phpmyadmin
sudo apt-get update

echo "INSTALL WGET"
sudo apt-get install -y wget htop locales ffmpeg

echo "INSTALL UFW"
sudo apt-get install ufw
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow ssh
sudo ufw allow http
sudo ufw allow https
sudo ufw allow 8080
sudo ufw allow 9080
sudo ufw allow 6969
sudo ufw allow 3306
sudo ufw enable

echo "INSTALL PHP"
sudo apt-get update
sudo apt-get install -y php-pear php7.2 php7.2-cli php7.2-common php7.2-mysql php7.2-mbstring php7.2-curl php7.2-zip php7.2-fpm php7.2-gd php7.2-xml php7.2-dom php7.2-zip php7.2-intl
php7.2-bcmath php7.2-amqp

echo "INSTALL GIT"
sudo apt-get install git curl unzip

echo "INSTALL COMPOSER"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
php -r "unlink('composer-setup.php');"
sudo apt-get install composer

sudo apt-get install apt-transport-https ca-certificates curl software-properties-common

chmod -R 777 /var/www/html/automateit

echo "INSTALL TMUX"
sudo apt-get install -y automake build-essential pkg-config libevent-dev libncurses5-dev
rm -fr /tmp/tmux
git clone https://github.com/tmux/tmux.git /tmp/tmux
cd /tmp/tmux
sh autogen.sh
./configure && make
sudo make install
cd -
rm -fr /tmp/tmux
wget https://gist.githubusercontent.com/aansubarkah/6f5afb20d972232501b1/raw/b4059067db4acecf8014a9c71166785da5412706/tmux.conf -O ~/.tmux.conf

echo "INSTALL RUBY"
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt-get update
sudo apt-get install -y git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev nodejs yarn
cd ~
wget http://ftp.ruby-lang.org/pub/ruby/2.5/ruby-2.5.1.tar.gz
tar -xzvf ruby-2.5.1.tar.gz
cd ruby-2.5.1/
./configure
make
sudo make install
ruby -v

sudo apt-get install rbenv
sudo gem install bundler
sudo gem install teamocil
mkdir ~/.teamocil
wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/automateit.yml -O ~/.teamocil/automateit.yml

echo "REBOOT"
sudo reboot
