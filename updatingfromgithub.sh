#!/bin/sh
cd /var/www/html/automateit
rm -rf /var/www/html/automateit/umbulenggalop
git clone https://gitlab.com/miyazghayda/umbulenggalop
cp -R umbulenggalop/background/* /var/www/html/automateit/background
cp -R umbulenggalop/src/* /var/www/html/automateit/src
cp -R umbulenggalop/webroot/css/* /var/www/html/automateit/webroot/css
cp -R umbulenggalop/webroot/js/* /var/www/html/automateit/webroot/js
cp umbulenggalop/*.* /var/www/html/automateit