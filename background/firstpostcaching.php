<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
require_once $rootPath . 'background/logging.php';

use Monolog\Logger;
use Medoo\Medoo;

$dbhost = 'localhost';
$dbname = 'automateit2';
$dbuser = 'user';
$dbpass = 'jayapura';
$dbtable = 'logs';
$dbcharset = 'utf8mb4';

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $dbname,
    'server' => $dbhost,
    'username' => $dbuser,
    'password' => $dbpass,
    'charset' => $dbcharset,
]);

$log = new Logger('scrapehashtagposts');
$log->pushHandler($mySQLHandler);
$account_id = 1; // mandatory
$scriptid = 2; // mandatory, scrapehashtagposts

$cachePath = $rootPath . 'tmp/cache/';
// Delete folder if exists
echo $cachePath . 'posts' . PHP_EOL;

if (is_dir($cachePath . 'posts')) {
    $files = glob($cachePath . 'posts/*');
    foreach ($files as $file) {
        if (is_file($file)) unlink($file);
    }
}

// Create cache path if not exists
if (!file_exists($cachePath . 'posts')) mkdir($cachePath . 'posts', 0777, true);

$cachePath = $cachePath . 'posts/';

$membersCount = $db->query('SELECT COUNT(id) AS total FROM posts WHERE active = 1')->fetchAll();
echo 'will processing ' . $membersCount[0]['total'] . ' rows' . PHP_EOL;

$queryLimit = 25000;// 25.000
$queryPages = ceil($membersCount[0]['total'] / $queryLimit);
for ($i = 1; $i <= $queryPages; $i++) {
    $queryOffset = ($i - 1) * $queryLimit;
    $members = $db->select('posts',
        [
            '[<]members' => ['member_id' => 'id'],
            '[<]locations' => ['location_id' => 'id']
        ],
        [
            'post' => [
                'posts.id(post_id)',
                'posts.pk(post_pk)',
                'posts.sourceid',
                'posts.typeid',
                'posts.caption',
                'posts.likes',
                'posts.comments',
                'posts.takenat',
                'posts.active'
            ],
            'member' => [
                'members.id(member_id)',
                'members.pk(member_pk)',
            ],
            'location' => [
                'locations.id(location_id)',
                'locations.pk(location_pk)',
            ]
        ],
        [
            'AND' => ['posts.id[>]' => 0],
            'ORDER' => ['posts.pk' => 'ASC'],
            'LIMIT' => [$queryOffset, $queryLimit]
        ]);
    $data = [];
    foreach ($members as $member) {
        $data[(int)$member['post']['post_pk']] = $member;
    }
    cache_set($members[0]['post']['post_pk'], $data);
    echo $i . '. caching ' . $members[0]['post']['post_pk'] . PHP_EOL;
}

// source https://medium.com/@dylanwenzlau/500x-faster-caching-than-redis-memcache-apc-in-php-hhvm-dcd26e8447ad
function cache_set($key, $val) {
    global $cachePath;
    $val = var_export($val, true);
    // HHVM fails at __set_state, so just use object cast for now
    $val = str_replace('stdClass::__set_state', '(object)', $val);
    // Write to temp file first to ensure atomicity
    $tmp = $cachePath . $key . '.' . uniqid('', true) . '.tmp';
    file_put_contents($tmp, '<?php $val = ' . $val . ';', LOCK_EX);
    rename($tmp, $cachePath . $key);
 }