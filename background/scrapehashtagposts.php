<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
require_once $rootPath . 'background/logging.php';

use Monolog\Logger;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;
use Medoo\Medoo;

$log = new Logger('scrapehashtagposts');
$log->pushHandler($mySQLHandler);
$account_id = 1; // mandatory
$scriptid = 2; // mandatory, scrapehashtagposts

// Get batch to process
$batch = 0;
if (isset($argv[1])) {
    $batch = $argv[1];
}

$account_id = 0;
$hashtag = '';
$maximumPost = 100;

if (isset($argv[1])) $account_id = (int)$argv[1];

if (isset($argv[2])) $hashtag = trim($argv[2]);

if (isset($argv[3])) $maximumPost = (int)$argv[3];

if ($account_id > 0 && strlen($hashtag) > 0 && $maximumPost > 0) {
    $log->info('Started', ['account_id' => $account_id, 'created' => date('Y-m-d H:i:s'), 'scriptid' => $scriptid]);

    $account = $db->select('accounts',
        [
            '[<]proxies' => ['proxy_id' => 'id'],
        ],
        [
            'account' => [
                'accounts.id(account_id)',
                'accounts.username',
                'accounts.password',
            ],
            'proxy' => [
                'proxies.id(proxy_id)',
                'proxies.name',
            ],
        ],
        [
            'AND' => [
                'accounts.id' => $account_id,
            ],
            'LIMIT' => 1
        ]
    );

    if (count($account) > 0) {
        $ig = new Instagram(false, false);

        $account = $account[0];

        if ($account['proxy']['proxy_id'] > 1) {
            $ig->setProxy($account['proxy']['name']);
        }

        try {
            $ig->login($account['account']['username'], $account['account']['password']);
            $log->info('Login with ' . $account['account']['username'], ['account_id' => $account_id, 'created' => date('Y-m-d H:i:s'), 'scriptid' => $scriptid]);

            $nextMediaIds = null;
            $ic = 0;
            $resPostIds = '';
            $resHashtagRelated = '';
            do {
                $responsesTop = $ig->hashtag->getSection($hashtag, 'top', $nextMediaIds);
                if ($responsesTop->getStatus() == 'ok') {
                    $countItem = count($responsesTop->getSections());
                    $items = $responsesTop->getSections();
                    // gather related hashtags
                    foreach ($items[0]->getLayoutContent()->getRelated() as $h) {
                        $resHashtagRelated = $h->getName() . ',' . $resHashtagRelated;
                    }

                    for ($i = 1; $i < $countItem; $i++) {
                        $medias = $items[$i]->getLayoutContent()->getMedias();
                        foreach ($medias as $item) {
                            // Insert Location
                            $location_id = 1;
                            if ($item->getMedia()->hasLocation() && $item->getMedia()->getLocation() !== null) {
                                $location_id = insertLocation($item->getMedia()->getLocation());
                            }

                            // Insert Member
                            $member_id = insertMember($item->getMedia()->getUser());

                            // Insert Post
                            $post_id = insertPost($item->getMedia(), $location_id, $member_id);
                            $resPostIds = $post_id . ',' . $resPostIds;

                            // Insert Wad
                            if ($post_id > 0 && $item->getMedia()->getMediaType() == 1) {// Photo
                                insertWad($item->getMedia()->getImageVersions2()->getCandidates()[0], $post_id, $item->getMedia()->getMediaType());
                            } elseif ($post_id > 0 && $item->getMedia()->getMediaType() == 2) {// Video
                                insertWad($item->getMedia()->getVideoVersions()[0], $post_id, $item->getMedia()->getMediaType());
                            } elseif ($post_id > 0 && $item->getMedia()->getMediaType() == 8) {// Carousel
                                insertWad($item->getMedia()->getCarouselMedia(), $post_id, $item->getMedia()->getMediaType());
                            }
                            //echo $ic . '. ' . $item->getMedia()->getPk() . PHP_EOL;
                            $ic++;
                        }
                    }
                }// .if status ok
                //$responsesTop->printPropertyDescriptions();
                //echo $responsesTop->getNextMediaIds()[0] . PHP_EOL;
                if (count($responsesTop->getNextMediaIds()) > 0) $nextMediaIds = $responsesTop->getNextMediaIds();
                if ($ic > $maximumPost) $nextMediaIds = null;
                //echo $nextMediaIds . PHP_EOL;
                sleep(rand(7, 12));
            } while ($nextMediaIds != null);
            //echo $ic . PHP_EOL;
            echo $resPostIds . '|' . $resHashtagRelated;

        } catch (\Exception $e) {
            $log->info($e->getMessage(), ['account_id' => $account_id, 'created' => date('Y-m-d H:i:s'), 'scriptid' => $scriptid]);
        }
    } else {
        echo 'No Account';
    }
} else {
    echo 'No Hashtag';
}

function insertMember($datum = null)
{
    global $db;
    $check = $db->select('members',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true],
        ['LIMIT' => 1]
    );

    ($datum->hasProfilePicUrl()) ? $profpicurl = $datum->getProfilePicUrl() : $profpicurl = '';

    if (count($check) > 0) {
        $db->update('members',
            [
                'fullname' => $datum->getFullName(),
                'description' => $datum->getBiography(),
                'profpicurl' => $profpicurl,
                'followers' => $datum->getFollowerCount(),
                'followings' => $datum->getFollowingCount(),
                'contents' => $datum->getMediaCount(),
                'profpicurlfixed' => true,
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'pk' => $datum->getPk(),
            ]
        );
        return $check[0]['id'];
    } else { // save member to db
        $db->insert('members',
            [
                'pk' => $datum->getPk(),
                'username' => $datum->getUsername(),
                'fullname' => $datum->getFullName(),
                'description' => $datum->getBiography(),
                'profpicurl' => $profpicurl,
                'followers' => $datum->getFollowerCount(),
                'followings' => $datum->getFollowingCount(),
                'contents' => $datum->getMediaCount(),
                'profpicurlfixed' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        );
        return $db->id();
    }
} // .function insertMember

function insertWad($datum = null, $post_id = 1, $typeid = 1)
{
    global $db;
    $newData = [
        'post_id' => $post_id,
        'typeid' => $typeid,
        'sequence' => 0,
    ];

    if ($typeid == 1 || $typeid == 2) { // Single photo or video
        $check = $db->select('wads',
            ['id'],
            ['post_id' => $post_id, 'url' => $datum->getUrl(), 'active' => true],
            ['LIMIT' => 1]
        );

        if (count($check) < 1) {
            $newData['url'] = $datum->getUrl();
            $newData['width'] = $datum->getWidth();
            $newData['height'] = $datum->getHeight();
            $db->insert('wads', $newData);
            return $db->id();
        } else {
            return $check[0]['id'];
        }
    } elseif ($typeid == 8) { // Carousel
        $sequence = 0;
        foreach ($datum as $d) {
            $newData['url'] = '';
            $newData['width'] = 0;
            $newData['height'] = 0;
            $newData['typeid'] = $d->getMediaType();
            if ($d->getMediaType() == 1) { // Photo
                $reap = $d->getImageVersions2()->getCandidates()[0];
            } elseif ($d->getMediaType() == 2) { // Video
                $reap = $d->getVideoVersions()[0];
            }
            $check = $db->select('wads',
                ['id'],
                ['post_id' => $post_id, 'url' => $reap->getUrl(), 'active' => true]
            );

            if (count($check) < 1) {
                $newData['url'] = $reap->getUrl();
                $newData['width'] = $reap->getWidth();
                $newData['height'] = $reap->getHeight();
                $newData['sequence'] = $sequence;
                $sequence++;
                $db->insert('wads', $newData);
            }
        }
    }
} // .insertWad

function insertPost($datum = null, $location_id = 1, $member_id = 1)
{
    global $db;
    $check = $db->select('posts',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else { // save location to db
        ($datum->hasCaption() && $datum->getCaption() !== null) ? $caption = $datum->getCaption()->getText() : $caption = '';
        //echo "New post with caption $caption";

        $db->insert('posts',
            [
                'pk' => $datum->getPk(),
                'sourceid' => $datum->getId(),
                'location_id' => $location_id,
                'member_id' => $member_id,
                'typeid' => $datum->getMediaType(),
                'caption' => $caption,
                'likes' => $datum->getLikeCount(),
                'comments' => $datum->getCommentCount(),
                'takenat' => $datum->getTakenAt(),
            ]
        );
        return $db->id();
    }
} // .function insertPost

function insertLocation($location = null)
{
    global $db;
    $check = $db->select('locations',
        ['id'],
        ['pk' => $location->getPk(), 'active' => true],
        ['LIMIT' => 1]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else { // save location to db
        //echo "New location named " . $location->getName() . "\n";
        ($location->hasFacebookPlacesId()) ? $fbplacesid = $location->getFacebookPlacesId() : $fbplacesid = 0;

        $db->insert('locations',
            [
                'pk' => $location->getPk(),
                'lat' => $location->getLat(),
                'lng' => $location->getLng(),
                'shortname' => $location->getShortName(),
                'name' => $location->getName(),
                'address' => $location->getAddress(),
                'fbplacesid' => $fbplacesid,
            ]
        );
        return $db->id();
    }
} // .function insertLocation
