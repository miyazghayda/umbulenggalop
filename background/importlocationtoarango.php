<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
require_once $rootPath . 'background/logging.php';

use Monolog\Logger;
use Medoo\Medoo;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use ArangoDBClient\Connection;
use ArangoDBClient\ConnectionOptions;
use ArangoDBClient\Collection;
use ArangoDBClient\CollectionHandler;
use ArangoDBClient\Document;
use ArangoDBClient\DocumentHandler;
use ArangoDBClient\UpdatePolicy;
use ArangoDBClient\Statement;

$log = new Logger('duplicatingposts');
$log->pushHandler($mySQLHandler);
$account_id = 1; // mandatory
$scriptid = 1; // mandatory, duplicatingposts

$dbhost = 'localhost';
$dbname = 'automateit2';
$dbuser = 'user';
$dbpass = 'jayapura';
$dbtable = 'logs';
$dbcharset = 'utf8mb4';
$arangoHost = '206.189.46.73';

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $dbname,
    'server' => $dbhost,
    'username' => $dbuser,
    'password' => $dbpass,
    'charset' => $dbcharset,
]);

$connectionOptions =array(
    // server endpoint to connect to
    ConnectionOptions::OPTION_ENDPOINT => 'tcp://' . $arangoHost . ':8529',
    // authorization type to use (currently supported: 'Basic')
    ConnectionOptions::OPTION_AUTH_TYPE => 'Basic',
    // user for basic authorization
    ConnectionOptions::OPTION_AUTH_USER => 'user',
    // password for basic authorization
    ConnectionOptions::OPTION_AUTH_PASSWD => 'jayapura',
    // connection persistence on server. can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
    ConnectionOptions::OPTION_CONNECTION => 'Close',
    // connect timeout in seconds
    ConnectionOptions::OPTION_TIMEOUT => 3,
    // whether or not to reconnect when a keep-alive connection has timed out on server
    ConnectionOptions::OPTION_RECONNECT => true,
    // optionally create new collections when inserting documents
    ConnectionOptions::OPTION_CREATE => true,
    // optionally create new collections when inserting documents
    ConnectionOptions::OPTION_UPDATE_POLICY => UpdatePolicy::LAST,
    ConnectionOptions::OPTION_DATABASE => 'automateit',
);
 
// open connection
$connection = new Connection($connectionOptions);

// create a new collection
$collectionName = 'locations';
$collection = new Collection($collectionName);
$collectionHandler = new CollectionHandler($connection);
 
if (!$collectionHandler->has($collectionName)) {
    $collectionId = $collectionHandler->create($collection);
}

$documentHandler = new DocumentHandler($connection);

$membersCount = $db->query('SELECT COUNT(id) AS total FROM locations WHERE active = 1')->fetchAll();
echo 'will processing ' . $membersCount[0]['total'] . ' rows' . PHP_EOL;

$queryLimit = 25000;// 25.000
$queryPages = ceil($membersCount[0]['total'] / $queryLimit);
$counter = 1;
for ($i = 1; $i <= $queryPages; $i++) {
    $queryOffset = ($i - 1) * $queryLimit;
    $members = $db->select('locations',
        [
            'id', 'pk', 'fbplacesid', 'lat', 'lng', 'address', 'name', 'shortname', 'active'
        ],
        [
            'id[>]' => 0,
            'ORDER' => ['id' => 'ASC'],
            'LIMIT' => [$queryOffset, $queryLimit],
        ]);
    foreach ($members as $member) {
        $member['address'] = addslashes($member['address']);
        $member['name'] = addslashes($member['name']);
        $member['shortname'] = addslashes($member['shortname']);

        $query = 'UPSERT { _key: "' . $member['fbplacesid'] . '"} ';
        $query = $query . 'INSERT { ';
        $query = $query . '_key: "' . $member['fbplacesid'] . '", ';
        $query = $query . 'fbplacesid: "' . $member['fbplacesid'] . '", ';
        $query = $query . 'pk: "' . $member['pk'] . '", ';
        $query = $query . 'name: "' . $member['name'] . '", ';
        $query = $query . 'shortname: "' . $member['shortname'] . '", ';
        $query = $query . 'address: "' . $member['address'] . '", ';
        $query = $query . 'lat: ' . $member['lat'] . ', ';
        $query = $query . 'lng: ' . $member['lng'] . ', ';
        $query = $query . 'active: ' . $member['active'];
        $query = $query . ' } ';

        $query = $query . 'UPDATE { ';
        $query = $query . 'name: "' . $member['name'] . '", ';
        $query = $query . 'shortname: "' . $member['shortname'] . '", ';
        $query = $query . 'address: "' . $member['address'] . '", ';
        $query = $query . 'lat: ' . $member['lat'] . ', ';
        $query = $query . 'lng: ' . $member['lng'] . ', ';
        $query = $query . 'active: ' . $member['active'];
        $query = $query . ' } IN locations';
        
        echo $query . PHP_EOL;

        $statement = new Statement(
            $connection,
            [
                'query' => $query,
                'count' => true,
                'batchSize' => 1,
                'sanitize' => true
            ]
            );
        $cursor = $statement->execute();

        echo $counter . '. ' . $member['name'] . PHP_EOL;
        $counter++;
    }
}