<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
//require_once $rootPath . 'background/logging.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use ArangoDBClient\Connection;
use ArangoDBClient\ConnectionOptions;
use ArangoDBClient\Collection;
use ArangoDBClient\CollectionHandler;
use ArangoDBClient\Document;
use ArangoDBClient\DocumentHandler;
use ArangoDBClient\UpdatePolicy;
use ArangoDBClient\Statement;

$motherServer = 'tanpa.download';
$motherDbUser = 'user';
$motherDbPass = 'jayapura';

$connection = new AMQPStreamConnection($motherServer, 5672, $motherDbUser, $motherDbPass);
$channel = $connection->channel();

$channel->queue_declare('member_self', false, true, false, false);

$callback = function ($msg) {
    $data = json_decode($msg->body, true);
    echo $data['username'] . PHP_EOL;

    insertMember($data);
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};// callback

$channel->basic_qos(null, 1, null);
//$channel->basic_consume('newfollower', '', false, true, false, false, $callback);// no acknowledgment
$channel->basic_consume('member_self', '', false, false, false, false, $callback);

while (count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();

function insertMember($datum = null)
{
    if ($datum != null) {
        $arangoHost = '206.189.46.73';
        $dbuser = 'user';
        $dbpass = 'jayapura';
        // ArangoDb section
        $connectionOptions = [
            // server endpoint to connect to
            ConnectionOptions::OPTION_ENDPOINT => 'tcp://' . $arangoHost . ':8529',
            // authorization type to use (currently supported: 'Basic')
            ConnectionOptions::OPTION_AUTH_TYPE => 'Basic',
            // user for basic authorization
            ConnectionOptions::OPTION_AUTH_USER => $dbuser,
            // password for basic authorization
            ConnectionOptions::OPTION_AUTH_PASSWD => $dbpass,
            // connection persistence on server. can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
            ConnectionOptions::OPTION_CONNECTION => 'Close',
            // connect timeout in seconds
            ConnectionOptions::OPTION_TIMEOUT => 60,
            // whether or not to reconnect when a keep-alive connection has timed out on server
            ConnectionOptions::OPTION_RECONNECT => true,
            // optionally create new collections when inserting documents
            ConnectionOptions::OPTION_CREATE => true,
            // optionally create new collections when inserting documents
            ConnectionOptions::OPTION_UPDATE_POLICY => UpdatePolicy::LAST,
            ConnectionOptions::OPTION_DATABASE => 'automateit',
        ];
        
        // open connection
        $connection = new Connection($connectionOptions);
        
        /*$data = [
                    'pk' => $d->getPk(),
                    'username' => $d->getUsername(),
                    'fullname' => $d->getFullName(),
                    'description' => $d->getBiography(),
                    'profpicurl' => $d->getProfilePicUrl(),
                    'followers' => $d->getFollowerCount(),
                    'followings' => $d->getFollowingCount(),
                    'contents' => $d->getMediaCount(),
                    'closed' => $priv,
                    'profpicurlfixed' => 0,
                    'genderdetection' => 0,
                    'genderdetected' => 0,
                    'active' => 1
    ];*/

        $query = 'FOR d IN members FILTER d._key == "' . $datum['pk'] . '" LIMIT 1 RETURN {pk:d.pk}';
        $statement = new Statement(
            $connection,
            [
                'query' => $query,
                'count' => true,
                'batchSize' => 1,
                'sanitize' => true
            ]
        );
        
        $cursor = $statement->execute();
        if (count($cursor->getAll()) > 0) {
            $d = $cursor->getAll()[0];
            return ['pk' => $d->pk];
        } else {
            $q = 'UPSERT {_key:"' . $datum['pk'] . '"} ';
            $q = $q . 'INSERT {';
            $q = $q . '_key:"' . $datum['pk'] . '",';
            $q = $q . 'pk:"' . $datum['pk'] . '",';
            $q = $q . 'username:"' . addslashes($datum['username']) . '",';
            $q = $q . 'fullname:"' . addslashes($datum['fullname']) . '",';
            $q = $q . 'description:"' . addslashes($datum['description']) . '",';
            $q = $q . 'profpicurl:"' . $datum['profpicurl'] . '",';
            $q = $q . 'followers:' . $datum['followers'] . ',';
            $q = $q . 'followings:' . $datum['followings'] . ',';
            $q = $q . 'contents:' . $datum['contents'] . ',';
            $q = $q . 'closed:' . $datum['closed'] . ',';
            $q = $q . 'profpicurlfixed:0,';
            $q = $q . 'genderdetection:0,';
            $q = $q . 'genderdetected:0,';
            $q = $q . 'active: 1';
            $q = $q . '} UPDATE {';
            $q = $q . 'username:"' . addslashes($datum['username']) . '",';
            $q = $q . 'fullname:"' . addslashes($datum['fullname']) . '",';
            $q = $q . 'description:"' . addslashes($datum['description']) . '",';
            $q = $q . 'profpicurl:"' . $datum['profpicurl'] . '",';
            $q = $q . 'followers:' . $datum['followers'] . ',';
            $q = $q . 'followings:' . $datum['followings'] . ',';
            $q = $q . 'contents:' . $datum['contents'] . ',';
            $q = $q . 'closed:' . $datum['closed'] . '';
            $q = $q . '} IN members LET r = NEW RETURN {pk:r.pk}';
            $statement = new Statement(
                $connection,
                [
                    'query' => $q,
                    'count' => true,
                    'batchSize' => 1,
                    'sanitize' => true
                ]
            );

            $c = $statement->execute();
            if (count($c->getAll()) > 0) {
                $d = $c->getAll()[0];
                return ['pk' => $d->pk];
            } else {
                return false;
            }
        }
    }
}