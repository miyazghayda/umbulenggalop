<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
require_once $rootPath . 'background/logging.php';

use Monolog\Logger;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;
use Medoo\Medoo;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

//$log = new Logger('scrapehashtagposts');
//$log->pushHandler($mySQLHandler);
$account_id = 1; // mandatory
$scriptid = 2; // mandatory, scrapehashtagposts
$connection = new AMQPStreamConnection('localhost', 5672, 'radmin', 'radmin');
$channel = $connection->channel();
$channel->queue_declare('hellow', false, false, false, false);

$data = implode(' ', array_slice($argv, 1));
if (empty($data)) {
    $data = "Hello Again (2) World MQ!";
}
$msg = new AMQPMessage($data);

$channel->basic_publish($msg, '', 'hellow');

echo ' [x] Sent ' . $data . PHP_EOL;

$channel->close();
$connection->close();