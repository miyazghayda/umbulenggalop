<?php

/**
 * 
 */
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
//require_once $rootPath . 'background/logging.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use InstagramAPI\Instagram;
use InstagramAPI\Push;
use InstagramAPI\Push\Notification;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;
use ArangoDBClient\Connection;
use ArangoDBClient\ConnectionOptions;
use ArangoDBClient\Collection;
use ArangoDBClient\CollectionHandler;
use ArangoDBClient\Document;
use ArangoDBClient\DocumentHandler;
use ArangoDBClient\UpdatePolicy;
use ArangoDBClient\Statement;
use Medoo\Medoo;

$ig = null;
// Medoo (mysql) section
$dbhost = 'localhost';
$dbname = 'automateit';
$dbuser = 'user';
$dbpass = 'jayapura';
$dbtable = 'logs';
$dbcharset = 'utf8mb4';

$igSession = [
    'dbhost' => $dbhost,
    'dbname' => $dbname,
    'dbusername' => $dbuser,
    'dbpassword' => $dbpass
];

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $dbname,
    'server' => $dbhost,
    'username' => $dbuser,
    'password' => $dbpass,
    'charset' => $dbcharset,
]);

$motherServer = 'tanpa.download';
$motherDbUser = 'user';
$motherDbPass = 'jayapura';

$connection = new AMQPStreamConnection($motherServer, 5672, $motherDbUser, $motherDbPass);
$channel = $connection->channel();

$channel->queue_declare('newfollower', false, true, false, false);

$callback = function ($msg) {
    global $igSession;
    //global $db;
    //global $ig;

    $data = json_decode($msg->body, true);
    $newFollowerUsername = $data['new_follower_username'];
    $accountPk = $data['account_pk'];
    // first login to ig with current account
    $account = getAccount($accountPk);

    if ($account != null) {
        $compositions = [];
        foreach ($account as $a) array_push($compositions, $a['composition']['caption']);
        $account = $account[0];
        // first login to ig
        $ig = new Instagram(false, false, $igSession);
        try {
            $ig->login($account['account']['username'], $account['account']['password']);

            // second gather new follower data
            $newFollower = $ig->people->getInfoByName($newFollowerUsername, 'followers');
            if ($newFollower->getStatus() == 'ok') {
                insertMember($newFollower->getUser());
                
                // then send welcoming message to new follower
                $pickComment = rand(0, count($compositions) - 1);

                $usernameComment = $newFollower->getUser()->getUsername();
                $frontnameComment = $usernameComment;
                $fullnameComment = $usernameComment;
                if (!empty($newFollower->getUser()->getFullName())) {
                    $fullnameComment = $newFollower->getUser()->getFullName();
                    $name = explode(' ', $fullnameComment);
                    $frontnameComment = $name[0];
                }
                $comment = $compositions[$pickComment];
                $comment = str_replace('@username', $usernameComment, $comment);
                $comment = str_replace('@fullname', $fullnameComment, $comment);
                $comment = str_replace('@frontname', $frontnameComment, $comment);
                
                echo $comment . PHP_EOL;
                sleep(rand(7, 12));
                $dm = $ig->direct->sendText([
                    'users' => [$newFollower->getUser()->getPk()]
                ], $comment);
                //print_r($dm);
            }
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    } else {
        echo 'hello';
    }


    // message from worker formated as below format
    /*[
        'message' => $push->getMessage(),
        'account_pk' => $push->getIntendedRecipientUserId(),
        'push_id' => $push->getPushId(),
        'new_follower_username' => $push->getActionParam('username')
    ]*/

    //$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};// callback

/*$d = (object)['body' => json_encode([
    'message' => 'hello',
    'account_pk' => '6045121554',
    'push_id' => '1',
    'new_follower_username' => 'aansubarkah'
])];
$callback($d);*/
$channel->basic_qos(null, 1, null);
$channel->basic_consume('newfollower', '', false, true, false, false, $callback);// no acknowledgment
//$channel->basic_consume('newfollower', '', false, false, false, false, $callback);

while (count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();

function getAccount($pk = 0)
{
    $dbhost = 'localhost';
    $dbname = 'automateit';
    $dbuser = 'user';
    $dbpass = 'jayapura';
    $dbtable = 'logs';
    $dbcharset = 'utf8mb4';
    
    $db = new Medoo([
        'database_type' => 'mysql',
        'database_name' => $dbname,
        'server' => $dbhost,
        'username' => $dbuser,
        'password' => $dbpass,
        'charset' => $dbcharset,
    ]);

    $data = null;

    if ((int)$pk > 0) {
        $data = $db->select(
            'accounts',
            [
                '[<]proxies' => ['proxy_id' => 'id'],
                '[>]preferences' => ['accounts.id' => 'account_id'],
                '[>]compositions' => ['accounts.id' => 'account_id'],
            ],
            [
                'account' => [
                    'accounts.id(account_id)',
                    'accounts.user_id',
                    'accounts.username',
                    'accounts.password',
                ],
                'proxy' => [
                    'proxies.id AS proxy_id',
                    'proxies.name AS proxy_name',
                ],
                'preference' => [
                    'preferences.dmbynewfollowing'
                ],
                'composition' => [
                    'compositions.caption'
                ]
            ],
            [
                'AND' => [
                    'accounts.pk' => (int)$pk,
                    'compositions.typeid' => 2,
                    'compositions.active' => 1,
                ],
            ]
        );

        if (count($data) > 0) {
            return $data;
        } else {
            return null;
        }
    }
}// .function get account

function insertMember($datum = null)
{
    $arangoHost = '206.189.46.73';

    //print_r($datum);
    $connectionOptions = [
        // server endpoint to connect to
        ConnectionOptions::OPTION_ENDPOINT => 'tcp://' . $arangoHost . ':8529',
        // authorization type to use (currently supported: 'Basic')
        ConnectionOptions::OPTION_AUTH_TYPE => 'Basic',
        // user for basic authorization
        ConnectionOptions::OPTION_AUTH_USER => 'user',
        // password for basic authorization
        ConnectionOptions::OPTION_AUTH_PASSWD => 'jayapura',
        // connection persistence on server. can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
        ConnectionOptions::OPTION_CONNECTION => 'Close',
        // connect timeout in seconds
        ConnectionOptions::OPTION_TIMEOUT => 3,
        // whether or not to reconnect when a keep-alive connection has timed out on server
        ConnectionOptions::OPTION_RECONNECT => true,
        // optionally create new collections when inserting documents
        ConnectionOptions::OPTION_CREATE => true,
        // optionally create new collections when inserting documents
        ConnectionOptions::OPTION_UPDATE_POLICY => UpdatePolicy::LAST,
        ConnectionOptions::OPTION_DATABASE => 'automateit',
    ];
    
    // open connection
    $connection = new Connection($connectionOptions);

    // create a new collection
    $collectionName = 'members';
    $collection = new Collection($collectionName);
    $collectionHandler = new CollectionHandler($connection);

    if (!$collectionHandler->has($collectionName)) {
        $collectionId = $collectionHandler->create($collection);
    }

    !$datum->getIsPrivate() ? $priv = 0 : $priv = 1;

    $query = 'UPSERT {_key: "' . $datum->getPk() . '"} ';
    $query = $query . 'INSERT { ';
    $query = $query . '_key: "' . $datum->getPk() . '", ';
    $query = $query . 'pk: "' . $datum->getPk() . '", ';
    $query = $query . 'username: "' . $datum->getUsername() . '", ';
    $query = $query . 'fullname: "' . $datum->getFullName() . '", ';
    $query = $query . 'description: "' . $datum->getBiography() . '", ';
    $query = $query . 'profpicurl: "' . $datum->getProfilePicUrl() . '", ';
    $query = $query . 'followers: ' . $datum->getFollowerCount() . ', ';
    $query = $query . 'followings: ' . $datum->getFollowingCount() . ', ';
    $query = $query . 'contents: 0, ';
    $query = $query . 'closed: ' . $priv . ', ';
    $query = $query . 'profpicurlfixed: 1, ';
    $query = $query . 'genderdetection: 0, ';
    $query = $query . 'genderdetected: 1, ';
    $query = $query . 'active: 1';
    $query = $query . ' } ';

    $query = $query . 'UPDATE { ';
    $query = $query . 'username: "' . $datum->getUsername() . '", ';
    $query = $query . 'fullname: "' . $datum->getFullName() . '", ';
    $query = $query . 'description: "' . $datum->getBiography() . '", ';
    $query = $query . 'profpicurl: "' . $datum->getProfilePicUrl() . '", ';
    $query = $query . 'followers: ' . $datum->getFollowerCount() . ', ';
    $query = $query . 'followings: ' . $datum->getFollowingCount() . ', ';
    $query = $query . 'closed: ' . $priv;
    $query = $query . ' } IN members';

    //echo $query . PHP_EOL;

    $statement = new Statement(
        $connection,
        [
            'query' => $query,
            'count' => true,
            'batchSize' => 1,
            'sanitize' => true
        ]
    );
    $cursor = $statement->execute();
}// function get member