<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
require_once $rootPath . 'background/logging.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Medoo\Medoo;

$dbhost = 'localhost';
$dbname = 'automateit';
$dbuser = 'user';
$dbpass = 'jayapura';
$dbtable = 'logs';
$dbcharset = 'utf8mb4';

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $dbname,
    'server' => $dbhost,
    'username' => $dbuser,
    'password' => $dbpass,
    'charset' => $dbcharset,
]);

// -------------------- First ask server which account(s) to handle
//$connection = new AMQPStreamConnection('localhost', 5672, 'radmin', 'radmin');
$connection = new AMQPStreamConnection('tanpa.download', 5672, 'user', 'jayapura');

$channelAskForAccounts = $connection->channel();

