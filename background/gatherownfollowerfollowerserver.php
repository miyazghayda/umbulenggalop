<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
//require_once $rootPath . 'background/logging.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use ArangoDBClient\Connection;
use ArangoDBClient\ConnectionOptions;
use ArangoDBClient\Collection;
use ArangoDBClient\CollectionHandler;
use ArangoDBClient\Document;
use ArangoDBClient\DocumentHandler;
use ArangoDBClient\UpdatePolicy;
use ArangoDBClient\Statement;

$motherServer = 'tanpa.download';
$motherDbUser = 'user';
$motherDbPass = 'jayapura';

$connection = new AMQPStreamConnection($motherServer, 5672, $motherDbUser, $motherDbPass);
$channel = $connection->channel();

$channel->queue_declare('member_follower', false, true, false, false);

$callback = function ($msg) {
    $data = json_decode($msg->body, true);

    $to = insertMember($data);
    insertEdge($data['from_pk'], $to['pk']);
    echo $data['to_username'] . PHP_EOL;
    //$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};// callback

$channel->basic_qos(null, 1, null);
$channel->basic_consume('member_follower', '', false, true, false, false, $callback);// no acknowledgment
//$channel->basic_consume('member_follower', '', false, false, false, false, $callback);

while (count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();

function insertEdge($from = null, $to = null)
{
    if ($from != null && $to != null) {
        $connection = createConnection();

        $query = 'FOR d IN followerof FILTER d._from == "members/' . $from . '" && ';
        $query = $query . 'd._to == "members/' . $to . '" LIMIT 1 RETURN {id:d._id}';
        $statement = new Statement(
            $connection,
            [
                'query' => $query,
                'count' => true,
                'batchSize' => 1,
                'sanitize' => true
            ]
        );

        $cursor = $statement->execute();
        if (count($cursor->getAll()) > 0) {
            $d = $cursor->getAll()[0];
            return ['id' => $d->id];
        } else {
            date_default_timezone_set("Asia/Jakarta");
            $q = 'UPSERT {_from:"members/' . $from . '", _to:"members/' . $to . '"} ';
            $q = $q . 'INSERT {';
            $q = $q . '_from:"members/' . $from . '",';
            $q = $q . '_to:"members/' . $to . '",';
            $q = $q . 'gatherat:' . time() . ',';
            $q = $q . 'active: 1';
            $q = $q . '} UPDATE {';
            $q = $q . 'gatherat:' . time();
            $q = $q . '} IN followerof LET r = NEW RETURN {id:r._id}';
            $statement = new Statement(
                $connection,
                [
                    'query' => $q,
                    'count' => true,
                    'batchSize' => 1,
                    'sanitize' => true
                ]
            );

            $c = $statement->execute();
            if (count($c->getAll()) > 0) {
                $d = $c->getAll()[0];
                return ['id' => $d->id];
            } else {
                return false;
            }
        }
    }
}// function insert edge

function insertMember($datum = null)
{
    if ($datum != null) {
        $connection = createConnection();
        
 /*'from_pk' => $selfPk,
                'to_pk' => $datum['pk'],
                'to_username' => $datum['username'],
                'to_fullname' => $datum['fullname'],
                'to_profpicurl' => $datum['profpicurl'],
                'to_closed' => $datum['closed']*/

        $query = 'FOR d IN members FILTER d._key == "' . $datum['to_pk'] . '" LIMIT 1 RETURN {pk:d.pk}';
        $statement = new Statement(
            $connection,
            [
                'query' => $query,
                'count' => true,
                'batchSize' => 1,
                'sanitize' => true
            ]
        );

        $cursor = $statement->execute();
        if (count($cursor->getAll()) > 0) {
            $d = $cursor->getAll()[0];
            return ['pk' => $d->pk];
        } else {
            $q = 'UPSERT {_key:"' . $datum['to_pk'] . '"} ';
            $q = $q . 'INSERT {';
            $q = $q . '_key:"' . $datum['to_pk'] . '",';
            $q = $q . 'pk:"' . $datum['to_pk'] . '",';
            $q = $q . 'username:"' . addslashes($datum['to_username']) . '",';
            $q = $q . 'fullname:"' . addslashes($datum['to_fullname']) . '",';
            $q = $q . 'profpicurl:"' . $datum['to_profpicurl'] . '",';
            $q = $q . 'closed:' . $datum['to_closed'] . ',';
            $q = $q . 'description:"",';
            $q = $q . 'followers:0,';
            $q = $q . 'followings:0,';
            $q = $q . 'contents:0,';
            $q = $q . 'profpicurlfixed:0,';
            $q = $q . 'genderdetection:0,';
            $q = $q . 'genderdetected:0,';
            $q = $q . 'active: 1';
            $q = $q . '} UPDATE {';
            $q = $q . 'username:"' . addslashes($datum['to_username']) . '",';
            $q = $q . 'fullname:"' . addslashes($datum['to_fullname']) . '",';
            $q = $q . 'profpicurl:"' . $datum['to_profpicurl'] . '",';
            $q = $q . 'closed:' . $datum['to_closed'] . '';
            $q = $q . '} IN members LET r = NEW RETURN {pk:r.pk}';
            $statement = new Statement(
                $connection,
                [
                    'query' => $q,
                    'count' => true,
                    'batchSize' => 1,
                    'sanitize' => true
                ]
            );

            $c = $statement->execute();
            if (count($c->getAll()) > 0) {
                $d = $c->getAll()[0];
                return ['pk' => $d->pk];
            } else {
                return false;
            }
        }
    }
}// function insert member

function createConnection()
{
    $arangoHost = '206.189.46.73';
    $dbuser = 'user';
    $dbpass = 'jayapura';
        // ArangoDb section
    $connectionOptions = [
            // server endpoint to connect to
        ConnectionOptions::OPTION_ENDPOINT => 'tcp://' . $arangoHost . ':8529',
            // authorization type to use (currently supported: 'Basic')
        ConnectionOptions::OPTION_AUTH_TYPE => 'Basic',
            // user for basic authorization
        ConnectionOptions::OPTION_AUTH_USER => $dbuser,
            // password for basic authorization
        ConnectionOptions::OPTION_AUTH_PASSWD => $dbpass,
            // connection persistence on server. can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
        ConnectionOptions::OPTION_CONNECTION => 'Close',
            // connect timeout in seconds
        ConnectionOptions::OPTION_TIMEOUT => 60,
            // whether or not to reconnect when a keep-alive connection has timed out on server
        ConnectionOptions::OPTION_RECONNECT => true,
            // optionally create new collections when inserting documents
        ConnectionOptions::OPTION_CREATE => true,
            // optionally create new collections when inserting documents
        ConnectionOptions::OPTION_UPDATE_POLICY => UpdatePolicy::LAST,
        ConnectionOptions::OPTION_DATABASE => 'automateit',
    ];
        
        // open connection
    $connection = new Connection($connectionOptions);
    return $connection;
}