<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
require_once $rootPath . 'background/logging.php';

use Monolog\Logger;
use Medoo\Medoo;

$dbhost = 'localhost';
$dbname = 'automateit2';
$dbuser = 'user';
$dbpass = 'jayapura';
$dbtable = 'logs';
$dbcharset = 'utf8mb4';

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $dbname,
    'server' => $dbhost,
    'username' => $dbuser,
    'password' => $dbpass,
    'charset' => $dbcharset,
]);

$log = new Logger('scrapehashtagposts');
$log->pushHandler($mySQLHandler);
$account_id = 1; // mandatory
$scriptid = 2; // mandatory, scrapehashtagposts

$members = $db->select('members',
    ['pk'],
    [
        'id[>]' => 0,
        'ORDER' => ['pk' => 'ASC'],
    ]);
//$data = [];
$fp = fopen('members.csv', 'w');
$i = 0;
foreach ($members as $member) {
    fputcsv($fp, $member['pk']);
    echo $i . PHP_EOL;
    $i++;
    //$data[(int)$member['pk']] = $member;
}
fclose($fp);

$posts = $db->select('posts',
['pk'],
['id[>]' => 0, 'ORDER' => ['pk' => 'ASC']]);

$fp = fopen('posts.csv', 'w');
$i = 0;
foreach ($posts as $post) {
    fputcsv($fp, $post['pk']);
    echo $i . PHP_EOL;
    $i++;
}
fclose($fp);

$locations = $db->select('locations',
['pk'],
['id[>]' => 0, 'ORDER' => ['pk' => 'ASC']]);

$fp = fopen('locations.csv', 'w');
$i = 0;
foreach ($locations as $location) {
    fputcsv($fp, $location['pk']);
    echo $i . PHP_EOL;
    $i++;
}
fclose($fp);