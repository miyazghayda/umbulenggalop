<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
require_once $rootPath . 'background/logging.php';

use Monolog\Logger;
use Medoo\Medoo;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use ArangoDBClient\Connection;
use ArangoDBClient\ConnectionOptions;
use ArangoDBClient\Collection;
use ArangoDBClient\CollectionHandler;
use ArangoDBClient\Document;
use ArangoDBClient\DocumentHandler;
use ArangoDBClient\UpdatePolicy;
use ArangoDBClient\Statement;

$log = new Logger('duplicatingposts');
$log->pushHandler($mySQLHandler);
$account_id = 1; // mandatory
$scriptid = 1; // mandatory, duplicatingposts

$dbhost = 'localhost';
$dbname = 'automateit2';
$dbuser = 'user';
$dbpass = 'jayapura';
$dbtable = 'logs';
$dbcharset = 'utf8mb4';
$arangoHost = '206.189.46.73';

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $dbname,
    'server' => $dbhost,
    'username' => $dbuser,
    'password' => $dbpass,
    'charset' => $dbcharset,
]);

$connectionOptions =array(
    // server endpoint to connect to
    ConnectionOptions::OPTION_ENDPOINT => 'tcp://' . $arangoHost . ':8529',
    // authorization type to use (currently supported: 'Basic')
    ConnectionOptions::OPTION_AUTH_TYPE => 'Basic',
    // user for basic authorization
    ConnectionOptions::OPTION_AUTH_USER => 'user',
    // password for basic authorization
    ConnectionOptions::OPTION_AUTH_PASSWD => 'jayapura',
    // connection persistence on server. can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
    ConnectionOptions::OPTION_CONNECTION => 'Close',
    // connect timeout in seconds
    ConnectionOptions::OPTION_TIMEOUT => 3,
    // whether or not to reconnect when a keep-alive connection has timed out on server
    ConnectionOptions::OPTION_RECONNECT => true,
    // optionally create new collections when inserting documents
    ConnectionOptions::OPTION_CREATE => true,
    // optionally create new collections when inserting documents
    ConnectionOptions::OPTION_UPDATE_POLICY => UpdatePolicy::LAST,
    ConnectionOptions::OPTION_DATABASE => 'automateit',
);
 
// open connection
$connection = new Connection($connectionOptions);

// create a new collection
$collectionName = 'posts';
$collection = new Collection($collectionName);
$collectionHandler = new CollectionHandler($connection);
 
if (!$collectionHandler->has($collectionName)) {
    $collectionId = $collectionHandler->create($collection);
}

$documentHandler = new DocumentHandler($connection);

$membersCount = $db->query('SELECT COUNT(id) AS total FROM posts WHERE active = 1')->fetchAll();
echo 'will processing ' . $membersCount[0]['total'] . ' rows' . PHP_EOL;

/*$query = 'FOR m IN members FILTER m.pk == 39631564510 LIMIT 1 RETURN {id:m._id, pk:m.pk, key:m._key, username:m.username}';
$statement = new Statement(
    $connection,
    [
        'query' => $query,
        'count' => true,
        'batchSize' => 1,
        'sanitize' => true
    ]
    );
$cursor = $statement->execute();
if (count($cursor->getAll()) > 0) {
    print_r($cursor->getAll()[0]->id);
} else {
    echo 'null' . PHP_EOL;
}*/
//print_r($cursor->getAll()[0]->id);
/*foreach ($cursor as $key => $value) {
    echo $value->id . PHP_EOL;
}*/
//print_r($cursor->data);

$queryLimit = 25000;// 25.000
$queryPages = ceil($membersCount[0]['total'] / $queryLimit);
$counter = 1;
for ($i = 1; $i <= $queryPages; $i++) {
    $queryOffset = ($i - 1) * $queryLimit;
    $members = $db->select('posts',
        [
            '[<]locations' => ['location_id' => 'id'],
            '[<]members' => ['member_id' => 'id'],
        ],
        [
            'post' => [
                'posts.pk(post_pk)',
                'posts.sourceid',
                'posts.typeid',
                'posts.caption',
                'posts.likes',
                'posts.comments',
                'posts.takenat',
                'posts.active(post_active)'
            ],
            'member' => [
                'members.pk(member_pk)',
                'members.username',
                'members.fullname',
                'members.description',
                'members.profpicurl',
                'members.followers',
                'members.followings',
                'members.contents',
                'members.closed',
                'members.profpicurlfixed',
                'members.genderdetection',
                'members.genderdetected',
                'members.active(member_active)'
            ],
            'location' => [
                'locations.pk(location_pk)',
                'locations.fbplacesid',
                'locations.lat',
                'locations.lng',
                'locations.address',
                'locations.name',
                'locations.shortname',
                'locations.active(location_active)'
            ]
        ],
        [
            'posts.id[>]' => 0,
            'ORDER' => ['posts.pk' => 'ASC'],
            'LIMIT' => [$queryOffset, $queryLimit],
        ]);
    foreach ($members as $member) {
        $member['location']['address'] = addslashes($member['location']['address']);
        $member['location']['name'] = addslashes($member['location']['name']);
        $member['location']['shortname'] = addslashes($member['location']['shortname']);
        $member['member']['fullname'] = addslashes($member['member']['fullname']);
        $member['member']['description'] = addslashes($member['member']['description']);
        $member['post']['caption'] = addslashes($member['post']['caption']);

        // Find Member
        $queryMember = 'FOR m IN members FILTER m._key == "' . $member['member']['member_pk'] . '" LIMIT 1 RETURN {id:m._id, pk:m.pk, key:m._key, username:m.username}';
        $statement = new Statement(
            $connection,
            [
                'query' => $queryMember,
                'count' => true,
                'batchSize' => 1,
                'sanitize' => true
            ]
            );
            
        //echo "inserting member" . PHP_EOL;
        //echo "member pk " . $member['member']['member_pk'] . PHP_EOL;
        $cursor = $statement->execute();
        if (count($cursor->getAll()) > 0) {
            $memberId = $cursor->getAll()[0]->id;
            //echo 'member exists' . PHP_EOL;
        } else {
            $q = 'UPSERT {_key:"' . $member['member']['member_pk'] . ',"} ';
            $q = $q . 'INSERT {pk:"' . $member['member']['member_pk'] . '",';
            $q = $q . 'INSERT {_key:"' . $member['member']['member_pk'] . '",';
            $q = $q . 'username:"' . $member['member']['username'] . '",';
            $q = $q . 'fullname:"' . $member['member']['fullname'] . '",';
            $q = $q . 'description:"' . $member['member']['description'] . '",';
            $q = $q . 'profpicurl:"' . $member['member']['profpicurl'] . '",';
            $q = $q . 'followers:' . $member['member']['followers'] . ',';
            $q = $q . 'followings:' . $member['member']['followings'] . ',';
            $q = $q . 'contents:' . $member['member']['contents'] . ',';
            $q = $q . 'closed:' . $member['member']['closed'] . ',';
            $q = $q . 'profpicurlfixed:' . $member['member']['profpicurlfixed'] . ',';
            $q = $q . 'genderdetection:' . $member['member']['genderdetection'] . ',';
            $q = $q . 'active:' . $member['member']['member_active'];
            $q = $q . '} UPDATE {';
            $q = $q . 'fullname:"' . $member['member']['fullname'] . '",';
            $q = $q . 'profpicurl:"' . $member['member']['profpicurl'] . '"';
            $q = $q . '} IN members OPTIONS {overwrite:true} LET ret = NEW RETURN ret';
            
            $statement = new Statement(
            $connection,
            [
                'query' => $q,
                'count' => true,
                'batchSize' => 1,
                'sanitize' => true
            ]
            );
            $c = $statement->execute();
            $memberId = $c->getAll()[0]->id;
            /*$document = new Document();
            $document = $document->createFromArray([
                'pk' => $member['member']['member_pk'],
                'username' => $member['member']['username'],
                'fullname' => $member['member']['fullname'],
                'description' => $member['member']['description'],
                'profpicurl' => $member['member']['profpicurl'],
                'followers' => $member['member']['followers'],
                'followings' => $member['member']['followings'],
                'contents' => $member['member']['contents'],
                'closed' => $member['member']['closed'],
                'profpicurlfixed' => $member['member']['profpicurlfixed'],
                'genderdetection' => $member['member']['genderdetection'],
                'genderdetected' => $member['member']['genderdetected'],
                'active' => $member['member']['member_active']
            ]);
            $memberId = $documentHandler->save('members', $document);*/
        }

        //echo "inserting location" . PHP_EOL;

        // Find Location
        $queryLocation = 'FOR m IN locations FILTER _.key == "' . $member['location']['fbplacesid'] . '" LIMIT 1 RETURN {id:m._id, fbplacesid:m.fbplacesid, pk:m.pk, key:m._key, name:m.name}';
        $statement = new Statement(
            $connection,
            [
                'query' => $queryLocation,
                'count' => true,
                'batchSize' => 1,
                'sanitize' => true
            ]
            );
            
        $cursor = $statement->execute();
        if (count($cursor->getAll()) > 0) {
            $locationId = $cursor->getAll()[0]->id;
        } else {
            $document = new Document();
            $document = $document->createFromArray([
                '_key' => (string)$member['location']['fbplacesid'],
                'fbplacesid' => (string)$member['location']['fbplacesid'],
                'pk' => $member['location']['location_pk'],
                'name' => $member['location']['name'],
                'shortname' => $member['location']['shortname'],
                'address' => $member['location']['address'],
                'lat' => $member['location']['lat'],
                'lng' => $member['location']['lng'],
                'active' => $member['location']['location_active']
            ]);
            $locationId = $documentHandler->save('locations', $document);
        }

        $query = 'UPSERT {_key: "' . $member['post']['post_pk'] . '"} ';
        $query = $query . 'INSERT { ';
        $query = $query . 'pk: "' . $member['post']['post_pk'] . '", ';
        $query = $query . '_key: "' . $member['post']['post_pk'] . '", ';
        $query = $query . 'sourceid: "' . $member['post']['sourceid'] . '", ';
        $query = $query . 'caption: "' . $member['post']['caption'] . '", ';
        $query = $query . 'typeid: ' . $member['post']['typeid'] . ', ';
        $query = $query . 'likes: ' . $member['post']['likes'] . ', ';
        $query = $query . 'comments: ' . $member['post']['comments'] . ', ';
        $query = $query . 'takenat: ' . $member['post']['takenat'] . ', ';
        $query = $query . 'location: "' . $locationId . '", ';
        $query = $query . 'member: "' . $memberId . '", ';
        $query = $query . 'location_resume: {';
        $query = $query . 'fbplacesid:"' . $member['location']['fbplacesid'] . '", ';
        $query = $query . 'pk:"' . $member['location']['location_pk'] . '", ';
        $query = $query . 'lat:' . $member['location']['lat'] . ', ';
        $query = $query . 'lng:' . $member['location']['lng'] . ', ';
        $query = $query . 'name:"' . $member['location']['name'] . '", ';
        $query = $query . 'shortname:"' . $member['location']['shortname'] . '"';
        $query = $query . '}, ';
        $query = $query . 'member_resume: {';
        $query = $query . 'pk:"' . $member['member']['member_pk'] . '", ';
        $query = $query . 'username:"'. $member['member']['username'] .'"';
        $query = $query . '},';
        $query = $query . 'active: ' . $member['post']['post_active'];
        $query = $query . ' } ';

        $query = $query . 'UPDATE { ';
        $query = $query . 'likes: ' . $member['post']['likes'] . ', ';
        $query = $query . 'comments: ' . $member['post']['comments'];
        $query = $query . ' } IN posts';
        
        echo $query . PHP_EOL;

        $statement = new Statement(
            $connection,
            [
                'query' => $query,
                'count' => true,
                'batchSize' => 1,
                'sanitize' => true
            ]
            );
        $cursor = $statement->execute();

        echo $counter . '. ' . $member['post']['caption'] . PHP_EOL;
        $counter++;
    }
}