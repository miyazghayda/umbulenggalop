<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
require_once $rootPath . 'background/logging.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

// -------------------- First ask server which account(s) to handle
//$connection = new AMQPStreamConnection('localhost', 5672, 'radmin', 'radmin');
$connection = new AMQPStreamConnection('tanpa.download', 5672, 'user', 'jayapura');

$channelAskForAccounts = $connection->channel();
//$channel->queue_declare('seeposts', false, true, false, false);

$localIp = getHostByName(getHostName());
//echo $localIp;

// First ask server which account(s) to handle
$channelAskForAccounts->queue_declare('askforaccountstohandle', false, true, false, false);
$msg = new AMQPMessage(
    $localIp,
    ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]
);
$channelAskForAccounts->basic_publish($msg, '', 'askforaccountstohandle');
$channelAskForAccounts->close();

// Second get accounts to handle
$channelAnswerForAccounts = $connection->channel();
$channelAnswerForAccounts->queue_declare('answerforaccountstohandle', false, true, false, false);

$callbackAnswerForAccounts = function ($msg) {
    $data = json_decode($msg->body);
    print_r($data);
};

$channelAnswerForAccounts->basic_qos(null, 1, null);
$channel->basic_consume('answerforaccountstohandle', '', false, false, false, false, $callbackAnswerForAccounts);

while (count($channelAnswerForAccounts->callbacks)) {
    $channelAnswerForAccounts->wait();
}

$connection->close();