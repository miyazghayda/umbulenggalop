<?php
/**
 * 
 */

$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
require_once $rootPath . 'background/logging.php';

use Monolog\Logger;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;
use Medoo\Medoo;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('localhost', 5672, 'radmin', 'radmin');
$channel = $connection->channel();
$channel->queue_declare('hellow', false, false, false, false);

echo "[*] Waiting for messages. To exit press CTRL+C" . PHP_EOL;

$callback = function ($msg) {
    echo '[x] Received ' . $msg->body . PHP_EOL;
};

$channel->basic_consume('hellow', '', false, true, false, false, $callback);

while (count($channel->callbacks)) {
    $channel->wait();
}