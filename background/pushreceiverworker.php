<?php
// multiple accounts reference
// https://github.com/mgp25/Instagram-API/issues/1991
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
//require_once $rootPath . 'background/logging.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use InstagramAPI\Instagram;
use InstagramAPI\Push;
use InstagramAPI\Push\Notification;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;
use React\EventLoop\Factory;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


$motherServer = 'tanpa.download';
$motherDbUser = 'user';
$motherDbPass = 'jayapura';
$igSession = [
    'dbhost' => $motherServer,
    'dbname' => 'automateit',
    'dbusername' => 'user2',
    'dbpassword' => $motherDbPass
];

// -------------------- First ask server which account(s) to handle
$url = 'https://tanpa.download/accounts/tohandleworkers';
// Get accounts to handle
$ch = curl_init();
// Disable SSL verification
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// Will return the response, if false it print the response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
curl_setopt($ch, CURLOPT_URL, $url);
// Execute
$result = curl_exec($ch);
// Closing
curl_close($ch);
$result = json_decode($result, true);
$result = array_slice($result, 0, 10, true);// only process first 10 element
//$result = [$result[0]];
/*$result = [[
    'username' => 'miyazghayda',
    'password' => 'jayapura'
]];*/
if (count($result) > 0) {
    $igs = [];
    $pushes = [];
    $debug = true;

    $loop = Factory::create();

    if ($debug) {
        $logger = new Logger('push');
        $logger->pushHandler(new StreamHandler('php://stdout', Logger::INFO));
    } else {
        $logger = null;
    }

    $accounts = $result;
    $i = 0;
    foreach ($accounts as $account) {
        $igs[$i] = new Instagram(false, false, $igSession);
        try {
            $igs[$i]->login($account['username'], $account['password']);
            $pushes[$i] = new Push($loop, $igs[$i], $logger);
        } catch (Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }
        $i++;
    }// .foreach account

    foreach ($pushes as $push) {
        $push->on('new_follower', function (Notification $push) {
            global $motherServer;
            global $motherDbUser;
            global $motherDbPass;
            /*$message = $push->getMessage();
            $accountPk = $push->getIntendedRecipientUserId();
            $pushId = $push->getPushId();
            $newFollowerUsername = $push->getActionParam('username');
            echo $message . ' | ' . $accountPk . ' | ' . $pushId . ' | ' . $newFollowerUsername . PHP_EOL;*/

            $connection = new AMQPStreamConnection($motherServer, 5672, $motherDbUser, $motherDbPass);
            $channelNewFollower = $connection->channel();
            $channelNewFollower->queue_declare('newfollower', false, true, false, false);
            $msg = new AMQPMessage(
                json_encode([
                    'message' => $push->getMessage(),
                    'account_pk' => $push->getIntendedRecipientUserId(),
                    'push_id' => $push->getPushId(),
                    'new_follower_username' => $push->getActionParam('username')
                ]),
                ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]
            );
            $channelNewFollower->basic_publish($msg, '', 'newfollower');
            $channelNewFollower->close();
            $connection->close();
        });

        $push->on('direct_v2_message', function (Notification $push) {
            $thread_id = $push->getActionParam('id');
            $thread_x = $push->getActionParam('x');
            $message = $push->getMessage();
            $accountPk = $push->getIntendedRecipientUserId();
            $pushId = $push->getPushId();
            echo $thread_id . ' | ' . $thread_x . ' | ' . $message . ' | ' . $accountPk . ' | ' . $pushId . PHP_EOL;
        });

        $push->on('error', function (Exception $e) use ($push, $loop) {
            printf('[!!!] Got fatal error from FBNS: %s%s', $e->getMessage(), PHP_EOL);
            $push->stop();
            $loop->stop();
        });

        $push->start();
    }// .foreach push

    $loop->run();
}// .if count array > 0