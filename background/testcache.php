<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
require_once $rootPath . 'background/logging.php';

use Monolog\Logger;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;
use Medoo\Medoo;

$dbhost = 'localhost';
$dbname = 'automateit2';
$dbuser = 'user';
$dbpass = 'jayapura';
$dbtable = 'logs';
$dbcharset = 'utf8mb4';

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $dbname,
    'server' => $dbhost,
    'username' => $dbuser,
    'password' => $dbpass,
    'charset' => $dbcharset,
]);

$log = new Logger('scrapehashtagposts');
$log->pushHandler($mySQLHandler);
$account_id = 1; // mandatory
$scriptid = 2; // mandatory, scrapehashtagposts

$t = microtime(true);
// Create cache path if not exists
$cachePath = $rootPath . 'tmp/cache/members/';
$pk = 1321;
$cacheFiles = scandir($cachePath, 1);

if (isset($argv[1])) $pk = $argv[1];

$cacheFile = null;
$i = 0;
$cacheNames = [];
foreach ($cacheFiles as $cf) {
    if (strpos($cf, '.') === false) {
        array_push($cacheNames, $cf);
    }
}

arsort($cacheNames);
foreach ($cacheNames as $cn) {
    if ((int)$pk >= (int)$cn) {
        $cacheFile = $cachePath . $cn;
        break;
    }
}
//print_r($cacheNames);
//echo $cacheFile . PHP_EOL;
$val = cache_get($cacheFile);
if ($val !== false) {
    print_r($val[$pk]);
}

echo 'took time ';
echo microtime(true) - $t;
echo PHP_EOL;
//echo time();//unixtime

function cache_get($cachePath) {
    @include $cachePath;
    return isset($val) ? $val : false;
}