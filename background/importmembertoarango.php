<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
require_once $rootPath . 'background/logging.php';

use Monolog\Logger;
use Medoo\Medoo;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use ArangoDBClient\Connection;
use ArangoDBClient\ConnectionOptions;
use ArangoDBClient\Collection;
use ArangoDBClient\CollectionHandler;
use ArangoDBClient\Document;
use ArangoDBClient\DocumentHandler;
use ArangoDBClient\UpdatePolicy;
use ArangoDBClient\Statement;
/*use triagens\ArangoDb;
use triagens\ArangoDb\Connection;
use triagens\ArangoDb\ConnectionOptions;
use triagens\ArangoDb\Collection;
use triagens\ArangoDb\CollectionHandler;
use triagens\ArangoDb\Document;
use triagens\ArangoDb\DocumentHandler;
use triagens\ArangoDb\UpdatePolicy;*/

$log = new Logger('duplicatingposts');
$log->pushHandler($mySQLHandler);
$account_id = 1; // mandatory
$scriptid = 1; // mandatory, duplicatingposts

$dbhost = 'localhost';
$dbname = 'automateit2';
$dbuser = 'user';
$dbpass = 'jayapura';
$dbtable = 'logs';
$dbcharset = 'utf8mb4';
$arangoHost = '206.189.46.73';

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $dbname,
    'server' => $dbhost,
    'username' => $dbuser,
    'password' => $dbpass,
    'charset' => $dbcharset,
]);

$connectionOptions =array(
    // server endpoint to connect to
    ConnectionOptions::OPTION_ENDPOINT => 'tcp://' . $arangoHost . ':8529',
    // authorization type to use (currently supported: 'Basic')
    ConnectionOptions::OPTION_AUTH_TYPE => 'Basic',
    // user for basic authorization
    ConnectionOptions::OPTION_AUTH_USER => 'user',
    // password for basic authorization
    ConnectionOptions::OPTION_AUTH_PASSWD => 'jayapura',
    // connection persistence on server. can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
    ConnectionOptions::OPTION_CONNECTION => 'Close',
    // connect timeout in seconds
    ConnectionOptions::OPTION_TIMEOUT => 3,
    // whether or not to reconnect when a keep-alive connection has timed out on server
    ConnectionOptions::OPTION_RECONNECT => true,
    // optionally create new collections when inserting documents
    ConnectionOptions::OPTION_CREATE => true,
    // optionally create new collections when inserting documents
    ConnectionOptions::OPTION_UPDATE_POLICY => UpdatePolicy::LAST,
    ConnectionOptions::OPTION_DATABASE => 'automateit',
);
 
// open connection
$connection = new Connection($connectionOptions);

// create a new collection
$collectionName = 'members';
$collection = new Collection($collectionName);
$collectionHandler = new CollectionHandler($connection);
 
/*if ($collectionHandler->has($collectionName)) {
  // drops an existing collection with the same name to make
  // tutorial repeatable
  $collectionHandler->drop($collectionName);
}
 
$collectionId = $collectionHandler->create($collection);
$documentHandler = new DocumentHandler($connection);*/
if (!$collectionHandler->has($collectionName)) {
    $collectionId = $collectionHandler->create($collection);
}

$documentHandler = new DocumentHandler($connection);

$membersCount = $db->query('SELECT COUNT(id) AS total FROM members WHERE active = 1')->fetchAll();
echo 'will processing ' . $membersCount[0]['total'] . ' rows' . PHP_EOL;

$queryLimit = 25000;// 25.000
$queryPages = ceil($membersCount[0]['total'] / $queryLimit);
$counter = 1;
for ($i = 1; $i <= $queryPages; $i++) {
    $queryOffset = ($i - 1) * $queryLimit;
    $members = $db->select('members',
        [
            'id', 'pk', 'username', 'fullname', 'description', 'profpicurl',
            'followers', 'followings', 'contents', 'closed', 'created', 'modified',
            'profpicurlfixed', 'genderdetection', 'genderdetected', 'active'
        ],
        [
            'id[>]' => 0,
            'ORDER' => ['id' => 'ASC'],
            'LIMIT' => [$queryOffset, $queryLimit],
        ]);
    //$data = [];
    foreach ($members as $member) {
        /*$document = new Document();
        $document->set('pk', $member['pk']);
        $document->set('username', $member['username']);
        $document->set('fullname', $member['fullname']);
        $document->set('description', $member['description']);
        $document->set('profpicurl', $member['profpicurl']);
        $document->set('followers', $member['followers']);
        $document->set('followings', $member['followings']);
        $document->set('contents', $member['contents']);
        $document->set('closed', $member['closed']);
        $document->set('profpicurlfixed', $member['profpicurlfixed']);
        $document->set('genderdetection', $member['genderdetection']);
        $document->set('genderdetected', $member['genderdetected']);
        $document->set('active', $member['active']);
        $documentId = $documentHandler->save($collectionName, $document);*/
        //$member['fullname'] = str_replace('"', '\'', $member['fullname']);
        //$member['description'] = str_replace('"', '\'', $member['description']);
        $member['fullname'] = addslashes($member['fullname']);
        $member['description'] = addslashes($member['description']);

        $query = 'UPSERT {_key: "' . $member['pk'] . '"} ';
        $query = $query . 'INSERT { ';
        $query = $query . '_key: "' . $member['pk'] . '", ';
        $query = $query . 'pk: "' . $member['pk'] . '", ';
        $query = $query . 'username: "' . $member['username'] . '", ';
        $query = $query . 'fullname: "' . $member['fullname'] . '", ';
        $query = $query . 'description: "' . $member['description'] . '", ';
        $query = $query . 'profpicurl: "' . $member['profpicurl'] . '", ';
        $query = $query . 'followers: ' . $member['followers'] . ', ';
        $query = $query . 'followings: ' . $member['followings'] . ', ';
        $query = $query . 'contents: ' . $member['contents'] . ', ';
        $query = $query . 'closed: ' . $member['closed'] . ', ';
        $query = $query . 'profpicurlfixed: ' . $member['profpicurlfixed'] . ', ';
        $query = $query . 'genderdetection: ' . $member['genderdetection'] . ', ';
        $query = $query . 'genderdetected: ' . $member['genderdetected'] . ', ';
        $query = $query . 'active: ' . $member['active'];
        $query = $query . ' } ';

        $query = $query . 'UPDATE { ';
        $query = $query . 'username: "' . $member['username'] . '", ';
        $query = $query . 'fullname: "' . $member['fullname'] . '", ';
        $query = $query . 'description: "' . $member['description'] . '", ';
        $query = $query . 'profpicurl: "' . $member['profpicurl'] . '", ';
        $query = $query . 'followers: ' . $member['followers'] . ', ';
        $query = $query . 'followings: ' . $member['followings'] . ', ';
        $query = $query . 'contents: ' . $member['contents'] . ', ';
        $query = $query . 'closed: ' . $member['closed'] . ', ';
        $query = $query . 'profpicurlfixed: ' . $member['profpicurlfixed'] . ', ';
        $query = $query . 'genderdetection: ' . $member['genderdetection'] . ', ';
        $query = $query . 'genderdetected: ' . $member['genderdetected'] . ', ';
        $query = $query . 'active: ' . $member['active'];
        $query = $query . ' } IN members';
        
        echo $query . PHP_EOL;

        $statement = new Statement(
            $connection,
            [
                'query' => $query,
                'count' => true,
                'batchSize' => 1,
                'sanitize' => true
            ]
            );
        $cursor = $statement->execute();

        echo $counter . '. ' . $member['username'] . PHP_EOL;
        $counter++;
        //$data[(int)$member['pk']] = $member;
    }
    //cache_set($members[0]['pk'], $data);
    //echo $i . '. caching ' . $members[0]['pk'] . PHP_EOL;
}
//var_dump($collectionId);