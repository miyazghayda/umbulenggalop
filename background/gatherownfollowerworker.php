<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
//require_once $rootPath . 'background/logging.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use InstagramAPI\Instagram;
use InstagramAPI\Push;
use InstagramAPI\Push\Notification;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;
use React\EventLoop\Factory;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


$motherServer = 'tanpa.download';
$motherDbUser = 'user';
$motherDbPass = 'jayapura';
$igSession = [
    'dbhost' => $motherServer,
    'dbname' => 'automateit',
    'dbusername' => 'user2',
    'dbpassword' => $motherDbPass
];

// -------------------- First ask server which account(s) to handle
//$url = 'https://tanpa.download/accounts/tohandleworkers/2/j4y4pur4';
$url = 'https://tanpa.download/accounts/tohandleworkers';

// Get accounts to handle
$ch = curl_init();
// Disable SSL verification
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// Will return the response, if false it print the response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
curl_setopt($ch, CURLOPT_URL, $url);
// Execute
$result = curl_exec($ch);
// Closing
curl_close($ch);
$result = json_decode($result, true);
$result = array_slice($result, 0, 10, true);// only process first 10 element
if (count($result) > 0) {
    $accounts = $result;
    //$accounts = [$accounts[2]];
    foreach ($accounts as $account) {
        $selfPk = 0;
        $followersFile = '';

        $ig = new Instagram(false, false, $igSession);
        try {
            echo 'Trying to login to ' . $account['username'] . PHP_EOL;
            $ig->login($account['username'], $account['password']);
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }

        try {
            global $followersFile;
            $response = $ig->people->getSelfInfo();
            //print_r($response);
            if ($response->getStatus() == 'ok') {
                $d = $response->getUser();
                !$d->getIsPrivate() ? $priv = 0 : $priv = 1;

                $selfPk = $d->getPk();
                $followersFile = $rootPath . 'tmp/cache/followers/' . $selfPk . '.txt';
                if (file_exists($followersFile)) unlink($followersFile);

                $selfData = [
                    'pk' => $d->getPk(),
                    'username' => $d->getUsername(),
                    'fullname' => $d->getFullName(),
                    'description' => $d->getBiography(),
                    'profpicurl' => $d->getProfilePicUrl(),
                    'followers' => $d->getFollowerCount(),
                    'followings' => $d->getFollowingCount(),
                    'contents' => $d->getMediaCount(),
                    'closed' => $priv,
                    'profpicurlfixed' => 0,
                    'genderdetection' => 0,
                    'genderdetected' => 0,
                    'active' => 1
                ];
                selfInfo($selfData);
            }
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }
        try {
            global $followersFile;
            $sig = new Signatures;
            $rankToken = $sig::generateUUID();
            $maxId = null;
            echo $followersFile . PHP_EOL;
            do {
                $response = $ig->people->getFollowers($selfPk, $rankToken, null, $maxId);

                //print_r($response);
                if ($response->getStatus() == 'ok') {
                    $maxId = $response->getNextMaxId();
                    foreach ($response->getUsers() as $user) {
                        !$user->getIsPrivate() ? $priv = 0 : $priv = 1;

                        // write to cache
                        $txt = "{";
                        $txt = $txt . "\"pk\":\"" . $user->getPk() . "\",";
                        $txt = $txt . "\"username\":\"" . $user->getUsername() . "\",";
                        $txt = $txt . "\"fullname\":\"" . $user->getFullName() . "\",";
                        $txt = $txt . "\"profpicurl\":\"" . $user->getProfilePicUrl() . "\",";
                        $txt = $txt . "\"closed\":\"" . $priv . "\"";
                        $txt = $txt . "}";
                        $myFile = file_put_contents($followersFile, $txt . PHP_EOL, FILE_APPEND | LOCK_EX);

                        // dont gather the rest of follower if the newest follower was inserted before
                        if ((int)$account['last_follower_pk'] == (int)$user->getPk()) $maxId = null;
                    }
                    sleep(rand(7, 12));
                }
                // comment below line on production
                //$maxId = null;
            } while ($maxId !== null);

        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }

        // read cache file
        $file = array_reverse(file($followersFile));
        foreach ($file as $f) {
            $da = json_decode(trim($f), true);
            echo $da['username'] . PHP_EOL;
            followerInfo($selfPk, $da);
        }
    }// foreach account
}// if count result > 0

function selfInfo($datum = null) {
    if ($datum != null) {
        $motherServer = 'tanpa.download';
        $motherDbUser = 'user';
        $motherDbPass = 'jayapura';

        $connection = new AMQPStreamConnection($motherServer, 5672, $motherDbUser, $motherDbPass);
        $channelNewFollower = $connection->channel();
        $channelNewFollower->queue_declare('member_self', false, true, false, false);
        $msg = new AMQPMessage(
            json_encode($datum),
            ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]
        );
        $channelNewFollower->basic_publish($msg, '', 'member_self');
        $channelNewFollower->close();
        $connection->close();
    }
}

function followerInfo($selfPk = 0, $datum = null) {
    if ($selfPk > 0 && $datum != null) {
        $motherServer = 'tanpa.download';
        $motherDbUser = 'user';
        $motherDbPass = 'jayapura';

        $connection = new AMQPStreamConnection($motherServer, 5672, $motherDbUser, $motherDbPass);
        $channelNewFollower = $connection->channel();
        $channelNewFollower->queue_declare('member_follower', false, true, false, false);
        $msg = new AMQPMessage(
            json_encode([
                'from_pk' => $selfPk,
                'to_pk' => $datum['pk'],
                'to_username' => $datum['username'],
                'to_fullname' => $datum['fullname'],
                'to_profpicurl' => $datum['profpicurl'],
                'to_closed' => $datum['closed']
            ]),
            ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]
        );
        $channelNewFollower->basic_publish($msg, '', 'member_follower');
        $channelNewFollower->close();
        $connection->close();
    }
}