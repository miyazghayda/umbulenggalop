<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
require_once $rootPath . 'background/logging.php';

use Monolog\Logger;
use Medoo\Medoo;

$dbhost = 'localhost';
$dbname = 'automateit2';
$dbuser = 'user';
$dbpass = 'jayapura';
$dbtable = 'logs';
$dbcharset = 'utf8mb4';

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $dbname,
    'server' => $dbhost,
    'username' => $dbuser,
    'password' => $dbpass,
    'charset' => $dbcharset,
]);

$log = new Logger('scrapehashtagposts');
$log->pushHandler($mySQLHandler);
$account_id = 1; // mandatory
$scriptid = 2; // mandatory, scrapehashtagposts

$cachePath = $rootPath . 'tmp/cache/';
// Delete folder if exists
echo $cachePath . 'members' . PHP_EOL;

if (is_dir($cachePath . 'members')) {
    $files = glob($cachePath . 'members/*');
    foreach ($files as $file) {
        if (is_file($file)) unlink($file);
    }
}

// Create cache path if not exists
if (!file_exists($cachePath . 'members')) mkdir($cachePath . 'members', 0777, true);

$cachePath = $cachePath . 'members/';

$membersCount = $db->query('SELECT COUNT(id) AS total FROM members WHERE active = 1')->fetchAll();
echo 'will processing ' . $membersCount[0]['total'] . ' rows' . PHP_EOL;

$queryLimit = 25000;// 25.000
$queryPages = ceil($membersCount[0]['total'] / $queryLimit);
for ($i = 1; $i <= $queryPages; $i++) {
    $queryOffset = ($i - 1) * $queryLimit;
    $members = $db->select('members',
        [
            'id', 'pk', 'username', 'fullname', 'description', 'profpicurl',
            'followers', 'followings', 'contents', 'closed', 'created', 'modified',
            'profpicurlfixed', 'genderdetection', 'genderdetected', 'active'
        ],
        [
            'id[>]' => 0,
            'ORDER' => ['pk' => 'ASC'],
            'LIMIT' => [$queryOffset, $queryLimit],
        ]);
    $data = [];
    foreach ($members as $member) {
        $data[(int)$member['pk']] = $member;
    }
    cache_set($members[0]['pk'], $data);
    echo $i . '. caching ' . $members[0]['pk'] . PHP_EOL;
}

/*$members = $db->select('members',
    ['id', 'pk', 'username', 'fullname', 'description', 'profpicurl',
    'followers', 'followings', 'contents', 'closed', 'created', 'modified',
    'profpicurlfixed', 'genderdetection', 'genderdetected', 'active'],
    [
        'active' => true
    ]
);

$i = 0;
foreach ($members as $member) {
    echo $i . '. ' . $member['username'] . PHP_EOL;
    $i++;
    cache_set($member['pk'], $member);
}*/

// source https://medium.com/@dylanwenzlau/500x-faster-caching-than-redis-memcache-apc-in-php-hhvm-dcd26e8447ad
function cache_set($key, $val) {
    global $cachePath;
    $val = var_export($val, true);
    // HHVM fails at __set_state, so just use object cast for now
    $val = str_replace('stdClass::__set_state', '(object)', $val);
    // Write to temp file first to ensure atomicity
    $tmp = $cachePath . $key . '.' . uniqid('', true) . '.tmp';
    file_put_contents($tmp, '<?php $val = ' . $val . ';', LOCK_EX);
    rename($tmp, $cachePath . $key);
 }