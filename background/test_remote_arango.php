<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
require_once $rootPath . 'background/logging.php';

use Monolog\Logger;
use Medoo\Medoo;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use ArangoDBClient\Connection;
use ArangoDBClient\ConnectionOptions;
use ArangoDBClient\Collection;
use ArangoDBClient\CollectionHandler;
use ArangoDBClient\Document;
use ArangoDBClient\DocumentHandler;
use ArangoDBClient\UpdatePolicy;
use ArangoDBClient\Statement;

// ArangoDb section
$connectionOptions = [
    // server endpoint to connect to
    ConnectionOptions::OPTION_ENDPOINT => 'tcp://tanpa.download:8529',
    // authorization type to use (currently supported: 'Basic')
    ConnectionOptions::OPTION_AUTH_TYPE => 'Basic',
    // user for basic authorization
    ConnectionOptions::OPTION_AUTH_USER => $dbuser,
    // password for basic authorization
    ConnectionOptions::OPTION_AUTH_PASSWD => $dbpass,
    // connection persistence on server. can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
    ConnectionOptions::OPTION_CONNECTION => 'Close',
    // connect timeout in seconds
    ConnectionOptions::OPTION_TIMEOUT => 60,
    // whether or not to reconnect when a keep-alive connection has timed out on server
    ConnectionOptions::OPTION_RECONNECT => true,
    // optionally create new collections when inserting documents
    ConnectionOptions::OPTION_CREATE => true,
    // optionally create new collections when inserting documents
    ConnectionOptions::OPTION_UPDATE_POLICY => UpdatePolicy::LAST,
    ConnectionOptions::OPTION_DATABASE => 'automateit',
];
 
// open connection
//$connection = new Connection($connectionOptions);
$connection = new Connection($connectionOptions);

$query = 'FOR d IN locations FILTER d._key == "0" LIMIT 1 ';
$query = $query . 'RETURN {id:d._id, fbplacesid:d.fbplacesid, pk:d.pk, name:d.name, shortname:d.shortname, lat:d.lat, lng:d.lng}';

$statement = new Statement(
    $connection,
    [
        'query' => $query,
        'count' => true,
        'batchSize' => 1,
        'sanitize' => true
    ]
    );
    
$cursor = $statement->execute();
if (count($cursor->getAll()) > 0) {
    //return $cursor->getAll()[0]->id;
    $d = $cursor->getAll()[0];
    print_r($d);
}