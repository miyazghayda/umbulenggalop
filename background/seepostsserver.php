<?php
$rootPath = '/var/www/html/automateit/';

require_once $rootPath . 'vendor/autoload.php';
require_once $rootPath . 'background/logging.php';

use Monolog\Logger;
use Medoo\Medoo;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use ArangoDBClient\Connection;
use ArangoDBClient\ConnectionOptions;
use ArangoDBClient\Collection;
use ArangoDBClient\CollectionHandler;
use ArangoDBClient\Document;
use ArangoDBClient\DocumentHandler;
use ArangoDBClient\UpdatePolicy;
use ArangoDBClient\Statement;

$dbhost = 'localhost';
$dbname = 'automateit';
$dbuser = 'user';
$dbpass = 'jayapura';
$dbtable = 'logs';
$dbcharset = 'utf8mb4';
$arangoHost = '206.189.46.73';

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $dbname,
    'server' => $dbhost,
    'username' => $dbuser,
    'password' => $dbpass,
    'charset' => $dbcharset,
]);

$log = new Logger('scrapehashtagposts');
$log->pushHandler($mySQLHandler);
$account_id = 1; // mandatory
$scriptid = 2; // mandatory, scrapehashtagposts

$connection = new AMQPStreamConnection('localhost', 5672, 'radmin', 'radmin');
$channel = $connection->channel();
$channel->queue_declare('seeposts', false, true, false, false);

// ArangoDb section
$connectionOptions = [
    // server endpoint to connect to
    ConnectionOptions::OPTION_ENDPOINT => 'tcp://' . $arangoHost . ':8529',
    // authorization type to use (currently supported: 'Basic')
    ConnectionOptions::OPTION_AUTH_TYPE => 'Basic',
    // user for basic authorization
    ConnectionOptions::OPTION_AUTH_USER => $dbuser,
    // password for basic authorization
    ConnectionOptions::OPTION_AUTH_PASSWD => $dbpass,
    // connection persistence on server. can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
    ConnectionOptions::OPTION_CONNECTION => 'Close',
    // connect timeout in seconds
    ConnectionOptions::OPTION_TIMEOUT => 60,
    // whether or not to reconnect when a keep-alive connection has timed out on server
    ConnectionOptions::OPTION_RECONNECT => true,
    // optionally create new collections when inserting documents
    ConnectionOptions::OPTION_CREATE => true,
    // optionally create new collections when inserting documents
    ConnectionOptions::OPTION_UPDATE_POLICY => UpdatePolicy::LAST,
    ConnectionOptions::OPTION_DATABASE => 'automateit',
];
 
// open connection
//$connection = new Connection($connectionOptions);

$callback = function ($msg) {
    global $rootPath;

    $t = $msg->body;
    $filename = $rootPath . 'tmp/cache/seeposts/' . trim($t);
    if (file_exists($filename)) {
        $val = cache_get($filename);
        if ($val != false) {
            $location_default = defaultLocation();

            $medias = json_decode($val['medias']);
            // to store new data, to update cache file later
            foreach ($medias as $media) {
                $location = $location_default;
                if (isset($media->location)) {
                    $location = insertLocation($media->location);
                    //echo $location_id . PHP_EOL;
                }

                $member = insertMember($media->user);

                $post_id = insertPost($media, $location, $member);
                echo $post_id . PHP_EOL;
            }
        }
    }
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};

$channel->basic_qos(null, 1, null);
$channel->basic_consume('seeposts', '', false, false, false, false, $callback);

while (count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();

function cache_get($cachePath) {
    @include $cachePath;
    return isset($val) ? $val : false;
}

function cache_set($key, $val) {
    global $rootPath;
    $cachePath = $rootPath . 'tmp/cache/locations/';
    $val = var_export($val, true);
    // HHVM fails at __set_state, so just use object cast for now
    $val = str_replace('stdClass::__set_state', '(object)', $val);
    // Write to temp file first to ensure atomicity
    $tmp = $cachePath . $key . '.' . uniqid('', true) . '.tmp';
    file_put_contents($tmp, '<?php $val = ' . $val . ';', LOCK_EX);
    rename($tmp, $cachePath . $key);
}

function defaultLocation() {
    global $connectionOptions;
    $connection = new Connection($connectionOptions);

    $query = 'FOR d IN locations FILTER d._key == "0" LIMIT 1 ';
    $query = $query . 'RETURN {id:d._id, fbplacesid:d.fbplacesid, pk:d.pk, name:d.name, shortname:d.shortname, lat:d.lat, lng:d.lng}';

    $statement = new Statement(
        $connection,
        [
            'query' => $query,
            'count' => true,
            'batchSize' => 1,
            'sanitize' => true
        ]
        );
        
    $cursor = $statement->execute();
    if (count($cursor->getAll()) > 0) {
        //return $cursor->getAll()[0]->id;
        $d = $cursor->getAll()[0];
        return [
            'id' => $d->id,
            'pk' => $d->pk,
            'fbplacesid' => $d->fbplacesid,
            'name' => $d->name,
            'shortname' => $d->shortname,
            'lat' => $d->lat,
            'lng' => $d->lng
        ];
    }
}

function insertPost($datum = null, $location = null, $member = null) {
    global $connectionOptions;

    $connection = new Connection($connectionOptions);

    $query = 'FOR d IN posts FILTER d._key == "' . $datum->pk . '" LIMIT 1 RETURN {id:d._id}';
    $statement = new Statement(
        $connection,
        [
            'query' => $query,
            'count' => true,
            'batchSize' => 1,
            'sanitize' => true
        ]
        );
        
    $cursor = $statement->execute();
    if (count($cursor->getAll()) > 0) {
        return $cursor->getAll()[0]->id;
    } else {
        $cap = '';
        if (isset($datum->caption)) $cap = $datum->caption->text;
        $cap = addslashes($cap);

        $q = 'UPSERT {_key:"' . $datum->pk . '"} ';
        $q = $q . 'INSERT {';
        $q = $q . '_key:"' . $datum->pk . '",';
        $q = $q . 'pk:"' . $datum->pk . '",';
        $q = $q . 'sourceid:"' . $datum->id . '",';
        $q = $q . 'caption:"' . $cap . '",';
        $q = $q . 'typeid:' . $datum->media_type . ',';
        $q = $q . 'likes:' . $datum->like_count . ',';
        $q = $q . 'comments:' . $datum->comment_count . ',';
        $q = $q . 'takenat:' . (int)$datum->taken_at . ',';
        $q = $q . 'location:"' . $location['id'] . '",';
        $q = $q . 'member:"' . $member['id'] . '",';
        $q = $q . 'location_resume:{';
        $q = $q . 'fbplacesid:"' . $location['fbplacesid'] . '",';
        $q = $q . 'pk:"' . $location['pk']. '",';
        $q = $q . 'lat:' . $location['lat'] . ',';
        $q = $q . 'lng:' . $location['lng'] . ',';
        $q = $q . 'name:"' . $location['name'] . '",';
        $q = $q . 'shortname:"' . $location['shortname'] . '"';
        $q = $q . '},';
        $q = $q . 'member_resume:{';
        $q = $q . 'pk:"' . $member['pk'] . '",';
        $q = $q . 'username:"' . addslashes($member['username']) . '"';
        $q = $q . '},';

        // Insert Wads
        if ($datum->media_type == 1) {// photo
            $q = $q . 'wads:[{';
            $q = $q . 'url:"' . $datum->image_versions2->candidates[0]->url . '",';
            $q = $q . 'width:' . $datum->image_versions2->candidates[0]->width . ',';
            $q = $q . 'height:' . $datum->image_versions2->candidates[0]->height . ',';
            $q = $q . 'sequence:0,';
            $q = $q . 'typeid:1';
            $q = $q . '}],';
        } elseif ($datum->media_type == 2) {//video
            $q = $q . 'wads:[{';
            $q = $q . 'url:"' . $datum->video_versions[0]->url . '",';
            $q = $q . 'width:' . $datum->video_versions[0]->width . ',';
            $q = $q . 'height:' . $datum->video_versions[0]->height . ',';
            $q = $q . 'sequence:0,';
            $q = $q . 'typeid:2';
            $q = $q . '}],';
        } elseif ($datum->media_type == 8) {//carousel
            $q = $q . 'wads:[';

            $sequence = 0;
            foreach ($datum->carousel_media as $cr) {
                $q = $q . '{';
                if ($cr->media_type == 1) {// photo
                    $q = $q . 'url:"' . $cr->image_versions2->candidates[0]->url . '",';
                    $q = $q . 'width:' . $cr->image_versions2->candidates[0]->width . ',';
                    $q = $q . 'height:' . $cr->image_versions2->candidates[0]->height . ',';
                    $q = $q . 'sequence:' . $sequence . ',';
                    $q = $q . 'typeid:1';
                } elseif ($cr->media_type == 2) {// video
                    $q = $q . 'url:"' . $cr->video_versions[0]->url . '",';
                    $q = $q . 'width:' . $cr->video_versions[0]->width . ',';
                    $q = $q . 'height:' . $cr->video_versions[0]->height . ',';
                    $q = $q . 'sequence:' . $sequence . ',';
                    $q = $q . 'typeid:2';
                }
                $q = $q . '},';
                $sequence++;
            }

            $q = rtrim($q, ',');
            $q = $q . '],';
        }

        $q = $q . 'active: 1';
        $q = $q . '} UPDATE {';
        $q = $q . 'likes:' . $datum->like_count . ',';
        $q = $q . 'comments:' . $datum->comment_count . ',';

        // Insert Wads
        if ($datum->media_type == 1) {// photo
            $q = $q . 'wads:[{';
            $q = $q . 'url:"' . $datum->image_versions2->candidates[0]->url . '",';
            $q = $q . 'width:' . $datum->image_versions2->candidates[0]->width . ',';
            $q = $q . 'height:' . $datum->image_versions2->candidates[0]->height . ',';
            $q = $q . 'sequence:0,';
            $q = $q . 'typeid:1';
            $q = $q . '}]';
        } elseif ($datum->media_type == 2) {//video
            $q = $q . 'wads:[{';
            $q = $q . 'url:"' . $datum->video_versions[0]->url . '",';
            $q = $q . 'width:' . $datum->video_versions[0]->width . ',';
            $q = $q . 'height:' . $datum->video_versions[0]->height . ',';
            $q = $q . 'sequence:0,';
            $q = $q . 'typeid:2';
            $q = $q . '}]';
        } elseif ($datum->media_type == 8) {//carousel
            $q = $q . 'wads:[';

            $sequence = 0;
            foreach ($datum->carousel_media as $cr) {
                $q = $q . '{';
                if ($cr->media_type == 1) {// photo
                    $q = $q . 'url:"' . $cr->image_versions2->candidates[0]->url . '",';
                    $q = $q . 'width:' . $cr->image_versions2->candidates[0]->width . ',';
                    $q = $q . 'height:' . $cr->image_versions2->candidates[0]->height . ',';
                    $q = $q . 'sequence:' . $sequence . ',';
                    $q = $q . 'typeid:1';
                } elseif ($cr->media_type == 2) {// video
                    $q = $q . 'url:"' . $cr->video_versions[0]->url . '",';
                    $q = $q . 'width:' . $cr->video_versions[0]->width . ',';
                    $q = $q . 'height:' . $cr->video_versions[0]->height . ',';
                    $q = $q . 'sequence:' . $sequence . ',';
                    $q = $q . 'typeid:2';
                }
                $q = $q . '},';
                $sequence++;
            }

            $q = rtrim($q, ',');
            $q = $q . ']';
        }

        $q = $q . '} IN posts';
        echo $q . PHP_EOL;
        $statement = new Statement(
            $connection,
            [
                'query' => $q,
                'count' => true,
                'batchSize' => 1,
                'sanitize' => true
            ]);
            
        $c = $statement->execute();
        if (count($c->getAll()) > 0) {
            return $c->getAll()[0]->id;
        } else {
            return false;
        }
    } 
}

function insertMember($datum = null) {
    global $connectionOptions;

    $connection = new Connection($connectionOptions);

    $query = 'FOR d IN members FILTER d._key == "' . $datum->pk . '" LIMIT 1 RETURN {id:d._id, pk:d.pk, username:d.username}';
    $statement = new Statement(
        $connection,
        [
            'query' => $query,
            'count' => true,
            'batchSize' => 1,
            'sanitize' => true
        ]
        );
       
    //print_r($datum);
    $cursor = $statement->execute();
    if (count($cursor->getAll()) > 0) {
        $d = $cursor->getAll()[0];
        return [
            'id' => $d->id,
            'pk' => $d->pk,
            'username' => $d->username
        ];
        //return $cursor->getAll()[0]->id;
    } else {
        $priv = 0;
        if ($datum->is_private) $priv = 1;
        $q = 'UPSERT {_key:"' . $datum->pk . '"} ';
        $q = $q . 'INSERT {';
        $q = $q . '_key:"' . $datum->pk . '",';
        $q = $q . 'pk:"' . $datum->pk . '",';
        $q = $q . 'username:"' . addslashes($datum->username) . '",';
        $q = $q . 'fullname:"' . addslashes($datum->full_name) . '",';
        $q = $q . 'description:"",';
        $q = $q . 'profpicurl:"' . $datum->profile_pic_url . '",';
        $q = $q . 'followers:0,';
        $q = $q . 'followings:0,';
        $q = $q . 'contents:0,';
        $q = $q . 'closed:' . $priv . ',';
        $q = $q . 'profpicurlfixed:0,';
        $q = $q . 'genderdetection:0,';
        $q = $q . 'genderdetected:0,';
        $q = $q . 'active: 1';
        $q = $q . '} UPDATE {';
        $q = $q . 'fullname:"' . addslashes($datum->full_name) . '",';
        $q = $q . 'profpicurl:"' . $datum->profile_pic_url . '",';
        $q = $q . 'closed:' . $priv;
        $q = $q . '} IN members LET r = NEW RETURN {id:r._id,pk:r.pk,username:r.username}';
        $statement = new Statement(
            $connection,
            [
                'query' => $q,
                'count' => true,
                'batchSize' => 1,
                'sanitize' => true
            ]);
            
        $c = $statement->execute();
        if (count($c->getAll()) > 0) {
            $d = $c->getAll()[0];
            return [
                'id' => $d->id,
                'pk' => $d->pk,
                'username' => $d->username
            ];
            //return $c->getAll()[0]->id;
        } else {
            return false;
        }
    }
}// insert member

function insertLocation($datum = null) {
    global $connectionOptions;

    $connection = new Connection($connectionOptions);

    $query = 'FOR d IN locations FILTER d._key == "' . $datum->facebook_places_id . '" LIMIT 1 ';
    $query = $query . 'RETURN {id:d._id, fbplacesid:d.fbplacesid, pk:d.pk, name:d.name, shortname:d.shortname, lat:d.lat, lng:d.lng}';
    $statement = new Statement(
        $connection,
        [
            'query' => $query,
            'count' => true,
            'batchSize' => 1,
            'sanitize' => true
        ]
        );
        
    $cursor = $statement->execute();
    if (count($cursor->getAll()) > 0) {
        $d = $cursor->getAll()[0];
        return [
            'id' => $d->id,
            'pk' => $d->pk,
            'fbplacesid' => $d->fbplacesid,
            'name' => $d->name,
            'shortname' => $d->shortname,
            'lat' => $d->lat,
            'lng' => $d->lng
        ];
    } else {
        $q = 'INSERT {';
        $q = $q . '_key:"' . $datum->facebook_places_id . '",';
        $q = $q . 'fbplacesid:"' . $datum->facebook_places_id . '",';
        $q = $q . 'pk:"' . $datum->pk . '",';
        $q = $q . 'name:"' . $datum->name . '",';
        $q = $q . 'shortname:"' . $datum->short_name . '",';
        $q = $q . 'address:"' . $datum->address . '",';
        $q = $q . 'lat:' . $datum->lat . ',';
        $q = $q . 'lng:' . $datum->lng . ',';
        $q = $q . 'active: 1';

        //$q = $q . '} INTO locations OPTIONS {overwrite:true}';
        $q = $q . '} INTO locations LET d = NEW RETURN ';
        $q = $q . '{id:d._id, fbplacesid:d.fbplacesid, pk:d.pk, name:d.name, shortname:d.shortname, lat:d.lat, lng:d.lng}';
        $statement = new Statement(
            $connection,
            [
                'query' => $q,
                'count' => true,
                'batchSize' => 1,
                'sanitize' => true
            ]);
            
        $c = $statement->execute();
        if (count($c->getAll()) > 0) {
            $d = $c->getAll()[0];
            return [
                'id' => $d->id,
                'pk' => $d->pk,
                'fbplacesid' => $d->fbplacesid,
                'name' => $d->name,
                'shortname' => $d->shortname,
                'lat' => $d->lat,
                'lng' => $d->lng
            ];
        } else {
            return false;
        }
    }
}// insert location