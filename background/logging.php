<?php
$rootPath = '/var/www/html/automateit/';
//$rootPath = '../';
require_once $rootPath . 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use MySQLHandler\MySQLHandler;
use Medoo\Medoo;

$dbhost = 'localhost';
$dbname = 'automateit';
$dbuser = 'user';
$dbpass = 'jayapura';
$dbtable = 'logs';
$dbcharset = 'utf8mb4';

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $dbname,
    'server' => $dbhost,
    'username' => $dbuser,
    'password' => $dbpass,
    'charset' => $dbcharset,
]);

$pdo = new PDO('mysql:host=' . $dbhost . ';dbname=' . $dbname . ';charset=' . $dbcharset, $dbuser, $dbpass);
$mySQLHandler = new MySQLHandler($pdo, $dbtable, ['account_id', 'created', 'scriptid'], Logger::DEBUG);

function myErrorHandler($errno, $errstr, $errfile, $errline)
{
    if (!(error_reporting() & $errno)) {
        // This error code is not included in error_reporting, so let it fall
        // through to the standard PHP error handler
        return false;
    }

    global $account_id;
    global $scriptid;
    global $log;

    switch ($errno) {
    case E_USER_ERROR:
        $log->error($errstr, ['account_id' => $account_id, 'created' => date('Y-m-d H:i:s'), 'scriptid' => $scriptid]);
        break;

    case E_USER_WARNING:
        $log->warning($errstr, ['account_id' => $account_id, 'created' => date('Y-m-d H:i:s'), 'scriptid' => $scriptid]);
        break;

    case E_USER_NOTICE:
        $log->notice($errstr, ['account_id' => $account_id, 'created' => date('Y-m-d H:i:s'), 'scriptid' => $scriptid]);
        break;

    default:
        $log->error('FILE: ' . $errfile . ' LINE: ' . $errline . ' MESSAGE: ' . $errstr, ['account_id' => $account_id, 'created' => date('Y-m-d H:i:s'), 'scriptid' => $scriptid]);
        break;
    }

    /* Don't execute PHP internal error handler */
    return true;
}

$old_error_handler = set_error_handler("myErrorHandler");