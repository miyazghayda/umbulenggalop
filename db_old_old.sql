-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 16, 2018 at 10:55 AM
-- Server version: 5.6.31-0ubuntu0.14.04.2
-- PHP Version: 7.2.8-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


--
-- Database: `umbulenggal`
--

-- --------------------------------------------------------

--
-- Table structure for table `accountlists`
--

CREATE TABLE `accountlists` (
  `id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `filter_id` bigint(20) NOT NULL DEFAULT '1',
  `celebrity_id` bigint(20) NOT NULL DEFAULT '1',
  `vassal_id` bigint(20) NOT NULL DEFAULT '1',
  `typeid` int(11) NOT NULL COMMENT '1 idol 2 blacklist',
  `proffixed` tinyint(1) NOT NULL DEFAULT '0',
  `allfollowersaved` tinyint(1) DEFAULT '0',
  `nextmaxid` varchar(1000) COLLATE utf8_bin NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT '1',
  `proxy_id` int(11) NOT NULL DEFAULT '1',
  `pk` bigint(20) NOT NULL,
  `profpicurl` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_bin NOT NULL,
  `fullname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin,
  `followers` int(11) NOT NULL,
  `followings` int(11) NOT NULL,
  `contents` int(11) NOT NULL,
  `started` date DEFAULT NULL,
  `ended` date DEFAULT NULL,
  `paid` tinyint(1) NOT NULL DEFAULT '0',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `statusid` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 need to test login 2 test login failed 3 test login success 4 unpaid 5 paid 6 paused by user',
  `note` text COLLATE utf8_bin,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `profpicurlfixed` tinyint(1) NOT NULL DEFAULT '0',
  `messy` char(32) COLLATE utf8_bin DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `cargos`
--

CREATE TABLE `cargos` (
  `id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `location_id` bigint(20) NOT NULL DEFAULT '1',
  `typeid` tinyint(4) NOT NULL COMMENT '1 photo 2 video 3 carousel 4 story',
  `schedule` datetime NOT NULL,
  `uploaded` tinyint(1) NOT NULL DEFAULT '0',
  `caption` text COLLATE utf8_bin NOT NULL,
  `question` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `answery` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `answern` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `hashtags` text COLLATE utf8_bin,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `celebrities`
--

CREATE TABLE `celebrities` (
  `id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `followers` int(11) NOT NULL DEFAULT '0',
  `followersaved` int(11) NOT NULL DEFAULT '0',
  `allfollowersaved` tinyint(1) NOT NULL DEFAULT '0',
  `nextmaxid` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `commentinglists`
--

CREATE TABLE `commentinglists` (
  `id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `typeid` tinyint(4) NOT NULL COMMENT '1 feed 2 hashtag 3 location 4 new follower',
  `commented` tinyint(1) NOT NULL DEFAULT '1',
  `uncommented` tinyint(1) NOT NULL DEFAULT '0',
  `commentedat` datetime DEFAULT NULL,
  `uncommentedat` datetime DEFAULT NULL,
  `who` tinyint(1) NOT NULL COMMENT '0 bot 1 user',
  `caption` text COLLATE utf8_bin NOT NULL,
  `note` text COLLATE utf8_bin NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `compositions`
--

CREATE TABLE `compositions` (
  `id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `typeid` smallint(6) NOT NULL DEFAULT '1' COMMENT '1 comment 2 dm',
  `caption` text COLLATE utf8_bin,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Stand-in structure for view `fellows`
--
CREATE TABLE `fellows` (
`id` bigint(20)
,`pk` bigint(20)
,`username` varchar(255)
,`fullname` varchar(255)
,`description` text
,`profpicurl` varchar(1000)
,`followers` int(11)
,`followings` int(11)
,`contents` int(11)
,`closed` tinyint(1)
,`created` timestamp
,`modified` timestamp
,`active` tinyint(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `filters`
--

CREATE TABLE `filters` (
  `id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `typeid` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 follow by hashtag 2 follow idol followers',
  `hashtagblacklist` text COLLATE utf8_bin COMMENT 'follow by hashtags filtering hashtags',
  `fullnameblacklist` text COLLATE utf8_bin COMMENT 'follow idol followers filtering by fullname',
  `biographyblacklist` text COLLATE utf8_bin COMMENT 'follow idol followers filtering by biography',
  `nmonths` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'follow idol followers filtering by last n month active',
  `accounttypeid` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'account 1 public and private 2 only public 3 only private',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `followinglists`
--

CREATE TABLE `followinglists` (
  `id` bigint(20) NOT NULL,
  `vassal_id` bigint(20) NOT NULL DEFAULT '1',
  `member_id` bigint(20) NOT NULL DEFAULT '1',
  `fellow_id` bigint(20) NOT NULL DEFAULT '1',
  `typeid` tinyint(4) NOT NULL COMMENT '1 someone followers 2 hashtag 3 location',
  `followed` tinyint(1) NOT NULL DEFAULT '0',
  `unfollowed` tinyint(1) NOT NULL DEFAULT '0',
  `followedat` datetime DEFAULT NULL,
  `unfollowedat` datetime DEFAULT NULL,
  `who` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 bot 1 user',
  `account_id` bigint(20) NOT NULL,
  `note` text COLLATE utf8_bin NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `hashtaglists`
--

CREATE TABLE `hashtaglists` (
  `id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `filter_id` bigint(20) NOT NULL DEFAULT '1',
  `typeid` tinyint(4) NOT NULL COMMENT '1 follow 2 like',
  `whitelist` tinyint(1) NOT NULL DEFAULT '1',
  `caption` varchar(255) COLLATE utf8_bin NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `likinglists`
--

CREATE TABLE `likinglists` (
  `id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `typeid` tinyint(4) NOT NULL COMMENT '1 feed 2 hashtag 3 location 4 new follower',
  `liked` tinyint(1) NOT NULL DEFAULT '1',
  `unliked` tinyint(1) NOT NULL DEFAULT '0',
  `likedat` datetime DEFAULT NULL,
  `unlikedat` datetime DEFAULT NULL,
  `who` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 bot 1 user',
  `note` text COLLATE utf8_bin NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `locationlists`
--

CREATE TABLE `locationlists` (
  `id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `whitelist` tinyint(1) NOT NULL DEFAULT '1',
  `caption` varchar(255) COLLATE utf8_bin NOT NULL,
  `typeid` tinyint(4) NOT NULL COMMENT '1 follow 2 like',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` bigint(20) NOT NULL,
  `pk` bigint(20) NOT NULL,
  `fbplacesid` bigint(20) NOT NULL DEFAULT '0',
  `lat` decimal(10,8) NOT NULL,
  `lng` decimal(11,8) NOT NULL,
  `address` text COLLATE utf8_bin NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL,
  `shortname` varchar(255) COLLATE utf8_bin NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `pk`, `fbplacesid`, `lat`, `lng`, `address`, `name`, `shortname`, `active`) VALUES
(1, 0, 0, '0.00000000', '0.00000000', 'Tanpa Alamat', 'Tanpa Nama', 'Tanpa Nama', 1);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` bigint(20) NOT NULL,
  `pk` bigint(20) NOT NULL,
  `username` varchar(255) COLLATE utf8_bin NOT NULL,
  `fullname` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin,
  `profpicurl` varchar(1000) COLLATE utf8_bin NOT NULL,
  `followers` int(11) NOT NULL DEFAULT '0',
  `followings` int(11) NOT NULL DEFAULT '0',
  `contents` int(11) NOT NULL DEFAULT '0',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `profpicurlfixed` tinyint(1) NOT NULL DEFAULT '0',
  `genderdetection` tinyint(1) NOT NULL DEFAULT '0',
  `genderdetected` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) NOT NULL,
  `pk` bigint(20) NOT NULL,
  `sourceid` varchar(1000) COLLATE utf8_bin NOT NULL,
  `location_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `typeid` tinyint(4) NOT NULL COMMENT '1 photo 2 video 8 carousel 4 story',
  `caption` text COLLATE utf8_bin,
  `likes` int(11) NOT NULL,
  `comments` int(11) NOT NULL,
  `takenat` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `preferences`
--

CREATE TABLE `preferences` (
  `id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `maxlikeperday` int(11) NOT NULL DEFAULT '0',
  `maxcommentperday` int(11) NOT NULL DEFAULT '0',
  `maxfollowperday` int(11) NOT NULL DEFAULT '0',
  `maxpostperday` tinyint(4) NOT NULL DEFAULT '0',
  `unfollowbydefault` tinyint(1) NOT NULL DEFAULT '1',
  `maxunfollowperday` int(11) NOT NULL DEFAULT '0',
  `maxdmperday` smallint(6) NOT NULL DEFAULT '30',
  `dmbynewfollowing` tinyint(1) NOT NULL DEFAULT '0',
  `dmtoday` smallint(6) NOT NULL DEFAULT '0',
  `newfollowertodm` smallint(6) NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `followidolfollower` tinyint(1) NOT NULL DEFAULT '0',
  `followbyhashtag` tinyint(1) NOT NULL DEFAULT '0',
  `followbylocation` tinyint(1) NOT NULL DEFAULT '0',
  `likefeed` tinyint(1) NOT NULL DEFAULT '0',
  `likebyhashtag` tinyint(1) NOT NULL DEFAULT '0',
  `likebylocation` tinyint(1) NOT NULL DEFAULT '0',
  `likebynewfollowing` tinyint(1) NOT NULL DEFAULT '0',
  `commentbynewfollowing` tinyint(1) NOT NULL DEFAULT '0',
  `commentfeed` tinyint(1) NOT NULL DEFAULT '0',
  `commentbyhashtag` tinyint(1) NOT NULL DEFAULT '0',
  `commentbylocation` tinyint(1) NOT NULL DEFAULT '0',
  `followtoday` smallint(6) NOT NULL DEFAULT '0',
  `unfollowtoday` smallint(6) NOT NULL DEFAULT '0',
  `liketoday` smallint(6) NOT NULL DEFAULT '0',
  `commenttoday` smallint(6) NOT NULL DEFAULT '0',
  `posttoday` smallint(6) NOT NULL DEFAULT '0',
  `hashtagtofollowtoday` smallint(6) NOT NULL DEFAULT '0',
  `gethashtagtofollowtoday` tinyint(1) NOT NULL DEFAULT '1',
  `idolfollowertofollowtoday` smallint(6) NOT NULL DEFAULT '0',
  `getidolfollowertofollowtoday` tinyint(1) NOT NULL DEFAULT '1',
  `followbatch` smallint(6) NOT NULL DEFAULT '0',
  `newfollowerposttolike` smallint(6) NOT NULL DEFAULT '0',
  `newfollowerposttocomment` smallint(6) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `proxies`
--

CREATE TABLE `proxies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '0.0.0.0',
  `username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `proxies`
--

INSERT INTO `proxies` (`id`, `name`, `username`, `password`, `active`) VALUES
(1, '0.0.0.0', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reaps`
--

CREATE TABLE `reaps` (
  `id` bigint(20) NOT NULL,
  `cargo_id` bigint(20) NOT NULL,
  `typeid` tinyint(4) NOT NULL COMMENT '1 photo 2 video',
  `extension` varchar(255) COLLATE utf8_bin NOT NULL,
  `sequence` tinyint(4) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `replicas`
--

CREATE TABLE `replicas` (
  `id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL DEFAULT '1',
  `member_id` bigint(20) NOT NULL DEFAULT '1',
  `post_id` bigint(20) NOT NULL,
  `location_id` bigint(20) NOT NULL DEFAULT '1',
  `typeid` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 photo 2 video 8 carousel 4 story',
  `takenat` int(11) NOT NULL DEFAULT '0',
  `caption` text COLLATE utf8_bin,
  `schedule` datetime NOT NULL,
  `uploaded` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `replicatinglists`
--

CREATE TABLE `replicatinglists` (
  `id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL DEFAULT '1',
  `member_id` bigint(20) NOT NULL DEFAULT '1',
  `username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `memberfixed` tinyint(1) NOT NULL DEFAULT '0',
  `contents` int(11) NOT NULL DEFAULT '0',
  `loads` int(11) NOT NULL DEFAULT '0',
  `replicated` tinyint(1) NOT NULL DEFAULT '0',
  `replicatedat` datetime DEFAULT NULL,
  `maxid` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `note` text COLLATE utf8_bin,
  `uploadat` time DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `username` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `fullname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `groupid` tinyint(4) NOT NULL COMMENT '1 admin 2 manager 3 user',
  `statusid` tinyint(4) NOT NULL COMMENT '1 registered 2 activated 3 banned 4 deleted',
  `lastlog` datetime NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `vassals`
--

CREATE TABLE `vassals` (
  `id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `fellow_id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` bigint(20) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `wads`
--

CREATE TABLE `wads` (
  `id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `typeid` tinyint(4) NOT NULL DEFAULT '1',
  `sequence` tinyint(4) NOT NULL,
  `url` varchar(1000) COLLATE utf8_bin NOT NULL,
  `width` smallint(6) NOT NULL,
  `height` smallint(6) NOT NULL,
  `urlfixed` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure for view `fellows`
--
DROP TABLE IF EXISTS `fellows`;

CREATE ALGORITHM=UNDEFINED DEFINER=`miyazghayda`@`%` SQL SECURITY DEFINER VIEW `fellows`  AS  select `members`.`id` AS `id`,`members`.`pk` AS `pk`,`members`.`username` AS `username`,`members`.`fullname` AS `fullname`,`members`.`description` AS `description`,`members`.`profpicurl` AS `profpicurl`,`members`.`followers` AS `followers`,`members`.`followings` AS `followings`,`members`.`contents` AS `contents`,`members`.`closed` AS `closed`,`members`.`created` AS `created`,`members`.`modified` AS `modified`,`members`.`active` AS `active` from `members` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accountlists`
--
ALTER TABLE `accountlists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `celebrities`
--
ALTER TABLE `celebrities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commentinglists`
--
ALTER TABLE `commentinglists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `compositions`
--
ALTER TABLE `compositions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filters`
--
ALTER TABLE `filters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `followinglists`
--
ALTER TABLE `followinglists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hashtaglists`
--
ALTER TABLE `hashtaglists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likinglists`
--
ALTER TABLE `likinglists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locationlists`
--
ALTER TABLE `locationlists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preferences`
--
ALTER TABLE `preferences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proxies`
--
ALTER TABLE `proxies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reaps`
--
ALTER TABLE `reaps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `replicas`
--
ALTER TABLE `replicas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `replicatinglists`
--
ALTER TABLE `replicatinglists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vassals`
--
ALTER TABLE `vassals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wads`
--
ALTER TABLE `wads`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accountlists`
--
ALTER TABLE `accountlists`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cargos`
--
ALTER TABLE `cargos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `celebrities`
--
ALTER TABLE `celebrities`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `commentinglists`
--
ALTER TABLE `commentinglists`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `compositions`
--
ALTER TABLE `compositions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `filters`
--
ALTER TABLE `filters`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `followinglists`
--
ALTER TABLE `followinglists`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hashtaglists`
--
ALTER TABLE `hashtaglists`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `likinglists`
--
ALTER TABLE `likinglists`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `locationlists`
--
ALTER TABLE `locationlists`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `preferences`
--
ALTER TABLE `preferences`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `proxies`
--
ALTER TABLE `proxies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `reaps`
--
ALTER TABLE `reaps`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `replicas`
--
ALTER TABLE `replicas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `replicatinglists`
--
ALTER TABLE `replicatinglists`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vassals`
--
ALTER TABLE `vassals`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wads`
--
ALTER TABLE `wads`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
DELIMITER $$
--
-- Events
--
CREATE DEFINER=`user`@`localhost` EVENT `resetdailyfollowbyhashtag` ON SCHEDULE EVERY 1 DAY STARTS '2018-01-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE preferences SET hashtagtofollowtoday = 0, gethashtagtofollowtoday = true WHERE followbyhashtag = true AND active = true$$

CREATE DEFINER=`user`@`localhost` EVENT `resetdailyunfollowbydefault` ON SCHEDULE EVERY 1 DAY STARTS '2018-01-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE preferences SET preferences.followtoday = 0, preferences.unfollowtoday = 0 WHERE preferences.unfollowbydefault = 1 AND preferences.active = 1$$

CREATE DEFINER=`user`@`localhost` EVENT `resetdailyfollowidolfollower` ON SCHEDULE EVERY 1 DAY STARTS '2018-01-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE preferences SET preferences.idolfollowertofollowtoday = 0, preferences.getidolfollowertofollowtoday = 1 WHERE preferences.followidolfollower = 1 AND preferences.active = 1$$

CREATE DEFINER=`user`@`localhost` EVENT `resertdailycomment` ON SCHEDULE EVERY 1 DAY STARTS '2018-01-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE preferences SET preferences.commenttoday = 0, preferences.newfollowerposttocomment = 0 WHERE preferences.commenttoday > 0 AND preferences.active = 1$$

CREATE DEFINER=`user`@`localhost` EVENT `resertdailylike` ON SCHEDULE EVERY 1 DAY STARTS '2018-01-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE preferences SET preferences.liketoday = 0, preferences.newfollowerposttolike = 0 WHERE preferences.liketoday > 0 AND preferences.active = 1$$

CREATE DEFINER=`user`@`localhost` EVENT `resertdailydm` ON SCHEDULE EVERY 1 DAY STARTS '2018-01-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE preferences SET preferences.dmtoday = 0, preferences.newfollowertodm = 0 WHERE preferences.dmtoday > 0 AND preferences.active = 1$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
