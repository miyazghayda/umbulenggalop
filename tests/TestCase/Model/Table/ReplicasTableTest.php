<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReplicasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReplicasTable Test Case
 */
class ReplicasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReplicasTable
     */
    public $Replicas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.replicas',
        'app.accounts',
        'app.members',
        'app.posts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Replicas') ? [] : ['className' => ReplicasTable::class];
        $this->Replicas = TableRegistry::getTableLocator()->get('Replicas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Replicas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
