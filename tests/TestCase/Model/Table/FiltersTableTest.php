<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FiltersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FiltersTable Test Case
 */
class FiltersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FiltersTable
     */
    public $Filters;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.filters',
        'app.accounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Filters') ? [] : ['className' => FiltersTable::class];
        $this->Filters = TableRegistry::getTableLocator()->get('Filters', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Filters);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
