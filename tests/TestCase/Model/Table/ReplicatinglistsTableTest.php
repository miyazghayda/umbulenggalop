<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReplicatinglistsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReplicatinglistsTable Test Case
 */
class ReplicatinglistsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReplicatinglistsTable
     */
    public $Replicatinglists;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.replicatinglists',
        'app.accounts',
        'app.members'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Replicatinglists') ? [] : ['className' => ReplicatinglistsTable::class];
        $this->Replicatinglists = TableRegistry::getTableLocator()->get('Replicatinglists', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Replicatinglists);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
