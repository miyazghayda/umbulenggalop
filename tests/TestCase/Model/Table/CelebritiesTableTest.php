<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CelebritiesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CelebritiesTable Test Case
 */
class CelebritiesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CelebritiesTable
     */
    public $Celebrities;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.celebrities',
        'app.members',
        'app.accountlists'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Celebrities') ? [] : ['className' => CelebritiesTable::class];
        $this->Celebrities = TableRegistry::getTableLocator()->get('Celebrities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Celebrities);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
