#!/bin/sh
cd ~

sudo add-apt-repository ppa:ondrej/php
sudo apt-get update

echo "INSTALL WGET"
sudo apt-get install -y wget htop locales ffmpeg

echo "INSTALL UFW"
sudo apt-get install ufw
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow ssh
sudo ufw allow http
sudo ufw allow https
sudo ufw allow 8080
sudo ufw allow 6969
sudo ufw allow 3306
sudo ufw enable

echo "INSTALL PHP"
sudo apt-get install python-software-properties
sudo apt-get install php7.2 php7.2-cli php7.2-common php7.2-mysql php7.2-mbstring php7.2-mcrypt php7.2-curl php7.2-zip php7.2-fpm php7.2-gd php7.2-xml php7.2-dom php7.2-zip php7.2-intl

echo "INSTALL GIT"
sudo apt-get install git curl unzip

echo "INSTALL COMPOSER"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
php -r "unlink('composer-setup.php');"
sudo apt-get install composer

echo "INSTALL DOCKER"
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce
sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo pip install docker-compose
sudo groupadd docker
sudo usermod -a -G docker $USER

echo "CREATING DIRECTORIES"
cd ~
mkdir container-php
mkdir container-php/automateit
mkdir container-php/automateit/webroot
mkdir container-php/automateit2
mkdir container-php/src
mkdir container-php/src/webroot
mkdir container-php/docker
mkdir container-php/docker/database
mkdir container-php/docker/nginx
mkdir container-php/docker/nginx/conf.d
mkdir container-php/docker/nginx/sites
mkdir container-php/docker/php-fpm
mkdir container-php/docker/phpmyadmin

echo "AUTOMATEIT"
cd ~/container-php/automateit
git clone https://gitlab.com/miyazghayda/umbulenggalop
cp -R umbulenggalop/* ~/container-php/automateit
rm -rf ~/container-php/automateit/umbulenggalop
composer update
cd ~/container-php
mkdir ~/container-php/automateit/webroot
mkdir ~/container-php/automateit/webroot/files
mkdir ~/container-php/automateit/webroot/files/csv
mkdir ~/container-php/automateit/webroot/files/csv/accounts
mkdir ~/container-php/automateit/webroot/files/csv/celebrities
mkdir ~/container-php/automateit/webroot/files/images
mkdir ~/container-php/automateit/webroot/files/images/profilepicture
mkdir ~/container-php/automateit/webroot/files/images/upload
mkdir ~/container-php/automateit/webroot/files/images/upload/thumbnail
mkdir ~/container-php/automateit/webroot/files/images/upload/thumbnail2
mkdir ~/container-php/automateit/webroot/files/images/uploaded
mkdir ~/container-php/automateit/webroot/files/videos
mkdir ~/container-php/automateit/webroot/files/videos/upload
mkdir ~/container-php/automateit/webroot/files/videos/uploaded
chmod -R 777 ~/container-php/automateit

echo "AUTOMATEIT2"
cd ~/container-php/automateit2
git clone https://gitlab.com/miyazghayda/umbulenggalbgop
cp -R umbulenggalbgop/* ~/container-php/automateit2
rm -rf ~/container-php/automateit2/umbulenggalbgop
composer update

ln -fs /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
dpkg-reconfigure -f noninteractive tzdata
cd ~/container-php
mkdir ~/container-php/automateit2/files
mkdir ~/container-php/automateit2/files/images
mkdir ~/container-php/automateit2/files/images/upload
mkdir ~/container-php/automateit2/images
chmod -R 777 ~/container-php/automateit2

echo "DOWNLOAD DOCKERFILE"
wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/docker-compose.yml -O ~/container-php/docker/docker-compose.yml
wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/databaseDockerfile -O ~/container-php/docker/database/Dockerfile
wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/nginxDockerfile -O ~/container-php/docker/nginx/Dockerfile
wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/php-fpmDockerfile -O ~/container-php/docker/php-fpm/Dockerfile
wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/phpmyadminDockerfile -O ~/container-php/docker/phpmyadmin/Dockerfile
wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/nginx.conf -O ~/container-php/docker/nginx/nginx.conf
wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/default.conf -O ~/container-php/docker/nginx/conf.d/default.conf
wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/sites-default.conf -O ~/container-php/docker/nginx/sites/default.conf

echo "INSTALL TMUX"
sudo apt-get install -y automake build-essential pkg-config libevent-dev libncurses5-dev
rm -fr /tmp/tmux
git clone https://github.com/tmux/tmux.git /tmp/tmux
cd /tmp/tmux
sh autogen.sh
./configure && make
sudo make install
cd -
rm -fr /tmp/tmux
wget https://gist.githubusercontent.com/aansubarkah/6f5afb20d972232501b1/raw/b4059067db4acecf8014a9c71166785da5412706/tmux.conf -O ~/.tmux.conf

echo "INSTALL RUBY"
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt-get update
sudo apt-get install -y git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev nodejs yarn
cd ~
wget http://ftp.ruby-lang.org/pub/ruby/2.5/ruby-2.5.1.tar.gz
tar -xzvf ruby-2.5.1.tar.gz
cd ruby-2.5.1/
./configure
make
sudo make install
ruby -v

sudo apt-get install rbenv
sudo gem install bundler
sudo gem install teamocil
mkdir ~/.teamocil
wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/automateit.yml -O ~/.teamocil/automateit.yml

echo "LOCALE"
sudo locale-gen "en_US.UTF-8"
sudo update-locale LC_ALL="en_US.UTF-8"

echo "REBOOT"
sudo reboot