#!/bin/sh
cd ~

echo "LOCALE"
sudo locale-gen "en_US.UTF-8"
sudo update-locale LC_ALL="en_US.UTF-8"
export LC_ALL=en_US.UTF-8

sudo add-apt-repository ppa:ondrej/nginx-mainline
sudo add-apt-repository ppa:nijel/phpmyadmin
sudo apt-get update

echo "INSTALL WGET"
sudo apt-get install -y wget htop locales ffmpeg

echo "INSTALL UFW"
sudo apt-get install ufw
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow ssh
sudo ufw allow http
sudo ufw allow https
sudo ufw allow 8080
sudo ufw allow 6969
sudo ufw allow 3306
sudo ufw enable

echo "INSTALL PHP"
sudo apt-get install -y python-software-properties nginx mariadb-server mariadb-client
sudo mysql_secure_installation
sudo apt-get install -y phpmyadmin
sudo apt-get install -y php-pear php7.2 php7.2-cli php7.2-common php7.2-mysql php7.2-mbstring php7.2-curl php7.2-zip php7.2-fpm php7.2-gd php7.2-xml php7.2-dom php7.2-zip php7.2-intl
sudo cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default.old
sudo wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/default -O /etc/nginx/sites-available/default 
sudo ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default
sudo systemctl restart nginx

echo "INSTALL GIT"
sudo apt-get install git curl unzip 

echo "INSTALL COMPOSER"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
php -r "unlink('composer-setup.php');"
sudo apt-get install composer

echo "INSTALL DOCKER"
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common

echo "CREATING DIRECTORIES"
cd ~
sudo mkdir /var/www/html/automateit
sudo mkdir /var/www/html/automateit2
sudo chmod -R 777 /var/www/html/automateit
sudo chmod -R 777 /var/www/html/automateit2

echo "AUTOMATEIT"
cd /var/www/html/automateit
git clone https://gitlab.com/miyazghayda/umbulenggalop
cp -R umbulenggalop/* /var/www/html/automateit
rm -rf /var/www/html/automateit/umbulenggalop
mkdir webroot/files
mkdir webroot/files/csv
mkdir files/csv/accounts
mkdir webroot/files/csv/celebrities
mkdir webroot/files/images
mkdir webroot/files/images/profilepicture
mkdir webroot/files/images/upload
mkdir files/images/upload/thumbnail
mkdir files/images/upload/thumbnail2
mkdir webroot/files/images/uploaded
mkdir webroot/files/videos
mkdir webroot/files/videos/upload
mkdir webroot/files/videos/uploaded
composer update
chmod -R 777 /var/www/html/automateit

echo "AUTOMATEIT2"
cd /var/www/html/automateit2
git clone https://gitlab.com/miyazghayda/umbulenggalbgop
cp -R umbulenggalbgop/* /var/www/html/automateit2
rm -rf /var/www/html/automateit2/umbulenggalbgop
mkdir files
mkdir files/images
mkdir files/images/upload
mkdir images
composer update
chmod -R 777 /var/www/html/automateit2

ln -fs /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
dpkg-reconfigure -f noninteractive tzdata

#echo "DOWNLOAD DOCKERFILE"
#wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/docker-compose.yml -O ~/container-php/docker/docker-compose.yml
#wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/databaseDockerfile -O ~/container-php/docker/database/Dockerfile
#wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/nginxDockerfile -O ~/container-php/docker/nginx/Dockerfile
#wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/php-fpmDockerfile -O ~/container-php/docker/php-fpm/Dockerfile
#wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/phpmyadminDockerfile -O ~/container-php/docker/phpmyadmin/Dockerfile
#wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/nginx.conf -O ~/container-php/docker/nginx/nginx.conf
#wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/default.conf -O ~/container-php/docker/nginx/conf.d/default.conf
#wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/sites-default.conf -O ~/container-php/docker/nginx/sites/default.conf

echo "INSTALL TMUX"
sudo apt-get install -y automake build-essential pkg-config libevent-dev libncurses5-dev
rm -fr /tmp/tmux
git clone https://github.com/tmux/tmux.git /tmp/tmux
cd /tmp/tmux
sh autogen.sh
./configure && make
sudo make install
cd -
rm -fr /tmp/tmux
wget https://gist.githubusercontent.com/aansubarkah/6f5afb20d972232501b1/raw/b4059067db4acecf8014a9c71166785da5412706/tmux.conf -O ~/.tmux.conf

echo "INSTALL RUBY"
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt-get update
sudo apt-get install -y git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev nodejs yarn
cd ~
wget http://ftp.ruby-lang.org/pub/ruby/2.5/ruby-2.5.1.tar.gz
tar -xzvf ruby-2.5.1.tar.gz
cd ruby-2.5.1/
./configure
make
sudo make install
ruby -v

sudo apt-get install rbenv
sudo gem install bundler
sudo gem install teamocil
mkdir ~/.teamocil
wget https://gitlab.com/miyazghayda/umbulenggalop/raw/master/automateit.yml -O ~/.teamocil/automateit.yml

#sudo mysql -u root | CREATE USER 'user'@'localhost' IDENTIFIED BY 'jayapura'; | GRANT ALL PRIVILEGES ON * . * TO 'user'@'localhost'; | FLUSH PRIVILEGES;
# CREATE DATABASE automateit;
sudo mysql -u root

echo "REBOOT"
sudo reboot