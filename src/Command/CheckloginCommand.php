<?php
namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\ConnectionManager;
use InstagramAPI\Instagram;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;

class CheckloginCommand extends Command {
    public $Accounts;

    public function initialize() {
        parent::initialize();
        // Load model yang akan digunakan
        $this->Accounts = $this->loadModel('Accounts');
    }

    public function buildOptionParser(ConsoleOptionParser $parser) {
        $parser->addArguments([
            'uname' => ['help' => 'Username', 'required' => true],
            'pass' => ['help' => 'Password', 'required' => true],
            'proxy' => ['help' => 'Proxy', 'required' => false]
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io) {
        $source = ConnectionManager::get('default');
        $igSession = [
            'storage' => 'mysql',
            'dbhost' =>$source->config()['host'],
            'dbname' => $source->config()['database'],
            'dbusername' => $source->config()['username'],
            'dbpassword' => $source->config()['password']
        ];

        $username = $args->getArgument('uname');
        $password = $args->getArgument('pass');
        $proxy = $args->getArgument('proxy');
        if (!empty($username) && !empty($password)) {
            //print_r($igSession);
            $ig = new Instagram(false, false, $igSession);
            if ($proxy != 'http://0.0.0.0') $ig->setProxy($proxy);
            //if (!empty($proxy)) $ig->setProxy($proxy);
            try {
                // Login akun IG
                $ig->login($username, $password);
                echo '1';
            } catch (\Exception $e) {
                echo $e->getMessage();
            }// .try login
        } else {
            echo 'Username/Password empty';
        }
    }// .execute
}