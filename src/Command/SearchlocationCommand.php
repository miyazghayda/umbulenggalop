<?php
namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use InstagramAPI\Instagram;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;

class SearchlocationCommand extends Command {
    public $Accounts;

    public function initialize() {
        parent::initialize();
        // Load model yang akan digunakan
        $this->Accounts = $this->loadModel('Accounts');
    }

    public function buildOptionParser(ConsoleOptionParser $parser) {
        $parser->addArguments([
            'query' => ['help' => 'Place to query', 'required' => true],
            'id' => ['help' => 'Account id', 'required' => false]
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io) {
        $query = $args->getArgument('query');
        $account_id = $args->getArgument('id');
        if ($args->getArgument('id') === null) $account_id = 1;
        // on production server use this instead
        //if (empty($args->getArgument('id'))) $account_id = 4;

        $data = [];
        if (!empty($query)) {
            $account = $this->Accounts->find()
                ->where(['Accounts.id' => $account_id])
                ->contain(['Proxies'])
                ->first();

            $arrQuery = explode('+', $query);
            if (count($arrQuery) > 1) {
                $query = implode(' ', $arrQuery);
            } else {
                $query = $arrQuery[0];
            }
            $ig = new Instagram(false, false);

            try {
                if ($account['proxy_id'] > 1) $ig->setProxy($account['proxy']['name']);
                // Login akun IG
                $ig->login($account['username'], $account['password']);
            } catch (\Exception $e) {
                echo $e->getMessage() . PHP_EOL;
            }// .try login

            try {
                $baseLat = -6.175051;
                $baseLng = 106.827131;

                $respons = $ig->location->search($baseLat, $baseLng, $query);
                //$respons = $ig->location->findPlaces($query);
                if ($respons->getStatus() == 'ok' && count($respons->getVenues()) > 0) {
                    foreach ($respons->getVenues() as $v) {
                        //echo $v;
                        //$io->out($v);
                        array_push($data, json_encode($v, true));
                    }
                }
                //print_r($respons);
            } catch (\Exception $e) {
                echo $e->getMessage() . PHP_EOL;
            }
        }

        foreach ($data as $d) {
            //$io->out($d);
            echo $d;
        }
    }// .execute
}