<?php
namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use InstagramAPI\Instagram;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;

class SearchhashtagCommand extends Command {
    public $Accounts;

    public function initialize() {
        parent::initialize();
        // Load model yang akan digunakan
        $this->Accounts = $this->loadModel('Accounts');
    }

    public function buildOptionParser(ConsoleOptionParser $parser) {
        $parser->addArguments([
            'account_id' => ['help' => 'Account ID', 'required' => true],
            'hashtag' => ['help' => 'Hashtag', 'required' => true],
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io) {
        $account_id = $args->getArgument('account_id');
        $hashtag = trim($args->getArgument('hashtag'));

        $account = $this->Accounts->find()
            ->where(['Accounts.id' => $account_id])
            ->contain(['Proxies'])
            ->first();
        if (isset($account['id']) && !empty($hashtag)) {
            $ig = new Instagram(false, false);
            if ($account['proxy_id'] != 1) $ig->setProxy('http://' . $account['proxy']['name']);

            try {
                // Login akun IG
                $ig->login($account['username'], $account['password']);
                //echo '1';
                $resHashtagRelated = '';
                $response = $ig->hashtag->search($hashtag);
                if ($response->getStatus() == 'ok') {
                    foreach ($response->getResults() as $item) {
                        $resHashtagRelated = $item->getName() . '|' . $item->getMediaCount() . ',' . $resHashtagRelated;
                    }
                }// .if status ok
                echo $resHashtagRelated;
            } catch (Exception $e) {
                echo $e->getMessage();
            }// .try login
        } else {
            echo 'Hashtag tidak diisi';
        }
    }// .execute
}