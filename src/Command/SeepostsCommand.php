<?php
namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use InstagramAPI\Instagram;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class SeepostsCommand extends Command {
    public $Accounts;
    public $Locations;
    public $Members;
    public $Posts;
    public $Wads;

    public function initialize() {
        parent::initialize();
        // Load model yang akan digunakan
        $this->Accounts = $this->loadModel('Accounts');
        $this->Locations = $this->loadModel("Locations");
        $this->Members = $this->loadModel('Members');
        $this->Posts = $this->loadModel('Posts');
        $this->Wads = $this->loadModel('Wads');
    }

    public function buildOptionParser(ConsoleOptionParser $parser) {
        $parser->addArguments([
            'account_id' => ['help' => 'Account ID', 'required' => true],
            'hashtag' => ['help' => 'Hashtag', 'required' => true],
            'count' => ['help' => 'How Many', 'required' => false],
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io) {
        $account_id = $args->getArgument('account_id');
        $hashtag = trim($args->getArgument('hashtag'));
        $maximumPost = $args->getArgument('count');
        if ($args->getArgument('count') == null) $maximumPost = 200;

        $account = $this->Accounts->find()
            ->where(['Accounts.id' => $account_id])
            ->contain(['Proxies'])
            ->first();
            
        // create temporary directory
        $dir = new Folder(TMP . 'cache' . DS . 'seeposts', true, 0777);
        $t = null;
        if (isset($account['id']) && !empty($hashtag)) {
            $source = ConnectionManager::get('default');
            $igSession = [
                'storage' => 'mysql',
                'dbhost' =>$source->config()['host'],
                'dbname' => $source->config()['database'],
                'dbusername' => $source->config()['username'],
                'dbpassword' => $source->config()['password']
            ];

            $ig = new Instagram(false, false, $igSession);

            if ($account['proxy_id'] != 1) $ig->setProxy('http://' . $account['proxy']['name']);

            try {
                // Login akun IG
                $ig->login($account['username'], $account['password']);
                //echo '1';
                $nextMediaIds = null;
                $ic = 0;
                $resPostIds = '';
                $resHashtagRelated = '';
                $return = '';
                $dataHashtag = [];
                $data = [];
                $mediaPks = [];
                do {
                    $responsesTop = $ig->hashtag->getSection($hashtag, 'top', $nextMediaIds);
                   
                    if ($responsesTop->getStatus() == 'ok') {
                        $countItem = count($responsesTop->getSections());
                        $items = $responsesTop->getSections();
                        // gather related hashtags
                        foreach ($items[0]->getLayoutContent()->getRelated() as $h) {
                            array_push($dataHashtag, $h->getName());
                            //$resHashtagRelated = $h->getName() . ',' . $resHashtagRelated;
                        }
                        /*$t = time();
                        $fp = fopen(TMP . 'seeposts' . DS . $t . '_hashtagrelated.json', 'w');
                        fwrite($fp, json_encode($items[0]->getLayoutContent()->getRelated()));
                        fclose($fp);
                        $return = $return . ',' . $t . '_hashtagrelated';*/

                        for ($i = 1; $i < $countItem; $i++) {
                            $medias = $items[$i]->getLayoutContent()->getMedias();
                            $ic = $ic + count($medias);
                            foreach ($medias as $item) {
                                if (!in_array($item->getMedia()->getPk(), $mediaPks)) {
                                    array_push($data, $item->getMedia());
                                    array_push($mediaPks, $item->getMedia()->getPk());
                                }
                            }
                            // create folder if doesn't exists yet
                            //$dir = new Folder(TMP . 'seeposts', true, 0777);
                            //$t = time();
                            //$fp = fopen(TMP . 'seeposts' . DS . $t . '.json', 'w');
                            //fwrite($fp, json_encode($medias, JSON_PRETTY_PRINT));
                            //fwrite($fp, json_encode($medias, JSON_PRETTY_PRINT));
                            //fclose($fp);
                            //$return = $return . ',' . $t;
                            //sleep(1);
                            //echo $t;
                            /*foreach ($medias as $item) {
                                // Insert Location
                                $location_id = 1;
                                if ($item->getMedia()->hasLocation() && $item->getMedia()->getLocation() !== null) {
                                    $location_id = $this->insertLocation($item->getMedia()->getLocation());
                                }

                                // Insert Member
                                $member_id = $this->insertMember($item->getMedia()->getUser());

                                // Insert Post
                                $post_id = $this->insertPost($item->getMedia(), $location_id, $member_id);
                                $resPostIds = $post_id . ',' . $resPostIds;

                                // Insert Wad
                                if ($post_id > 0 && $item->getMedia()->getMediaType() == 1) {// Photo
                                    $this->insertWad($item->getMedia()->getImageVersions2()->getCandidates()[0], $post_id, $item->getMedia()->getMediaType());
                                } elseif ($post_id > 0 && $item->getMedia()->getMediaType() == 2) {// Video
                                    $this->insertWad($item->getMedia()->getVideoVersions()[0], $post_id, $item->getMedia()->getMediaType());
                                } elseif ($post_id > 0 && $item->getMedia()->getMediaType() == 8) {// Carousel
                                    $this->insertWad($item->getMedia()->getCarouselMedia(), $post_id, $item->getMedia()->getMediaType());
                                }
                                echo $ic . '. ' . $item->getMedia()->getPk() . PHP_EOL;
                                $ic++;
                            }*/
                        }
                    }// .if status ok
                    //$responsesTop->printPropertyDescriptions();
                    //echo $responsesTop->getNextMediaIds()[0] . PHP_EOL;
                    //print_r($responsesTop->getNextMediaIds());
                    if (count($responsesTop->getNextMediaIds()) > 0) $nextMediaIds = $responsesTop->getNextMediaIds();
                    
                    if ($ic > $maximumPost) $nextMediaIds = null;
                    //echo $nextMediaIds . PHP_EOL;
                    sleep(rand(7, 12));
                } while ($nextMediaIds != null);
                //echo $ic . PHP_EOL;
                //echo $resPostIds . '|' . $resHashtagRelated;
                $t = time();
                /*$fp = fopen(TMP . 'seeposts' . DS . $t . '_hashtagrelated.json', 'w');
                fwrite($fp, json_encode($dataHashtag, JSON_PRETTY_PRINT));
                fclose($fp);
                $fp = fopen(TMP . 'seeposts' . DS . $t . '.json', 'w');
                fwrite($fp, json_encode($data, JSON_PRETTY_PRINT));
                fclose($fp);
                echo $t;*/
                $val = [
                    'hashtag' => $dataHashtag,
                    'medias' => json_encode($data)
                ];
                $this->cache_set($dir->pwd(), $t, $val);
                echo $t;

                //echo $return;
            } catch (Exception $e) {
                echo $e->getMessage();
            }// .try login
        } else {
            echo 'Hashtag tidak diisi';
        }
        //$this->inserting($t);
        //$connection = new AMQPStreamConnection('localhost', 5672, 'radmin', 'radmin');
        $connection = new AMQPStreamConnection('tanpa.download', 5672, 'radmin', 'radmin');

        $channel = $connection->channel();
        
        $channel->queue_declare('seeposts', false, true, false, false);

        $msg = new AMQPMessage(
            (string)$t,
            ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]
        );
        $channel->basic_publish($msg, '', 'seeposts');
        $channel->close();
        $connection->close();
    }// .execute

    private function inserting($t) {
        if ($t != null) {
            $cachePath = CACHE . 'seeposts' . DS . $t;
            $retFile = $this->cache_get(CACHE . 'seeposts' . DS . $t);
            if ($retFile != false) {
                $medias = json_decode($retFile['medias']);

            }
        }
    }

    private function cache_get($cachePath) {
        @include $cachePath;
        return isset($val) ? $val : false;
    }

    private function cache_set($cachePath, $key, $val) {
        $val = var_export($val, true);
        // HHVM fails at __set_state, so just use object cast for now
        $val = str_replace('stdClass::__set_state', '(object)', $val);
        // Write to temp file first to ensure atomicity
        $tmp = $cachePath . DS . $key . '.' . uniqid('', true) . '.tmp';
        file_put_contents($tmp, '<?php $val = ' . $val . ';', LOCK_EX);
        rename($tmp, $cachePath . DS . $key);
    }

    private function insertWad($datum = null, $post_id = 1, $typeid = 1) {
        $newData = [
            'post_id' => $post_id,
            'typeid' => $typeid,
            'sequence' => 0
        ];

        if ($typeid == 1 || $typeid == 2) {// Single photo or video
            $newData['url'] = $datum->getUrl();
            $newData['width'] = $datum->getWidth();
            $newData['height'] = $datum->getHeight();
            $newData['urlfixed'] = false;
            $newData['active'] = true;
            
            $conn = ConnectionManager::get('default');
            $stmt = $conn->execute('SELECT id, count(id) AS idcount FROM wads WHERE post_id = ? AND url = ? AND active = 1', [$post_id, $datum->getUrl()]);
            $check = $stmt->fetch('assoc');

            /*$check = $this->Wads->find()
                ->select(['id'])
                ->where(['post_id' => $post_id, 'url' => $datum->getUrl(), 'active' => true]);
            if ($check->count() < 1) {*/
            if ($check['idcount'] < 1) {
                $data = $this->Wads->newEntity();
                $data = $this->Wads->patchEntity($data, $newData);
                if ($this->Wads->save($data)) {
                    return $data->id;
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        } elseif ($typeid == 8) {// Carousel
            $sequence = 0;
            foreach ($datum as $d) {
                $newData['url'] = '';
                $newData['width'] = 0;
                $newData['height'] = 0;
                $newData['typeid'] = $d->getMediaType();
                if ($d->getMediaType() == 1) {// Photo
                    $reap = $d->getImageVersions2()->getCandidates()[0];
                } elseif ($d->getMediaType() == 2) {// Video
                    $reap = $d->getVideoVersions()[0];
                }
                $newData['url'] = $reap->getUrl();
                $newData['width'] = $reap->getWidth();
                $newData['height'] = $reap->getHeight();
                $newData['sequence'] = $sequence;
                $newData['urlfixed'] = false;
                $newData['active'] = true;
                $sequence++;
                /*$check = $this->Wads->find()
                    ->select(['id'])
                    ->where(['post_id' => $post_id, 'url' => $reap->getUrl(), 'active' => true]);
                if ($check->count() < 1) {*/
                $conn = ConnectionManager::get('default');
                $stmt = $conn->execute('SELECT id, count(id) AS idcount FROM wads WHERE post_id = ? AND url = ? AND active = 1', [$post_id, $reap->getUrl()]);
                $check = $stmt->fetch('assoc');
                if ($check['idcount'] < 1) {

                    $data = $this->Wads->newEntity();
                    $data = $this->Wads->patchEntity($data, $newData);
                    if ($this->Wads->save($data)) {
                        return $data->id;
                    } else {
                        return 1;
                    }
                } else {
                    return 1;
                }
            }// foreach datum
        }// if type id single or carousel
    }// insert wads function

    private function insertPost($datum = null, $location_id = 1, $member_id = 1) {
        $conn = ConnectionManager::get('default');
        $stmt = $conn->execute('SELECT id, count(id) AS idcount FROM posts WHERE pk = ? AND active = 1', [$datum->getPk()]);
        $check = $stmt->fetch('assoc');
        //print_r($row);
        //$check = $this->Posts->find()
            //->select(['id'])
            //->where(['pk' => $datum->getPk(), 'active' => true]);

        if ($check['idcount'] > 0) {
            return $check['id'];
            //$data = $check->first();
            //return $data->id;
        } else {
            ($datum->hasCaption() && $datum->getCaption() !== null) ? $caption = $datum->getCaption()->getText() : $caption = '';
            ($datum->hasCommentCount() && $datum->getCommentCount() !== null) ? $comment = $datum->getCommentCount() : $comment = 0;

            $post = $this->Posts->newEntity();
            $post->pk = $datum->getPk();
            $post->sourceid = $datum->getId();;
            $post->location_id = $location_id;
            $post->member_id = $member_id;
            $post->typeid = $datum->getMediaType();
            $post->caption = $caption;
            $post->likes = $datum->getLikeCount();
            $post->comments = $comment;
            $post->takenat = $datum->getTakenAt();
            $post->active = true;
            if ($this->Posts->save($post)) {
                return $post->id;
            } else {
                return 0;
            }
        }
    }

    private function insertMember($datum = null) {
        $check = $this->Members->find()
            ->select(['id'])
            ->where(['pk' => $datum->getPk(), 'active' => true]);

        if ($check->count() > 0) {
            $data = $check->first();
            return $data->id;
        } else {
            $account = $this->Members->newEntity();
            $account->pk = $datum->getPk();
            $account->username = $datum->getUsername();
            $account->fullname = $datum->getFullName();
            $account->description = '';
            $account->profpicurl = $datum->getProfilePicUrl();
            $account->followers = 0;
            $account->followings = 0;
            $account->contents = 0;
            $account->closed = $datum->getIsPrivate();
            $account->profpicurlfixed = true;
            $account->genderdetection = false;
            $account->genderdetected = false;
            $account->active = true;
            if ($this->Members->save($account)) {
                return $account->id;
            } else {
                return 1;
            }
        }
    }

    private function insertLocation($datum = null) {
        $check = $this->Locations->find()
            ->select(['id'])
            ->where(['pk' => $datum->getPk(), 'active' => true]);

        if ($check->count() > 0) {
            $data = $check->first();
            return $data->id;
        } else {
            $location = $this->Locations->newEntity();
            $location->pk = $datum->getPk();
            $location->fbplacesid = $datum->getFacebookPlacesId();
            $location->lat = $datum->getLat();
            $location->lng = $datum->getLng();
            $location->address = $datum->getAddress();
            $location->name = $datum->getName();
            $location->shortname = $datum->getShortName();
            $location->active = true;
            if ($this->Locations->save($location)) {
                return $location->id;
            } else {
                return 1;
            }
        }
    }
}