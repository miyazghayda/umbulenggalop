<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Filters Model
 *
 * @property \App\Model\Table\AccountsTable|\Cake\ORM\Association\BelongsTo $Accounts
 * @property \App\Model\Table\AccountlistsTable|\Cake\ORM\Association\HasMany $Accountlists
 * @property \App\Model\Table\HashtaglistsTable|\Cake\ORM\Association\HasMany $Hashtaglists

 *
 * @method \App\Model\Entity\Filter get($primaryKey, $options = [])
 * @method \App\Model\Entity\Filter newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Filter[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Filter|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Filter|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Filter patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Filter[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Filter findOrCreate($search, callable $callback = null, $options = [])
 */
class FiltersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('filters');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Accountlists', [
            'foreignKey' => 'filter_id'
        ]);
        $this->hasMany('Hashtaglits', [
            'foreignKey' => 'filter_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('typeid', 'create')
            ->notEmpty('typeid');

        $validator
            ->scalar('hashtagblacklist')
            ->allowEmpty('hashtagblacklist');

        $validator
            ->scalar('fullnameblacklist')
            ->allowEmpty('fullnameblacklist');

        $validator
            ->scalar('biographyblacklist')
            ->allowEmpty('biographyblacklist');

        $validator
            ->requirePresence('nmonths', 'create')
            ->notEmpty('nmonths');

        $validator
            ->requirePresence('accounttypeid', 'create')
            ->notEmpty('accounttypeid');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));

        return $rules;
    }
}
