<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Replicatinglists Model
 *
 * @property \App\Model\Table\AccountsTable|\Cake\ORM\Association\BelongsTo $Accounts
 * @property \App\Model\Table\MembersTable|\Cake\ORM\Association\BelongsTo $Members
 *
 * @method \App\Model\Entity\Replicatinglist get($primaryKey, $options = [])
 * @method \App\Model\Entity\Replicatinglist newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Replicatinglist[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Replicatinglist|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Replicatinglist|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Replicatinglist patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Replicatinglist[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Replicatinglist findOrCreate($search, callable $callback = null, $options = [])
 */
class ReplicatinglistsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('replicatinglists');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Members', [
            'foreignKey' => 'member_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->allowEmpty('username');

        $validator
            ->boolean('memberfixed')
            ->requirePresence('memberfixed', 'create')
            ->notEmpty('memberfixed');

        $validator
            ->integer('contents')
            ->requirePresence('contents', 'create')
            ->notEmpty('contents');

        $validator
            ->integer('loads')
            ->requirePresence('loads', 'create')
            ->notEmpty('loads');

        $validator
            ->boolean('replicated')
            ->requirePresence('replicated', 'create')
            ->notEmpty('replicated');

        $validator
            ->scalar('note')
            ->allowEmpty('note');

        $validator
            ->dateTime('replicatedat')
            ->allowEmpty('replicatedat');

        $validator
            ->time('uploadat')
            ->allowEmpty('uploadat');

        $validator
            ->scalar('maxid')
            ->maxLength('maxid', 1000)
            ->allowEmpty('maxid');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        //$rules->add($rules->isUnique(['username']));
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));
        $rules->add($rules->existsIn(['member_id'], 'Members'));

        return $rules;
    }
}
