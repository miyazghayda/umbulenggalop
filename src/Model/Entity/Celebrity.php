<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Celebrity Entity
 *
 * @property int $id
 * @property int $member_id
 * @property int $followers
 * @property int $followersaved
 * @property bool $allfollowersaved
 * @property string $nextmaxid
 * @property bool $active
 *
 * @property \App\Model\Entity\Member $member
 * @property \App\Model\Entity\Accountlist[] $accountlists
 */
class Celebrity extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'member_id' => true,
        'followers' => true,
        'followersaved' => true,
        'allfollowersaved' => true,
        'nextmaxid' => true,
        'active' => true,
        'member' => true,
        'accountlists' => true
    ];
}
