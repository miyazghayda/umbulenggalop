<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Filter Entity
 *
 * @property int $id
 * @property int $account_id
 * @property int $filter_id
 * @property int $typeid
 * @property string $hashtagblacklist
 * @property string $fullnameblacklist
 * @property string $biographyblacklist
 * @property int $nmonths
 * @property int $accounttypeid
 * @property bool $active
 *
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\Accountlist[] $accountlists
 * @property \App\Model\Entity\Hashtaglist[] $hashtaglists

 */
class Filter extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'account_id' => true,
        'typeid' => true,
        'hashtagblacklist' => true,
        'fullnameblacklist' => true,
        'biographyblacklist' => true,
        'nmonths' => true,
        'accounttypeid' => true,
        'active' => true,
        'account' => true,
        'accountlist' => true,
        'hashtaglist' => true,
    ];
}
