<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Accountlist Entity
 *
 * @property int $id
 * @property int $account_id
 * @property int $member_id
 * @property int $filter_id
 * @property int $celebrity_id
 * @property int $vassal_id
 * @property int $typeid
 * @property bool $allfollowersaved
 * @property string $nextmaxid
 * @property bool $active
 *
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\Member $member
 * @property \App\Model\Entity\Filter $filter
 * @property \App\Model\Entity\Celebrity $celebrity
 * @property \App\Model\Entity\Vassal $vassal
 */
class Accountlist extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'account_id' => true,
        'member_id' => true,
        'filter_id' => true,
        'celebrity_id' => true,
        'vassal_id' => true,
        'typeid' => true,
        'proffoxed' => true,
        'allfollowersaved' => true,
        'nextmaxid' => true,
        'active' => true,
        'account' => true,
        'member' => true,
        'filter' => true,
        'celebrity' => true,
        'vassal' => true
    ];
}
