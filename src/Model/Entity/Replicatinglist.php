<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Replicatinglist Entity
 *
 * @property int $id
 * @property int $account_id
 * @property int $member_id
 * @property string $username
 * @property bool $memberfixed
 * @property string $note
 * @property int $contents
 * @property int $loads
 * @property bool $replicated
 * @property \Cake\I18n\FrozenTime $replicatedat
 * @property string $maxid
 * @property \Cake\I18n\Time $uploadat
 * @property bool $active
 *
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\Member $member
 */
class Replicatinglist extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'account_id' => true,
        'member_id' => true,
        'username' => true,
        'memberfixed' => true,
        'contents' => true,
        'loads' => true,
        'replicated' => true,
        'replicatedat' => true,
        'maxid' => true,
        'uploadat' => true,
        'note' => true,
        'active' => true,
        'account' => true,
        'member' => true
    ];
}
