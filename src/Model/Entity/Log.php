<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Log Entity
 *
 * @property int $id
 * @property string $channel
 * @property int $level
 * @property string $message
 * @property int $time
 * @property int $account_id
 * @property \Cake\I18n\FrozenTime $created
 * @property int $scriptid
 *
 * @property \App\Model\Entity\Account $account
 */
class Log extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'channel' => true,
        'level' => true,
        'message' => true,
        'time' => true,
        'account_id' => true,
        'created' => true,
        'scriptid' => true,
        'account' => true
    ];
}
