<?php echo $this->Html->css('jquery.bsPhotoGallery'); ?>
<style>
ul {
    padding: 0 0 0 0;
    margin: 0 0 0 0;
}

ul li {
    list-style: none;
    margin-bottom: 25px;
}

ul li img {
    cursor: pointer;
}
</style>
<div class="clearfix"></div>
<section class="content-header">
    <h1>Konten akan di-Repost</h1>
    <ol class="breadcrumb">
        <li>Auto Post</li>
        <li>Repost</li>
        <li class="active">Konten akan di-Repost</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="col-xs-4 pull-left">
                        <a href="#" class="btn btn-default" role="button" id="btnDeleteAll"><i class="fa fa-trash"></i> Hapus yang Dipilih</a>
                    </div>
                    <div class="col-xs-4 pull-right">

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                <?php echo $this->Form->select('account_id', $accounts, ['class' => 'form-control', 'id' => 'accountId', 'default' => 0]); ?>
                            </div><!--/.input-group -->
                        </div><!--/. form-group -->

                    </div><!--/.col-xs-4 -->
                </div><!--/.box-header -->
                <div class="box-body">
                    <?php if (count($cargos) < 1) { ?>
                        <p style="font-weight: 400">Belum ada konten.</p>
                    <?php } else { ?>
                        <ul class="first">
                            <?php foreach($cargos as $cargo) { ?>
                            <li id="<?php echo $cargo['id']; ?>">
                            <img alt="<?php echo $cargo['caption']; ?>" src="<?php echo $cargo['post']['wads'][0]['url']; ?>">
                            <p class="text"><?php echo $this->Time->format($cargo['schedule'], 'dd-MM-yyyy HH:mm'); ?></p>
                            </li>
                            <li class="no-modal">
                                <input type="checkbox" id="cb-<?php echo $cargo['id'];?>"
                                    data-id="<?php echo $cargo['id'];?>"
                                    data-caption="<?php echo $cargo['caption'];?>">
                            </li>
                            <?php } ?>
                        </ul><!--/.first -->
                    <div class="row pull-right" style="padding-right: 20px;">
                            <ul class="pagination pagination-sm inline">
                                <?php
                                echo $this->Paginator->first('First');
                                echo $this->Paginator->prev('«');
                                echo $this->Paginator->numbers(['modulus' => 4]);
                                echo $this->Paginator->next('»');
                                echo $this->Paginator->last("Last");
                                ?>
                            </ul>
                    </div><!--/.row -->
                    <?php } ?>
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>

<div id="modalWarning" class="modal modal-warning fade" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hiddent="True">x</span></button>
                <h4 class="modal-title">Perhatian</h4>
            </div><!--/.modal-header -->
            <div class="modal-body">
            </div><!--/.modal-body -->
            <div class="modal-footer">
                <button class="btn btn-outline" type="button" data-dismiss="modal">Tutup</button>
                <button class="btn btn-outline" type="button" id="btnDeleteYes">Hapus</button>
            </div><!--/.modal-footer -->
        </div><!--/.modal-content -->
    </div><!--/.modal-dialog -->
</div><!--/.modalWarning -->

<?php echo $this->Html->script('jquery.bsPhotoGallery'); ?>
<script>
$(document).ready(function() {
    $('#btnDeleteAll').hide();
    var checkedPost = [];
    var checkedPostCaption = [];
    $('input:checkbox').change(function() {
        let isChecked = $(this).is(':checked');
        if ($(this).is(':checked')) {
            if ($.inArray($(this).attr('data-id'), checkedPost) == -1) {
                checkedPost.push($(this).attr('data-id'));
            }
        } else {
            if ($.inArray($(this).attr('data-id'), checkedPost) > -1) {
                checkedPost.splice($.inArray($(this).attr('data-id'), checkedPost), 1);
            }
        }
        if (checkedPost.length > 0) {
            $('#btnDeleteAll').show();
        } else {
            $('#btnDeleteAll').hide();
        }
    });
    $('#btnDeleteAll').click(function() {
        if (checkedPost.length > 0) {
            $('.modal-body').html('Yakin akan menghapus ' + checkedPost.length + ' post(s)?');
            $('#modalWarning').modal('show');
        }
    });
    $('#btnDeleteYes').click(function() {
        if (checkedPost.length > 0) {
            checkedPost.forEach(function(el) {
                $.ajax({
                    url: '/replicas/bulkdelete/' + el
                }).done(function(data) {
                    if (data == 1) {
                        $('#' + el).hide();
                        $('#cb-' + el).parent().hide();
                    }
                });
            });
            checkedPost = [];
        }
        $('#modalWarning').modal('hide');
    });

    $('ul.first').bsPhotoGallery({
        'classes': 'col-lg-2 col-md-4 col-sm-3 col-xs-4 col-xxs-12',
        'hasModal': true
    });

    $('li.bspHasModal').on('click', function(){
        let liId = $(this).attr('id');
        $('#bsp-view').attr('href', '/replicas/view/' + liId);
        $('#bsp-edit').attr('href', '/replicas/edit/' + liId);
        $('#bsp-delete').attr('href', '/replicas/delete/' + liId);
    });

    $('#accountId').on('change', function() {
        console.log($(this).val());
        window.location.replace('/replicas/queue/' + $(this).val());
    });
});
</script>