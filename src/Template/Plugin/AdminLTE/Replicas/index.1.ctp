<?php echo $this->Html->css('jquery.bsPhotoGallery'); ?>
<style>
ul {
    padding: 0 0 0 0;
    margin: 0 0 0 0;
}

ul li {
    list-style: none;
    margin-bottom: 25px;
}

ul li img {
    cursor: pointer;
}
</style>
<div class="clearfix"></div>
<section class="content-header">
    <h1>Konten telah di-Repost</h1>
    <ol class="breadcrumb">
        <li>Auto Post</li>
        <li>Repost</li>
        <li class="active">Konten telah di-Repost</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="col-xs-4 pull-right">

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                <?php echo $this->Form->select('account_id', $accounts, ['class' => 'form-control', 'id' => 'accountId', 'default' => 0]); ?>
                            </div><!--/.input-group -->
                        </div><!--/. form-group -->

                    </div><!--/.col-xs-4 -->
                </div><!--/.box-header -->
                <div class="box-body">
                    <?php if (count($cargos) < 1) { ?>
                        <p style="font-weight: 400">Belum ada konten.</p>
                    <?php } else { ?>
                        <ul class="first">
                            <?php
                            foreach($cargos as $cargo) {
                            ?>
                            <li id="<?php echo $cargo['id']; ?>">
                            <img alt="<?php echo $cargo['caption']; ?>" src="<?php echo $cargo['post']['wads'][0]['url']; ?>">
                            <p class="text"><?php echo $this->Time->format($cargo['modified'], 'dd-MM-yyyy HH:mm', null, 'Pacific/Kiritimati'); ?></p>
                            </li>
                            <?php } ?>
                        </ul><!--/.first -->
                    <div class="row pull-right" style="padding-right: 20px;">
                            <ul class="pagination pagination-sm inline">
                                <?php
                                echo $this->Paginator->first('First');
                                echo $this->Paginator->prev('«');
                                echo $this->Paginator->numbers(['modulus' => 4]);
                                echo $this->Paginator->next('»');
                                echo $this->Paginator->last("Last");
                                ?>
                            </ul>
                    </div><!--/.row -->
                    <?php } ?>
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>

<?php echo $this->Html->script('jquery.bsPhotoGallery.alreadyUpload'); ?>
<script>
$(document).ready(function() {
    $('ul.first').bsPhotoGallery({
        'classes': 'col-lg-2 col-md-4 col-sm-3 col-xs-4 col-xxs-12',
        'hasModal': true
    });

    $('li.bspHasModal').on('click', function(){
        let liId = $(this).attr('id');
        $('#bsp-view').attr('href', '/replicas/view/' + liId);
    });

    $('#accountId').on('change', function() {
        console.log($(this).val());
        window.location.replace('/replicas/index/' + $(this).val());
    });
});
</script>