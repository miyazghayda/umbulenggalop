<section class="content-header">
    <h1>Direct Message Follower Baru</h1>
    <ol class="breadcrumb">
        <li>
            Auto DM 
        </li>
        <li class="active">
            DM Follower Baru
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <?php //echo $this->Html->link('Template Komentar', ['controller' => 'Compositions', 'action' => 'index']); ?>
                </div><!--/.box-header -->
                <div class="box-body">
                    <div class="dt-bootstrap">
                    <?php echo $this->Form->create('Preferences.add', ['id' => 'addForm', 'data-toggle' => 'validator']); ?>
                        <div class="form-group">
                            <label>Aktifkan <a href="/documentation/faq#maxdmperday" target="_blank"><i class=" fa fa-question-circle"></i></a></label>
                            <br/>
                            <?php echo $this->Form->checkbox('filtercheckbox', ['hiddenField' => false, 'id' => 'filtercheckbox']);?>
                            <?php echo $this->form->hidden('dmbynewfollowing', ['id' => 'dmbynewfollowing', 'value' => $preference['dmbynewfollowing']]); ?>
                        </div><!--/. form-group -->
                        
                        <div class="form-group follow-filter">
                            <label>Banyaknya Follower yang akan dikirimi DM per hari. Isi DM akan menggunakan <a href="/compositions/dms">template DM</a> yang dipilih secara acak</label>
                            <?php echo $this->Form->text('newfollowertodm', [
                                'placeholder' => 'Banyak Akun min 1 max 20',
                                'id' => 'newfollowertodm',
                                'class' => 'form-control',
                                'required' => 'false',
                                'type' => 'number',
                                'min' => 0,
                                'max' => 30,
                                'step' => 1,
                                'value' => $preference['newfollowertodm'],
                                'data-required-error' => 'Harus diisi']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
<?php echo $this->Form->submit('Simpan', ['class' => 'btn btn-primary btn-block']); ?>
                        </div><!--/. form-group -->
                    </div><!--/.dt-bootstrap -->
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>

<?php echo $this->Html->script('bootstrap-checkbox.min'); ?>

<script>
$(function() {
    $('.follow-filter').hide();

<?php if ($preference['dmbynewfollowing'] == true) {?>
    $('#filtercheckbox').prop('checked', true);
    $('.follow-filter').show();
<?php }?>
 
    $('#filtercheckbox').checkboxpicker({
        offLabel: 'Tidak',
        onLabel: 'Ya'
    }).on('change', function() {
        if($(this).prop('checked')) {
            $('#dmbynewfollowing').val(1);
            $('.follow-filter').show(1000);
            $('#newfollowertodm').attr('required', 'true');
            $('#newfollowertodm').attr('min', 1);
        } else {
            $('#dmbynewfollowing').val(0);
            $('.follow-filter').hide(1000);
            $('#newfollowertodm').removeAttr('required');
            $('#newfollowertodm').attr('min', 0);
        }
        $('#addForm').validator('update');
        $('#addForm').validator('validate');
    });

    $('#addForm').submit(function() {
        if ($('#dmbynewfollowing').val() === 1) {
            if ($('#newfollowertodm').val() === '' || $('#newfollowertodm').val() < 1) {
                return false;
            }
        }
    });
});
</script>