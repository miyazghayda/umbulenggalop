<section class="content-header">
    <h1>Like + Comment pada Konten Follower Baru</h1>
    <ol class="breadcrumb">
        <li>
            Auto Like/Comment
        </li>
        <li class="active">
            Like + Comment Follower Baru
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <?php //echo $this->Html->link('Template Komentar', ['controller' => 'Compositions', 'action' => 'index']); ?>
                </div><!--/.box-header -->
                <div class="box-body">
                    <div class="dt-bootstrap">
                    <?php echo $this->Form->create('Preferences.add', ['id' => 'addForm', 'data-toggle' => 'validator']); ?>
                        <div class="form-group">
                            <label>Aktifkan <a href="/documentation/faq#filteridolfollower" target="_blank"><i class=" fa fa-question-circle"></i></a></label>
                            <br/>
                            <?php echo $this->Form->checkbox('filtercheckbox', ['hiddenField' => false, 'id' => 'filtercheckbox']);?>
                            <?php echo $this->form->hidden('likebynewfollowing', ['id' => 'likebynewfollowing', 'value' => $preference['likebynewfollowing']]); ?>
                        </div><!--/. form-group -->
                        
                        <div class="form-group follow-filter">
                            <label>Banyaknya konten yang akan di-Like pada tiap akun follower</label>
                            <?php echo $this->Form->text('newfollowerposttolike', [
                                'placeholder' => 'Banyak Konten di-Like, min 1 max 50',
                                'id' => 'newfollowerposttolike',
                                'class' => 'form-control',
                                'required' => 'false',
                                'type' => 'number',
                                'min' => 0,
                                'max' => 50,
                                'step' => 1,
                                'value' => $preference['newfollowerposttolike'],
                                'data-required-error' => 'Harus diisi']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group follow-filter">
                            <label>Banyaknya konten yang akan di-Comment pada tiap akun follower. Komentar akan menggunakan <a href='/compositions/comments'>Template Komentar</a> yang dipilih secara acak</label>
                            <?php echo $this->Form->text('newfollowerposttocomment', [
                                'placeholder' => 'Banyak Konten di-Comment, min 0 max 50',
                                'id' => 'newfollowerposttocomment',
                                'class' => 'form-control',
                                'required' => 'false',
                                'type' => 'number',
                                'min' => 0,
                                'max' => 50,
                                'step' => 1,
                                'value' => $preference['newfollowerposttocomment'],
                                'data-required-error' => 'Harus diisi']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
<?php echo $this->Form->submit('Simpan', ['class' => 'btn btn-primary btn-block']); ?>
                        </div><!--/. form-group -->
                    </div><!--/.dt-bootstrap -->
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>

<?php echo $this->Html->script('bootstrap-checkbox.min'); ?>

<script>
$(function() {
    $('.follow-filter').hide();

    $('#filtercheckbox').checkboxpicker({
        offLabel: 'Tidak',
        onLabel: 'Ya'
    }).on('change', function() {
        if($(this).prop('checked')) {
            $('#likebynewfollowing').val(1);
            $('.follow-filter').show(1000);
            $('#newfollowerposttolike').attr('required', 'true');
            $('#newfollowerposttolike').attr('min', 1);
            $('#newfollowerposttocomment').attr('required', 'true');
        } else {
            $('#likebynewfollowing').val(0);
            $('.follow-filter').hide(1000);
            $('#newfollowerposttolike').removeAttr('required');
            $('#newfollowerposttolike').attr('min', 0);
            $('#newfollowerposttocomment').removeAttr('required');
        }
        $('#addForm').validator('update');
        $('#addForm').validator('validate');
    });

<?php if ($preference['likebynewfollowing'] == true) {?>
    $('#filtercheckbox').prop('checked', true);
    $('.follow-filter').show();
<?php }?>
        
    $('#addForm').submit(function() {
        $('#addForm').validator('validate');
        if ($('#filter').val() === 1) {
            if ($('#newfollowerposttolike').val() === '' && $('#newfollowerposttocomment').val() === '') {
                return false;
            }
        }
    });
});
</script>