<?php echo $this->Html->css('bootstrap-tagsinput'); ?>
<style>
label {
    font-weight: 400;
}
.bootstrap-tagsinput {
    width: 100%;
}
.bootstrap-tagsinput input {
    width: 100%;
}
</style>

<section class="content-header">
    <h1>Tambah Hashtag yang akan di-Follow</h1>
    <ol class="breadcrumb">
        <li>
            Post
        </li>
        <li class="active">
            Tambah
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <?php echo $this->Html->link('Daftar Hashtag', ['controller' => 'Hashtaglists', 'action' => 'index']); ?>
                </div><!--/.box-header -->
                <div class="box-body">
                    <div class="dt-bootstrap">
                    <?php echo $this->Form->create('Hashtaglists.add', ['id' => 'addForm', 'data-toggle' => 'validator']); ?>
                        <div class="form-group">
                            <label>Hashtag yang akan di-Follow akun yang mengirim konten dengan hashtag tersebut</label>
                            <?php echo $this->Form->text('caption', ['placeholder' => 'Hashtag, diisi tanpa #, mis. exploreindonesia', 'class' => 'form-control', 'id' => 'caption', 'required' => 'true', 'data-required-error' => 'Harus diisi']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->
                                               
                        <div class="form-group">
                            <label>Gunakan Filter <a href="/documentation/faq#filteridolfollower" target="_blank"><i class=" fa fa-question-circle"></i></a></label>
                            <br/>
                            <?php echo $this->Form->checkbox('filtercheckbox', ['hiddenField' => false, 'id' => 'filtercheckbox']); ?>
                            <?php echo $this->form->hidden('filter', ['id' => 'filter']); ?>
                        </div><!--/. form-group -->
                        
                        <div class="form-group follow-filter">
                            <label>Kata dalam Nama Akun yang <strong>Dihindari</strong>, tanpa spasi, misal <strong>Jual</strong>, pisahkan dengan koma, dapat dikosongkan.</label>
                            <?php echo $this->Form->text('fullnameblacklist', ['placeholder' => 'Kata dalam Nama', 'id' => 'fullnameblacklist', 'class' => 'form-control', 'required' => 'false', 'data-required-error' => 'Harus diisi', 'data-role' => 'tagsinput']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group follow-filter">
                            <label>Kata dalam Deskripsi Akun (Biografi) yang <strong>Dihindari</strong>, tanpa spasi, misal <strong>Jual</strong>, pisahkan dengan koma, dapat dikosongkan.</label>
                            <?php echo $this->Form->text('biographyblacklist', ['placeholder' => 'Kata dalam Biografi', 'id' => 'biographyblacklist', 'class' => 'form-control', 'required' => 'false', 'data-required-error' => 'Harus diisi', 'data-role' => 'tagsinput']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group follow-filter">
                            <label>Hashtag lain dalam konten yang sama yang <strong>Dihindari</strong>, tanpa spasi, tanpa #, misal <strong>dagelan</strong>, pisahkan dengan koma, dapat dikosongkan.</label>
                            <?php echo $this->Form->text('hashtagblacklist', ['placeholder' => 'Hashtag Lain dalam Konten', 'id' => 'hashtagblacklist', 'class' => 'form-control', 'required' => 'false', 'data-required-error' => 'Harus diisi', 'data-role' => 'tagsinput']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
<?php echo $this->Form->submit('Tambah', ['class' => 'btn btn-primary btn-block']); ?>
                        </div><!--/. form-group -->
                    </div><!--/.dt-bootstrap -->
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>

<div id="modalWarning" class="modal fade modal-warning" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hiddent="True">x</span></button>
                <h4 class="modal-title">Perhatian</h4>
            </div><!--/.modal-header -->
            <div class="modal-body">
                <p id="warningText">Tentukan hashtag</p>
            </div><!--/.modal-body -->
            <div class="modal-footer">
                <button class="btn btn-outline pull-right" type="button" data-dismiss="modal">Tutup</button>
            </div><!--/.modal-footer -->
        </div><!--/.modal-content -->
    </div><!--/.modal-dialog -->
</div><!--/.modalWarning -->

<?php echo $this->Html->script('bootstrap-tagsinput.min'); ?>
<?php echo $this->Html->script('bootstrap-checkbox.min'); ?>

<script>
$(function() {
    $('.follow-filter').hide();

    $('#filtercheckbox').checkboxpicker({
        offLabel: 'Tidak',
        onLabel: 'Ya'
    }).on('change', function() {
        if($(this).prop('checked')) {
            $('#filter').val(1);
            $('.follow-filter').show(1000);
        } else {
            $('#filter').val(0);
            $('.follow-filter').hide(1000);
        }
    });

    // To remove comma after label
    $('#fullnameblacklist').on('itemAdded', function(event) {
        let $field = $(this).siblings('.bootstrap-tagsinput').find('input');
        setTimeout(function() {
            $field.val('');
        });
    });
    $('#biographyblacklist').on('itemAdded', function(event) {
        let $field = $(this).siblings('.bootstrap-tagsinput').find('input');
        setTimeout(function() {
            $field.val('');
        });
    });
    $('#hashtagblacklist').on('itemAdded', function(event) {
        let $field = $(this).siblings('.bootstrap-tagsinput').find('input');
        setTimeout(function() {
            $field.val('');
        });
    });


    function sleep(milliseconds) {
        let start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
                break;
            }
        }
    }
  
    $('#addForm').submit(function() {
        if ($('#caption').val() === '') {
            //removeModalClass(2);
            $('#warningText').text('Tentukan Hashtag');

            return false;
        }
        if ($('#filter').val() == 1) {
            if ($('#fullnameblacklist').val() === '' && $('#biographyblacklist').val() === '' && $('#hashtagblacklist').val() === '') {
                $('#warningText').text('Isi salah satu dari ketiga filter');
                $('#modalWarning').modal('show');
                return false;
            }
        }
    });
});
</script>
