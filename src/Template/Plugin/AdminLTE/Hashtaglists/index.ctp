<section class="content-header">
    <h1>Daftar Hashtags</h1>
    <ol class="breadcrumb">
        <li class="active">
            Hashtag
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="col-xs-4 pull-left">
                    <?php echo $this->Html->link('Tambah', ['controller' => 'Hashtaglists', 'action' => 'addwhite']); ?>
                    </div>
                    <div class="col-xs-4 pull-right">
                    </div><!--/.col-xs-4 -->
                </div><!--/.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php if (count($data) < 1) {?>
                                <h1>Belum terdapat data, <?php echo $this->Html->link('Tambah', ['controller' => 'Hashtaglists', 'action' => 'add']); ?></h1>
                            <?php } else {?>
                            <table id="dataTable" class="table table-bordered table-striped dataTable" role="grid">
                                <thead>
                                    <tr>
                                        <td>Hashtag</td>
                                </thead>
                                <tbody>
                                    <?php foreach ($data as $datum) { ?>
                                        <tr>
                                            <td>
                                                <div class="container-fluid">
                                                    <div class="row" style="font-weight: 400">
                                                        <p>
                                                            <strong><?php echo $datum['caption'];?></strong>
                                                        </p>
                                                    </div>
                                                    <!--<div class="row pull-right">
                                                        <a href="<?php echo $this->Url->build('/hashtaglists/edit/' . $datum['id']); ?>">
                                                            <i class="fa fa-pencil"></i> <span>Ubah</span>
                                                        </a>
                                                        <a href="<?php echo $this->Url->build('/hashtaglists/delete/' . $datum['id']); ?>">
                                                            <i class="fa fa-trash"></i> <span>Hapus</span>
                                                        </a>
                                                    </div>-->
                                                </div><!--/ .container-fluid -->
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php }?>
                        </div><!--/.col-sm-12 -->
                    </div><!--/.row table body -->
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>
<script>
$(document).ready(function() {
    $('#dataTable').DataTable();
});
</script>
