<section class="content-header">
    <h1>Lihat Akun Instagram di-Repost</h1>
    <ol class="breadcrumb">
        <li>Auto Post</li>
        <li>Repost</li>
        <li>Akun di-Repost</li>
        <li class="active">
            Lihat
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <i class="fa fa-mobile"></i> <h3 class="box-title">Profil</h3>
                    <a href="/replicatinglists/edit/<?php echo $account['id']; ?>" class="pull-right">
                        <i class="fa fa-pencil"> </i><span>Ubah</span>
                    </a>
                </div><!--/.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-1">
                            <div class="box box-widget widget-user-2">
                                <div class="widget-user-header bg-blue">
<?php if(!empty($account['member'])) { ?>
                                    <div class="widget-user-image">
                                    <img class="img-circle" src="<?php echo $account['member']['profpicurl']; ?>">
                                    </div><!--/.widget-user-image -->
<?php } ?>
                                    <h3 class="widget-user-username"><?php echo $account['username']; ?></h3>
<?php if(!empty($account['member'])) { ?>
                                    <h5 class="widget-user-desc"><?php echo $account['member']['description']; ?></h5>
<?php } ?>
                                </div><!--/.widget-user-header -->
                                <div class="box-footer no-padding">
                                    <ul class="nav nav-stacked">
                                        <li><a href="#">Posts<span class="pull-right badge bg-aqua"><?php echo $account['contents']; ?></span></a></li>
<?php if(!empty($account['member'])) { ?>
                                        <li><a href="#">Followers<span class="pull-right badge bg-aqua"><?php echo $account['member']['followers']; ?></span></a></li>
                                        <li><a href="#">Followings<span class="pull-right badge bg-aqua"><?php echo $account['member']['followings']; ?></span></a></li>
<?php } ?>
                                    </ul><!--/.nav -->
                                </div><!--/.box-footer -->
                            </div><!--/.box-widget -->
                        </div><!--/.col-md-4 -->
                        <div class="col-md-4 col-md-offset-1">
                            <div class="box box-widget widget-user-2">
                                <div class="widget-user-header bg-blue">
                                    <h3 class="widget-user-username">Setup</h3>
                                </div><!--/.widget-user-header -->
                                <div class="box-footer no-padding">
                                    <ul class="nav nav-stacked">
                                        <li><a href="#">Jadwal Repost Harian<span class="pull-right badge bg-aqua"><?php echo $this->Time->format($uploadat, 'HH:mm'); ?></span></a></li>
                                        <li><a href="<?php echo $this->Url->build(['controller' => 'replicas', 'action' => 'queue', $account['id']]); ?>">Konten akan di-Repost<span class="pull-right badge bg-aqua"><?php echo $unreposted; ?></span></a></li>
                                        <li><a href="<?php echo $this->Url->build(['controller' => 'replicas', 'action' => 'index', $account['id']]); ?>">Konten telah di-Repost<span class="pull-right badge bg-aqua"><?php echo $reposted; ?></span></a></li>
                                    </ul><!--/.nav -->
                                </div><!--/.box-footer -->
                            </div><!--/.box-widget -->
                        </div><!--/.col-md-4 -->
                    </div><!--/.row -->
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>
