<?php
echo $this->Html->css('bootstrap-datetimepicker.min');
?>

<section class="content-header">
    <h1>Ubah Akun di-Repost</h1>
    <ol class="breadcrumb">
        <li>Auto Post</li>
        <li>Repost</li>
        <li>Akun di-Repost</li>
        <li class="active">
            Ubah
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <?php echo $this->Html->link('Daftar Akun', ['controller' => 'Replicatinglists', 'action' => 'index']); ?>
                </div><!--/.box-header -->
                <div class="box-body">
                    <div class="dt-bootstrap">
                    <?php echo $this->Form->create('Replicatinglists.add', ['id' => 'addForm', 'data-toggle' => 'validator']); ?>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                <?php echo $this->Form->select('account_id', $accounts, [
                                    'class' => 'form-control',
                                    'id' => 'accountId',
                                    'required' => 'true',
                                    'default' => $replicatinglist['account_id']]); ?>
                            </div><!--/.input-group -->
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
                            <?php echo $this->Form->text('username', [
                                'placeholder' => 'Username Akun IG yang kontennya akan di-Duplikat, diisi tanpa @, mis. miyazghayda',
                                'class' => 'form-control',
                                'id' => 'username',
                                'required' => 'true',
                                'data-required-error' => 'Harus diisi',
                                'value' => $replicatinglist['username']]); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                <?php echo $this->Form->text('uploadatShow', [
                                    'class' => 'form-control',
                                    'id' => 'uploadatShow',
                                    'required' => 'false',
                                    'placeholder' => 'Jadwal upload sekali dalam sehari, dapat diubah untuk masing-masing konten melalui Menu Auto Post > Repost > Konten akan di-Repost',
                                    'required' => 'required',
                                    'data-required-error' => 'Harus diisi',
                                    'value' => $this->Time->format($uploadat, 'HH:mm')]); ?>
                                <?php echo $this->Form->hidden('uploadat', ['id' => 'uploadat', 'value' => $this->Time->format($uploadat, 'HH:mm')]);?>
                            </div><!--/.input-group -->
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
<?php echo $this->Form->submit('Ubah', ['class' => 'btn btn-primary btn-block']); ?>
                        </div><!--/. form-group -->
                    </div><!--/.form-inline -->
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>
<?php
echo $this->Html->script('bootstrap-datetimepicker.min');
?>
<script>
$(function() {
    let schedule = $('#uploadatShow').datetimepicker({
    locale: 'id',
        sideBySide: true,
        format: 'HH:mm'
    });

    schedule.on('dp.change', function(e) {
        let scheduleTime = $('#uploadatShow').val() + ':00';
        if (scheduleTime == ':00') scheduleTime = '';
        $('#uploadat').val(scheduleTime);
    });
});
</script>