<section class="content-header">
    <h1>Akun di-Repost</h1>
    <ol class="breadcrumb">
        <li>Auto Post</li>
        <li>Repost</li>
        <li class="active">Akun di-Repost</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <?php echo $this->Html->link('Tambah Akun', ['controller' => 'Replicatinglists', 'action' => 'add']); ?>
                    <div class="col-xs-4 pull-right">
                    </div><!--/.col-xs-4 -->
                </div><!--/.box-header -->
                <div class="box-body">
                    <?php if (count($replicatinglists) < 1) { ?>
                        <p style="font-weight: 400">Belum ada akun.</p>
                    <?php } else { ?>
                        <table id="dataTable" class="table table-bordered table-striped dataTable" role="grid">
                            <thead>
                                <tr>
                                    <td>Akun</td>
                                    <!--<td>Url</td>
                                    <td>Menu</td>-->
                            </thead>
                            <tbody>
                                <?php foreach ($replicatinglists as $r) { ?>
                                    <tr>
                                        <!--<td><?php echo $this->Html->link($r['username'], ['controller' => 'replicatinglists', 'action' => 'view', $r['id']]); ?></td>
                                        <td><?php echo $this->Html->link('https://instagram.com/' . $r['username'], 'https://instagram.com/' . $r['username'], ['target' => '_blank']); ?></td>
                                        <td>
                                            <a href="<?php echo $this->Url->build('/replicatinglists/view/' . $r['id']); ?>">
                                                <i class="fa fa-desktop"></i> <span>Lihat</span>
                                            </a>
                                            <a href="<?php echo $this->Url->build('/replicatinglists/edit/' . $r['id']); ?>">
                                                <i class="fa fa-pencil"></i> <span>Ubah</span>
                                            </a>
                                            <a href="<?php echo $this->Url->build('/replicatinglists/delete/' . $r['id']); ?>">
                                                <i class="fa fa-trash"></i> <span>Hapus</span>
                                            </a>
                                        </td>-->
                                        <td>
                                            <div class="container-fluid">
                                                <div class="row" style="font-weight: 400">
                                                    <p>
                                                        <strong><?php echo $r['username'];?></strong>
                                                    </p>
                                                    <p>
                                                    </p>
                                                </div>
                                                <div class="row pull-right">
                                                    <a href="<?php echo $this->Url->build('/replicatinglists/view/' . $r['id']); ?>">
                                                        <i class="fa fa-desktop"></i> <span>Lihat</span>
                                                    </a>
                                                    <a href="<?php echo $this->Url->build('/replicatinglists/edit/' . $r['id']); ?>">
                                                        <i class="fa fa-pencil"></i> <span>Ubah</span>
                                                    </a>
                                                    <a href="<?php echo $this->Url->build('/replicatinglists/delete/' . $r['id']); ?>">
                                                        <i class="fa fa-trash"></i> <span>Hapus</span>
                                                    </a>
                                                </div>
                                            </div><!--/ .container-fluid -->
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>
<script>
$(function() {
    $('#accountId').on('change', function() {
        window.location.replace('/replicatinglists/index/' + $(this).val());
    });
});
</script>
