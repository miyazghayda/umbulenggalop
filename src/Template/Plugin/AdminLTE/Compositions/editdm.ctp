<?php echo $this->Html->css('bootstrap-tagsinput'); ?>
<style>
label {
    font-weight: 400;
}
.bootstrap-tagsinput {
    width: 100%;
}
.bootstrap-tagsinput input {
    width: 100%;
}
</style>

<section class="content-header">
    <h1>Ubah Template DM</h1>
    <ol class="breadcrumb">
        <li>
            DM
        </li>
        <li class="active">
            Ubah
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <?php echo $this->Html->link('Daftar Template Komentar', ['controller' => 'Compositions', 'action' => 'index']); ?>
                </div><!--/.box-header -->
                <div class="box-body">
                    <div class="dt-bootstrap">
                    <?php echo $this->Form->create('Compositions.editdm', ['id' => 'addForm', 'data-toggle' => 'validator']); ?>
                        <div class="form-group">
                            <label>Template DM. Dapat disisipkan <strong>Kata khusus</strong></label>
                            <?php
                            echo $this->Form->textarea('caption', [
                                'placeholder' => 'Template DM, mis. "Terimakasih, Kak @frontname karena telah follow kami"',
                                'class' => 'form-control',
                                'id' => 'caption',
                                'required' => 'true',
                                'data-required-error' => 'Harus diisi',
                                'value' => $composition['caption']]); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->
                                               
                        <div class="form-group">
<?php echo $this->Form->submit('Ubah', ['class' => 'btn btn-primary btn-block']); ?>
                        </div><!--/. form-group -->

                        <div class="form-group" style="font-weight: 400;">
                            <p>Daftar <strong>Kata Khusus</strong></p>
                            <ul>
                                <li>
                                <strong>@username</strong> gunakan untuk menampilkan <strong>username</strong> pemilik konten
                                </li>
                                <li>
                                <strong>@fullname</strong> akan menampilkan <strong>Nama Lengkap</strong> pemilik konten.
                                </li>
                                <li>
                                <strong>@frontname</strong> akan menampilkan <strong>Nama Depan</strong> pemilik konten.
                                </li>
                            </ul>
                        </div><!--/. form-group -->
                    </div><!--/.dt-bootstrap -->
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>

<script>
$(function() {
    $('#addForm').submit(function() {
        if ($('#caption').val() === '') {
            $('#warningText').text('Tentukan Komentar');

            return false;
        }
    });
});
</script>