<?php echo $this->Html->css('bootstrap-tagsinput'); ?>
<style>
label {
    font-weight: 400;
}
.bootstrap-tagsinput {
    width: 100%;
}
.bootstrap-tagsinput input {
    width: 100%;
}
</style>

<section class="content-header">
    <h1>Hapus Template Komentar</h1>
    <ol class="breadcrumb">
        <li>
            Comments
        </li>
        <li class="active">
            Hapus
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <?php echo $this->Html->link('Daftar Template Komentar', ['controller' => 'Compositions', 'action' => 'index']); ?>
                </div><!--/.box-header -->
                <div class="box-body">
                    <div class="dt-bootstrap">
                    <?php echo $this->Form->create('Compositions.deletecomment', ['id' => 'addForm']); ?>
                        <div class="form-group">
                            <p style="font-weight: 400">Yakin akan menghapus template <strong><?php echo $composition['caption']; ?></strong>?</p>
                        </div><!--/. form-group -->
                                               
                        <div class="form-group">
<?php echo $this->Form->submit('Hapus', ['class' => 'btn btn-primary btn-block']); ?>
                        </div><!--/. form-group -->
                    </div><!--/.dt-bootstrap -->
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>
