<section class="content-header">
    <h1>FAQ <small>Frequently Asked Question</small></h1>
    <ol class="breadcrumb">
        <li class="active">FAQ</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                </div><!--/.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php echo $this->element('faq/safety'); ?>

                            <?php echo $this->element('faq/engagement'); ?>

                            <?php echo $this->element('faq/maxlikeperday'); ?>

                            <?php echo $this->element('faq/maxfollowperday'); ?>

                            <?php echo $this->element('faq/maxunfollowperday'); ?>

                            <?php echo $this->element('faq/maxdmperday'); ?>
                        </div><!--/.col-sm-12 -->
                    </div><!--/.row table body -->
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>

<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-MML-AM_CHTML"></script>
<?php
echo $this->Html->script('adminlte.min');
?>
<script>
$(document).ready(function() {
    function hashchanged() {
        let url = $(location).attr('href');
        url = url.split('#');
        $('.collapsed-box').boxWidget('collapse');
        if (url[1] !== null) {
            $('#' + url[1]).boxWidget('expand');
        } else {
            $('#safety').boxWidget('expand');
        }
    }
    window.addEventListener('hashchange', hashchanged, false);
    hashchanged();

    MathJax.Hub.Config({
        tex2jax: {
            inlineMath: [['$','$'], ['\\(','\\)']],
            processEscapes: true
        }
    });

});
</script>
