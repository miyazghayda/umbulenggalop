<?php
if (count($data) > 0) {?>
<div class="table-responsive">
    <table class="table table-striped">
        <tbody>
        <?php foreach ($data as $datum) {?>
            <tr>
                <td>
                <?php echo $this->Html->link(
                    $datum['username'],
                    '/accounts/picked/' . $datum['messy']
                ); ?>
                <span class="pull-right">
                    <a href="<?php echo $this->Url->build('/accounts/delete/' . $datum['messy']); ?>">
                        <i class="fa fa-trash"></i>
                    </a>
                </span>
                </td>
            </tr>
        <?php }?>
                <tr>
                    <td><?php
                    echo $this->Html->link('Tambah', ['controller' => 'Accounts', 'action' => 'addnew']);
                    ?></td>
                </tr>
        </tbody>
    </table>
</div>
<?php }?>