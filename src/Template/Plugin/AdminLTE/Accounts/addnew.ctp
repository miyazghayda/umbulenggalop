<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="dt-bootstrap">
                    <?php echo $this->Form->create('Accounts.add', ['id' => 'addForm', 'data-toggle' => 'validator']); ?>

                        <div class="form-group">
                            <label>Username</label>
                            <?php echo $this->Form->text('username', ['placeholder' => 'Username Akun IG, diisi tanpa @, mis. miyazghayda', 'class' => 'form-control', 'id' => 'username', 'required' => 'true', 'data-required-error' => 'Harus diisi', 'autocomplete' => 'off']); ?>
                            <?php echo $this->Form->hidden('proxy_id', ['value' => 1, 'id' => 'proxyid']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
                            <label>Password</label>
                            <div class="input-group">
                                <?php echo $this->Form->text('password', ['placeholder' => 'Password Akun IG', 'class' => 'form-control', 'id' => 'password', 'required' => 'true', 'data-required-error' => 'Harus diisi', 'autocomplete' => 'off']); ?>
                                <span class="input-group-btn"><button class="btn btn-default" id="btnTogglePassword"><i class="fa fa-eye"></i></button></span>
                            </div>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
                            <button class="btn btn-primary btn-block" id="btnCheck">Coba Login</button>
<?php echo $this->Form->submit('Tambah', ['class' => 'btn btn-primary btn-block', 'id' => 'btnSubmit', 'disabled' => 'disabled']); ?>
                        </div><!--/. form-group -->

                        <div class="form-group" style="font-weight: 400">
                            <label>Perhatian</label>
                            <p>
                            <strong>Pastikan</strong> Anda telah membaca <a href="/documentation/faq#safety" target="_blank">ini</a> dan memahami
                            semua risiko yang mungkin terjadi pada penggunaan layanan ini. Kami tidak dapat dikenakan
                            pertanggung jawaban atas semua hal yang menjadi dampak dari penggunaan <strong>Umbul Enggal (UE)</strong>.
                            </p>
                            <p>
                            Agar proses penambahan akun berhasil, pastikan saat ini Anda <strong>sedang membuka</strong> akun tersebut, misal pada Aplikasi
                            pada <i>Handphone</i> atau melalui web agar Anda mengetahui penyebab kegagalan proses ini. Pesan kesalahan yang umum antara lain:
                            </p>
                            <ol>
                            <li><strong>The password you entered is incorrect</strong>, terdapat kekeliruan pada username/password.</li>
                            <li><strong>Challenge required</strong> silahkan lihat lebih lanjut pada akun yang Anda buka melalui <i>Handphone</i> atau web untuk mengetahui kesalahan yang terjadi. Kesalahan terjadi biasanya karena:
                                <ul>
                                    <li>IG mendeteksi akun dibuka dari lokasi yang tidak biasa. Anda dapat memperbaiki dengan
                                    memilih <strong>This was me</strong> pada web/handphone.</li>
                                    <li>IG meminta Anda menambahkan no handphone pada akun.</li>
                                </ul>
                            </li>
                            <li>Pastikan Anda <strong>telah memperbaiki kesalahan di atas</strong> sebelum mengklik <strong>Coba Login</strong> kembali.</li>
                            </ol>
                        </div><!--/. form-group -->
                    </div><!--/.form-inline -->
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>

<div id="modalWarning" class="modal fade" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hiddent="True">x</span></button>
                <h4 class="modal-title">Perhatian</h4>
            </div><!--/.modal-header -->
            <div class="modal-body">
                <p id="warningText">Tentukan username yang terdapat dalam IG</p>
            </div><!--/.modal-body -->
            <div class="modal-footer">
                <button class="btn btn-outline pull-right" type="button" data-dismiss="modal">Tutup</button>
            </div><!--/.modal-footer -->
        </div><!--/.modal-content -->
    </div><!--/.modal-dialog -->
</div><!--/.modalWarning -->


<script type='text/javascript'>
$(function() {
    $('#btnSubmit').hide();
    $('#btnSubmit').prop('disabled', true);
    $('#password').attr('type', 'password');
    $('#btnTogglePassword').click(function() {
        let password = $('#password');
        if (password.attr('type') == 'password') {
            password.attr('type', 'text');
        } else {
            password.attr('type', 'password');
        }
        return false;
    });
    
    function sleep(milliseconds) {
        let start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
                break;
            }
        }
    }

    $('#modalWarning').on('show.bs.modal', function() {
        $(this).removeClass('modal-info');
    });

    function removeModalClass(option) {
        let modal = $('#modalWarning');
        if (option === 1) {// info
            if (modal.hasClass('modal-warning')) modal.removeClass('modal-warning');
            if (modal.hasClass('modal-success')) modal.removeClass('modal-success');
            modal.addClass('modal-info');
        } else if (option === 2) {// warning
            if (modal.hasClass('modal-success')) modal.removeClass('modal-success');
            if (modal.hasClass('modal-info')) modal.removeClass('modal-info');
            modal.addClass('modal-warning');
        } else if (option === 3) {// success
            if (modal.hasClass('modal-info')) modal.removeClass('modal-info');
            if (modal.hasClass('modal-warning')) modal.removeClass('modal-warning');
            modal.addClass('modal-success');
        }
    }

    $('#btnCheck').click(function() {
        let username = $('#username').val();
        let password = $('#password').val();
        if (username !== null && username !== '' && password !== null && password !== '') {
            $.ajax({
                url: '/accounts/check/' + username + '/' + password,
                beforeSend: function() {
                    let t = 'Memeriksa Akun ' + username + ', silahkan menunggu...';
                    removeModalClass(1);
                    $('#warningText').text(t);
                    $('#modalWarning').modal();
                }
            }).done(function(data) {
                let resp = data;
                if (data.status == '1') {
                    removeModalClass(3);
                    $('#warningText').text('Login Berhasil');
                    $('#proxyid').val(data.proxy_id);
                    sleep(3);
                    $('#addForm').submit();
                } else if (data.status == '0') {
                    removeModalClass(2);
                    $('#warningText').text(resp.text);
                }
            });
        }
        return false;
    });

});
</script>