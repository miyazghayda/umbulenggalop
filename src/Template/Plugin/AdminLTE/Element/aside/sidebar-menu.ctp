<?php
use Cake\Core\Configure;

$file = Configure::read('Theme.folder'). DS . 'src' . DS . 'Template' . DS . 'Element' . DS . 'aside' . DS . 'sidebar-menu.ctp';
if (file_exists($file)) {
    ob_start();
    include_once $file;
    echo ob_get_clean();
} else {
?>
<ul class="sidebar-menu">
    <li class="header">FITUR</li>
    <li class="treeview">
        <a href="<?php echo $this->Url->build('/accounts'); ?>">
        <i class="fa fa-users"></i> <span><?php echo $accountUsername; ?></span>
        </a>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-camera-retro"></i>
            <span>Auto Post</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-industry"></i>
                    <span>Konten Sendiri</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo $this->Url->build('/cargos/queue'); ?>"><i class="fa fa-hourglass-2"></i> Konten akan di-Post</a></li>
                    <li><a href="<?php echo $this->Url->build('/cargos/'); ?>"><i class="fa fa-image"></i> Konten telah di-Post</a></li>
                    <li><a href="<?php echo $this->Url->build('/cargos/story'); ?>"><i class="fa fa-caret-square-o-left"></i> Story akan di-Post</a></li>
                    <li><a href="<?php echo $this->Url->build('/cargos/archive'); ?>"><i class="fa fa-caret-square-o-right"></i> Story telah di-Post</a></li>

                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Repost</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo $this->Url->build('/replicatinglists'); ?>"><i class="fa fa-user"></i> Akun di-Repost</a></li>
                    <li><a href="<?php echo $this->Url->build('/replicas/queue'); ?>"><i class="fa fa-hourglass-2"></i> Konten akan di-Repost</a></li>
                    <li><a href="<?php echo $this->Url->build('/replicas'); ?>"><i class="fa fa-image"></i> Konten telah di-Repost</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <!--<li class="treeview">
        <a href="#">
            <i class="fa fa-user-plus"></i>
            <span>Auto Follow</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-child"></i>
                    <span>Follower(s) Akun Lain</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo $this->Url->build('/accountlists/activateidol'); ?>"><i class="fa fa-plug"></i> Aktifkan</a></li>
                    <li><a href="<?php echo $this->Url->build('/accountlists/idol'); ?>"><i class="fa fa-child"></i> Akun</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-slack"></i>
                    <span>Dari Hashtag(s)</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo $this->Url->build('/hashtaglists/activate'); ?>"><i class="fa fa-plug"></i> Aktifkan</a></li>
                    <li><a href="<?php echo $this->Url->build('/hashtaglists'); ?>"><i class="fa fa-slack"></i> Hashtag(s)</a></li>
                </ul>
            </li>
            <li><a href="<?php echo $this->Url->build('/followinglists/index'); ?>"><i class="fa fa-child"></i> Akun telah di-Follow</a></li>
        </ul>
    </li>-->

    <!--<li class="treeview">
        <a href="#">
            <i class="fa fa-heart"></i>
            <span>Auto Like/Comment</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo $this->Url->build('/preferences/likecommentnewfollower'); ?>"><i class="fa fa-odnoklassniki-square"></i> Follower Baru</a></li>
            <li><a href="<?php echo $this->Url->build('/compositions/comments'); ?>"><i class="fa fa-comments"></i> Template Komentar</a></li>
        </ul>
    </li><!--/ .treeview -->

    <li class="treeview">
        <a href="#">
            <i class="fa fa-envelope"></i>
            <span>Auto Direct Message (DM)</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo $this->Url->build('/preferences/dmnewfollower'); ?>"><i class="fa fa-odnoklassniki-square"></i> Follower Baru</a></li>
            <li><a href="<?php echo $this->Url->build('/compositions/dms'); ?>"><i class="fa fa-file-text"></i> Template Pesan</a></li>
        </ul>
    </li><!--/ .treeview -->

    <li class="treeview">
        <a href="#">
            <i class="fa fa-binoculars"></i>
            <span>Riset</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo $this->Url->build('/hashtags/seeposts'); ?>"><i class="fa fa-film"></i> Konten pada Hashtag</a></li>
            <li><a href="<?php echo $this->Url->build('/hashtags/search'); ?>"><i class="fa fa-search"></i> Cari Hashtag</a></li>
        </ul>
    </li><!--/ .treeview -->

    <li><a href="<?php echo $this->Url->build('/documentation/faq'); ?>"><i class="fa fa-question-circle"></i> <span>FAQ</span></a></li>
</ul>
<?php } ?>
