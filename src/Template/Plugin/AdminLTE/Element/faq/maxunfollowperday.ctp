<div class="box box-widget collapsed-box" id="maxunfollowperday">
    <div class="box-header with-border">
        <div class="user-block">
            <span class="username">Maksimum Unfollow per Hari</span>
            <span class="description">Maksimum Unfollow yang Dapat Dilakukan Tiap Hari</span>
        </div><!--/.user-block -->
        <div class="box-tools">
            <button class="btn btn-box-tool" type="button" data-widget="collapse">
                <i class="fa fa-plus"></i>
            </button><!--/.btn-box-tool -->
        </div><!--/.box-tools -->
    </div><!--/.box-header -->
    <div class="box-body">
        <p style="font-weight: 400;">
            Sama seperti <a href="maxfollowperda">maksimum follow</a> tidak terdapat batasan berapapun Unfollow per hari namun kami menyarankan untuk membatasi <strong>maksimal 1000 Unfollows </strong>perhari. Sedangkan untuk <strong>akun baru</strong> disarankan hanya maksimal <strong>500</strong> per hari. Hal tersebut untuk menghindari akun Anda terkena <strong>shadowban</strong> atau malah <strong>tersuspend</strong> permanen.
        </p>
        <p style="font-weight: 400">
            Dalam menentukan jumlah Unfollow perhari yang harus diperhatikan sama dengan <a href="#maxlikeperday">maksimum Like</a> dan <a href="#maxfollowperday">maksimum Follow</a>. Untuk menambah keamanan pada akun Anda, sistem ini akan menggunakan <strong>jeda 28-38 detik</strong> antar Unfollow.
        </p>
        <p>
            Referensi:
        </p>
        <ol>
            <li><a href="https://www.quora.com/What-are-the-limits-of-follow-and-unfollow-on-instagram-per-hour" target="_blank">https://www.quora.com/What-are-the-limits-of-follow-and-unfollow-on-instagram-per-hour</a></li>
            <li><a href="https://www.androidtipster.com/instagram-limits/" target="_blank">https://www.androidtipster.com/instagram-limits/</a></li>
            <li><a href="https://www.quora.com/What-are-hourly-daily-limits-for-follows-likes-and-comments-on-instagram" target="_blank">https://www.quora.com/What-are-hourly-daily-limits-for-follows-likes-and-comments-on-instagram</a></li>
            <li><a href="https://later.com/blog/instagram-shadowban/" target="_blank">https://later.com/blog/instagram-shadowban/</a></li>
        </ol>
    </div><!--/.box-body -->
</div><!--/.box box-widget -->
