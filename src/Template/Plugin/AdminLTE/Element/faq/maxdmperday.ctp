<div class="box box-widget collapsed-box" id="maxdmperday">
    <div class="box-header with-border">
        <div class="user-block">
            <span class="username">Maksimum DM per Hari</span>
            <span class="description">Maksimum DM yang Dapat Dilakukan Tiap Hari</span>
        </div><!--/.user-block -->
        <div class="box-tools">
            <button class="btn btn-box-tool" type="button" data-widget="collapse">
                <i class="fa fa-plus"></i>
            </button><!--/.btn-box-tool -->
        </div><!--/.box-tools -->
    </div><!--/.box-header -->
    <div class="box-body">
        <p style="font-weight: 400;">
            Tidak terdapat batasan berapapun DM (percakapan baru) yang dapat dikirimkan per hari
            namun kami menyarankan untuk membatasi <strong>maksimal 30 DM </strong>perhari.&nbsp;
            Sedangkan untuk <strong>akun baru</strong> disarankan hanya maksimal <strong>10</strong>&nbsp;
            per hari. Hal tersebut untuk menghindari akun Anda terkena <strong>shadowban</strong>&nbsp;
            atau malah <strong>tersuspend</strong> permanen.
        </p>
        <p style="font-weight: 400">
            Kami belum menemukan jumlah pasti atau hal yang menjadi penyebab banyaknya DM yang dapat&nbsp;
            dikirimkan perhari. Jika Anda mengetahui hal tersebut, akan menyenangkan bila&nbsp;
            Anda dapat membagi pengetahuan tersebut pada kami.
        </p>
        <p>
            Referensi:
        </p>
        <ol>
            <li><a href="https://www.blackhatworld.com/seo/instagram-dm-limits-2018.1032631/" target="_blank">https://www.blackhatworld.com/seo/instagram-dm-limits-2018.1032631/</a></li>
            <li><a href="https://mpsocial.com/t/instagram-dm-limit-2017/28394/25" target="_blank">https://mpsocial.com/t/instagram-dm-limit-2017/28394/25</a></li>
            <li><a href="https://www.quora.com/How-many-messages-can-we-send-on-Instagram-per-day">https://www.quora.com/How-many-messages-can-we-send-on-Instagram-per-day</a></li>
        </ol>
    </div><!--/.box-body -->
</div><!--/.box box-widget -->
