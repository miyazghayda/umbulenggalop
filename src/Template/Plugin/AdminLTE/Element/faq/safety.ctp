<div class="box box-widget collapsed-box" id="safety">
    <div class="box-header with-border">
        <div class="user-block">
            <span class="username">Keamanan</span>
            <span class="description">Apakah Layanan Ini Aman?</span>
        </div><!--/.user-block -->
        <div class="box-tools">
            <button class="btn btn-box-tool" type="button" data-widget="collapse">
                <i class="fa fa-plus"></i>
            </button><!--/.btn-box-tool -->
        </div><!--/.box-tools -->
    </div><!--/.box-header -->
    <div class="box-body">
        <p style="font-weight: 400;">
            <strong>Tidak</strong> layanan otomasi IG seperti <strong>Umbul Enggal (UE)</strong> ini tidak aman.
            Antara lain karena:
        </p>
        <ul style="font-weight: 400">
            <li>Anda memberikan username + password akun IG Anda pada pihak lain</li>
            <li>
                Akun IG dibuka dari lokasi yang tidak lazim, misal Anda membuka akun <strong>A</strong>
                dari tempat Anda di <strong>Kota B</strong> yang berlokasi di Indonesia&nbsp;
                sedang server UE berada di Singapura/Amerika.
            </li>
            <li>
                IG melarang setiap tindakan otomasi.
            </li>
            <li>
                Layanan otomasi selalu menggoda pengguna untuk lebih agresif, misalnya aktif mengomentari
                konten orang lain atau mengirimi pesan (DM) pada banyak orang, hal tersebut dapat mengganggu
                dan berisiko dilaporkan kepada IG.
            </li>
        </ul>
        <p style="font-weight: 400">
            Dengan menggunakan UE maka Anda dianggap telah membaca dan paham mengenai risiko-risiko
            seperti <strong>shadowbanned</strong> atau bahkan <strong>suspend</strong> permanen.
            Segala risiko yang timbul akibat penggunaan UE adalah <strong>tanggungjawab pengguna</strong>.
        </p>
        <p>Referensi:</p>
        <ol>
            <li><a href="https://help.instagram.com/477434105621119/?helpref=hc_fnav&bc[0]=368390626577968&bc[1]=285881641526716" target="_blank">https://help.instagram.com/477434105621119/?helpref=hc_fnav&bc[0]=368390626577968&bc[1]=285881641526716</a></li>
            <li><a href="https://help.instagram.com/581066165581870" target="_blank">https://help.instagram.com/581066165581870</a></li>
            <li><a href="https://blog.hootsuite.com/i-tried-instagram-automation-so-you-dont-have-to/" target="_blank">https://blog.hootsuite.com/i-tried-instagram-automation-so-you-dont-have-to/</a></li>
        </ol>
    </div><!--/.box-body -->
</div><!--/.box box-widget -->
