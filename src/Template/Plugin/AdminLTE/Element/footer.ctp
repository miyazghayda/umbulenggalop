<?php
use Cake\Core\Configure;

$file = Configure::read('Theme.folder') . DS . 'src' . DS . 'Template' . DS . 'Element' . DS . 'footer.ctp';

if (file_exists($file)) {
    ob_start();
    include_once $file;
    echo ob_get_clean();
} else {
?>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Based on great works of <a href="https://github.com/mgp25/Instagram-API">mgp25</a>.</strong> Keluhan/Saran silahkan melalui Akun Telegram <a href="https://t.me/miyazghayda" target="_blank">@miyazghyada</a>.
</footer>
<?php } ?>
