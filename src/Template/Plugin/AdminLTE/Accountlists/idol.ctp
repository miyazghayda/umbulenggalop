<section class="content-header">
    <h1>Daftar Akun dengan Follower Target</h1>
    <ol class="breadcrumb">
        <li class="active">
            Akun
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <?php echo $this->Html->link('Tambah', ['controller' => 'Accountlists', 'action' => 'addidol']); ?>
                </div><!--/.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php if (count($accounts) < 1) {?>
                                <h1>Belum terdapat data, <?php echo $this->Html->link('Tambah', ['controller' => 'Accountlists', 'action' => 'addidol']); ?></h1>
                            <?php } else {?>
                            <table id="dataTable" class="table table-bordered table-striped dataTable" role="grid">
                                <thead>
                                    <tr>
                                        <!--<td>Username</td>
                                        <td>Fullname</td>
                                        <td>Url</td>
                                        <td>Followers</td>
                                        <!--<td>Menu</td>-->
                                        <td>Akun</td>
                                </thead>
                                <tbody>
                                    <?php foreach ($accounts as $account) { ?>
                                        <tr>
                                            <!--<td><?php echo $account['Members']['username']; ?></td>
                                            <td><?php echo $account['Members']['fullname']; ?></td>
                                            <td><?php echo $this->Html->link('https://instagram.com/' . $account['Members']['username'], 'https://instagram.com/' . $account['Members']['username'], ['target' => '_blank']); ?></td>
                                            <td><?php echo $this->Number->format($account['Members']['followers'], ['locale' => 'id_ID']); ?></td>
                                            <!--<td>
                                                <a href="<?php echo $this->Url->build('/accounts/view/' . $account['id']); ?>">
                                                    <i class="fa fa-desktop"></i> <span>Lihat</span>
                                                </a>
                                                <a href="<?php echo $this->Url->build('/accounts/edit/' . $account['id']); ?>">
                                                    <i class="fa fa-pencil"></i> <span>Ubah</span>
                                                </a>
                                                <a href="<?php echo $this->Url->build('/accounts/delete/' . $account['id']); ?>">
                                                    <i class="fa fa-trash"></i> <span>Hapus</span>
                                                </a>
                                            </td>-->
                                            <td>
                                                <div class="container-fluid">
                                                    <div class="row" style="font-weight: 400">
                                                        <p>
                                                            <strong><?php echo $account['username'];?></strong>
                                                            <?php if (!empty($account['fullname'])) echo '(' . $account['fullname'] . ')';?>
                                                        </p>
                                                        <p>
                                                            <?php echo $this->Number->format($account['followers'], ['locale' => 'id_ID']); ?>
                                                            followers
                                                        </p>
                                                        <p>
                                                            <?php
                                                            if ($account['file']) {
                                                                echo $this->Html->link('Download Daftar Followers (CSV)', ['controller' => 'Accountlists', 'action' => 'downloadcsv', $account['username']]);
                                                            }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="row pull-right">
                                                        <!--<a href="<?php echo $this->Url->build('/member/view/' . $account['Member']['id']); ?>">
                                                            <i class="fa fa-desktop"></i> <span>Lihat</span>
                                                        </a>
                                                        <a href="<?php echo $this->Url->build('/replicatinglists/edit/' . $r['id']); ?>">
                                                            <i class="fa fa-pencil"></i> <span>Ubah</span>
                                                        </a>
                                                        <a href="<?php echo $this->Url->build('/replicatinglists/delete/' . $r['id']); ?>">
                                                            <i class="fa fa-trash"></i> <span>Hapus</span>
                                                        </a>-->
                                                    </div>
                                                </div><!--/ .container-fluid -->
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php }?>
                        </div><!--/.col-sm-12 -->
                    </div><!--/.row table body -->
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>
<script>
$(document).ready(function() {
    $('#dataTable').DataTable();
});
</script>
