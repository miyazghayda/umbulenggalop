<?php echo $this->Html->css('bootstrap-tagsinput'); ?>
<style>
label {
    font-weight: 400;
}
.bootstrap-tagsinput {
    width: 100%;
}
.bootstrap-tagsinput input {
    width: 100%;
}
</style>

<section class="content-header">
    <h1>Tambah Akun dengan Follower Target</h1>
    <ol class="breadcrumb">
        <li>
            Post
        </li>
        <li class="active">
            Tambah
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <?php echo $this->Html->link('Daftar Akun dengan Follower Target', ['controller' => 'Accountlists', 'action' => 'idol']); ?>
                </div><!--/.box-header -->
                <div class="box-body">
                    <div class="dt-bootstrap">
                    <?php echo $this->Form->create('Cargos.add', ['id' => 'addForm', 'data-toggle' => 'validator']); ?>

                        <div class="form-group">
                            <label>Akun Anda</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                <?php echo $this->Form->select('account_id', $accounts, ['class' => 'form-control', 'id' => 'accountId', 'required' => 'true']); ?>
                            </div><!--/.input-group -->
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->
                        
                        <div class="form-group">
                            <label>Akun dengan Follower Target, Pastikan Akun tersebut bukan Private atau Akun Anda telah memFollow Akun tersebut</label>
                            <?php echo $this->Form->text('member', ['placeholder' => 'Username Akun IG, diisi tanpa @, mis. miyazghayda', 'class' => 'form-control', 'id' => 'member', 'required' => 'true', 'data-required-error' => 'Harus diisi']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
                            <?php echo $this->Form->hidden('username', ['id' => 'username', 'required' => 'true', 'data-required-error' => 'Tentukan Member IG yang Valid']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->
                        
                        <div class="form-group">
                            <?php echo $this->Form->hidden('fullname', ['id' => 'fullname', 'required' => 'true', 'data-required-error' => 'Tentukan Member IG yang Valid']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
                            <?php echo $this->Form->hidden('description', ['id' => 'description', 'required' => 'true', 'data-required-error' => 'Tentukan Member IG yang Valid']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
                            <?php echo $this->Form->hidden('followers', ['id' => 'followers', 'required' => 'true', 'data-required-error' => 'Tentukan Member IG yang Valid']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
                            <?php echo $this->Form->hidden('followings', ['id' => 'followings', 'required' => 'true', 'data-required-error' => 'Tentukan Member IG yang Valid']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
                            <?php echo $this->Form->hidden('contents', ['id' => 'contents', 'required' => 'true', 'data-required-error' => 'Tentukan Member IG yang Valid']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->
                        
                        <div class="form-group">
                            <?php echo $this->Form->hidden('pk', ['id' => 'pk', 'required' => 'true', 'data-required-error' => 'Tentukan Member IG yang Valid']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->
                        
                        <div class="form-group">
                            <?php echo $this->Form->hidden('profpicurl', ['id' => 'profpicurl', 'required' => 'true', 'data-required-error' => 'Tentukan Member IG yang Valid']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->
                        
                        <div class="form-group">
                            <?php echo $this->Form->hidden('closed', ['id' => 'closed', 'required' => 'true', 'data-required-error' => 'Tentukan Member IG yang Valid']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->
                                                
                        <div class="form-group">
                            <label>Gunakan Filter <a href="/documentation/faq#filteridolfollower" target="_blank"><i class=" fa fa-question-circle"></i></a></label>
                            <br/>
                            <?php echo $this->Form->checkbox('filtercheckbox', ['hiddenField' => false, 'id' => 'filtercheckbox']); ?>
                            <?php echo $this->form->hidden('filter', ['id' => 'filter']); ?>
                        </div><!--/. form-group -->
                        
                        <div class="form-group follow-filter">
                            <label>Kata dalam Nama yang <strong>Dihindari</strong>, tanpa spasi, misal <strong>Jual</strong>, pisahkan dengan koma, dapat dikosongkan.</label>
                            <?php echo $this->Form->text('fullnameblacklist', ['placeholder' => 'Kata dalam Nama', 'id' => 'fullnameblacklist', 'class' => 'form-control', 'required' => 'false', 'data-required-error' => 'Harus diisi', 'data-role' => 'tagsinput']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group follow-filter">
                            <label>Kata dalam Deskripsi Akun (Biografi) yang <strong>Dihindari</strong>, tanpa spasi, misal <strong>Jual</strong>, pisahkan dengan koma, dapat dikosongkan.</label>
                            <?php echo $this->Form->text('biographyblacklist', ['placeholder' => 'Kata dalam Biografi', 'id' => 'biographyblacklist', 'class' => 'form-control', 'required' => 'false', 'data-required-error' => 'Harus diisi', 'data-role' => 'tagsinput']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group follow-filter">
                            <label>Akun harus aktif (mengirim foto/video) dalam ... bulan terakhir, hanya angka, misal <strong>1</strong>, dapat diisi <strong>0</strong>.</label>
                            <?php echo $this->Form->text('nmonths', ['placeholder' => 'Hanya Angka', 'id' => 'nmonths', 'class' => 'form-control', 'required' => 'false', 'data-required-error' => 'Harus diisi', 'type' => 'number', 'value' => '0']); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->
                        
                        <div class="form-group follow-filter">
                            <label>Akun dalam keadaan</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-unlock"></i></span>
                                <?php
                                $accounttypeids = [
                                    2 => 'Publik',
                                    3 => 'Private',
                                    1 => 'Keduanya'
                                ];
                                ?>
                                <?php echo $this->Form->select('accounttypeid', $accounttypeids, ['class' => 'form-control', 'id' => 'accounttypeid', 'required' => 'false']); ?>
                            </div><!--/.input-group -->
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
<?php echo $this->Form->submit('Tambah', ['class' => 'btn btn-primary btn-block']); ?>
                        </div><!--/. form-group -->
                    </div><!--/.dt-bootstrap -->
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>

<div id="modalWarning" class="modal fade" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hiddent="True">x</span></button>
                <h4 class="modal-title">Perhatian</h4>
            </div><!--/.modal-header -->
            <div class="modal-body">
                <p id="warningText">Tentukan username yang terdapat dalam IG</p>
            </div><!--/.modal-body -->
            <div class="modal-footer">
                <button class="btn btn-outline pull-right" type="button" data-dismiss="modal">Tutup</button>
            </div><!--/.modal-footer -->
        </div><!--/.modal-content -->
    </div><!--/.modal-dialog -->
</div><!--/.modalWarning -->

<?php echo $this->Html->script('bootstrap-tagsinput.min'); ?>
<?php echo $this->Html->script('bootstrap-checkbox.min'); ?>

<script>
$(function() {
    $('.follow-filter').hide();

    $('#filtercheckbox').checkboxpicker({
        offLabel: 'Tidak',
        onLabel: 'Ya'
    }).on('change', function() {
        if($(this).prop('checked')) {
            $('#filter').val(1);
            $('.follow-filter').show(1000);
            /*$('#fullnameblacklist').attr('required', 'true');
            $('#biographyblacklist').attr('required', 'true');
            $('#nmonths').attr('required', 'true');*/
            $('#accounttypeid').attr('required', 'true');
        } else {
            $('#filter').val(0);
            $('.follow-filter').hide(1000);
            /*$('#fullnameblacklist').removeAttr('required');
            $('#biographyblacklist').removeAttr('required');
            $('#nmonths').removeAttr('required');*/
            $('#accounttypeid').removeAttr('required');
        }
    });

    // To remove comma after label
    $('#fullnameblacklist').on('itemAdded', function(event) {
        let $field = $(this).siblings('.bootstrap-tagsinput').find('input');
        setTimeout(function() {
            $field.val('');
        });
    });
    $('#biographyblacklist').on('itemAdded', function(event) {
        let $field = $(this).siblings('.bootstrap-tagsinput').find('input');
        setTimeout(function() {
            $field.val('');
        });
    });


    function sleep(milliseconds) {
        let start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
                break;
            }
        }
    }

    $('#modalWarning').on('show.bs.modal', function() {
        $(this).removeClass('modal-info');
    });

    function removeModalClass(option) {
        let modal = $('#modalWarning');
        if (option === 1) {// info
            if (modal.hasClass('modal-warning')) modal.removeClass('modal-warning');
            if (modal.hasClass('modal-success')) modal.removeClass('modal-success');
            modal.addClass('modal-info');
        } else if (option === 2) {// warning
            if (modal.hasClass('modal-success')) modal.removeClass('modal-success');
            if (modal.hasClass('modal-info')) modal.removeClass('modal-info');
            modal.addClass('modal-warning');
        } else if (option === 3) {// success
            if (modal.hasClass('modal-info')) modal.removeClass('modal-info');
            if (modal.hasClass('modal-warning')) modal.removeClass('modal-warning');
            modal.addClass('modal-success');
        }
    }

    $('#member').focusout(function() {
        if ($(this).val() !== '') {
            $.ajax({
                url: '/accountlists/checkidol/' + $(this).val(),
                beforeSend: function () {
                    let t = 'Memeriksa Akun ' + $('#member').val() + ', silahkan menunggu...';
                    removeModalClass(1);
                    $('#warningText').text(t);
                    $('#modalWarning').modal();
                }
            }).done(function (data) {
                let resp = JSON.parse(data);
                if (resp.hasOwnProperty('error') && resp.error === 404) {
                    removeModalClass(2);
                    $('#warningText').text('Username tidak teridentifikasi, silahkan masukkan username yang valid');
                }

                if (resp.hasOwnProperty('pk')) {
                    removeModalClass(3);
                    let f = new Intl.NumberFormat('id-ID').format(resp.followers)
                    let t = 'Akun ' + resp.username + ' dengan ' + f + ' follower(s) valid.';
                    $('#warningText').text(t);

                    $('#username').val(resp.username);
                    $('#fullname').val(resp.fullname);
                    $('#description').val(resp.description);
                    $('#pk').val(resp.pk);
                    $('#followers').val(resp.followers);
                    $('#contents').val(resp.contents);
                    $('#followings').val(resp.following);
                    $('#closed').val(resp.closed);
                    $('#profpicurl').val(resp.profpicurl);
                }
            });
        }
    });
    
    $('#addForm').submit(function() {
        if ($('#username').val() === '') {
            removeModalClass(2);
            $('#warningText').text('Tentukan username yang terdapat dalam IG');

            return false;
        }
        if ($('#filter').val() === 1) {
            if ($('#fullnameblacklist').val() === '' && $('#biographyblacklist').val() === '' && $('#nmonths').val() === '' && $('#accounttypeid').val() === '') {
                return false;
            }
        }
    });
});
</script>
