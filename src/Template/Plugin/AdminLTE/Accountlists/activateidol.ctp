<section class="content-header">
    <h1>Aktifkan Follower(s) Akun Lain</h1>
    <ol class="breadcrumb">
        <li>
            Auto Follow 
        </li>
        <li class="active">
           Follow Follower(s) Akun Lain 
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <?php //echo $this->Html->link('Template Komentar', ['controller' => 'Compositions', 'action' => 'index']); ?>
                </div><!--/.box-header -->
                <div class="box-body">
                    <div class="dt-bootstrap">
                    <?php echo $this->Form->create('Preferences.add', ['id' => 'addForm', 'data-toggle' => 'validator']); ?>
                        <div class="form-group">
                            <label>Aktifkan <a href="/documentation/faq#filteridolfollower" target="_blank"><i class=" fa fa-question-circle"></i></a></label>
                            <br/>
                            <?php echo $this->Form->checkbox('filtercheckbox', ['hiddenField' => false, 'id' => 'filtercheckbox']);?>
                            <?php echo $this->form->hidden('followidolfollower', ['id' => 'followidolfollower', 'value' => $preference['followidolfollower']]); ?>
                        </div><!--/. form-group -->
                        
                        <div class="further-info">
                            <p style="font-weight: 400;">
                            Daftar Akun yang Followernya akan di Follow ada di <a href="/accountlists/idol">sini</a>.
                            </p>
                        </div><!--/.further-info -->

                        <div class="form-group">
<?php echo $this->Form->submit('Simpan', ['class' => 'btn btn-primary btn-block']); ?>
                        </div><!--/. form-group -->

                    </div><!--/.dt-bootstrap -->
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>

<?php echo $this->Html->script('bootstrap-checkbox.min'); ?>

<script>
$(function() {
    $('.further-info').hide();

    $('#filtercheckbox').checkboxpicker({
        offLabel: 'Tidak',
        onLabel: 'Ya'
    }).on('change', function() {
        if($(this).prop('checked')) {
            $('#followidolfollower').val(1);
            $('.further-info').show(500);
        } else {
            $('#followidolfollower').val(0);
            $('.further-info').hide(500);
        }
    });

<?php if ($preference['followidolfollower'] == true) {?>
    $('#filtercheckbox').prop('checked', true);
<?php }?>
});
</script>