<?php echo $this->Html->css('jquery.bsPhotoGallery'); ?>
<style>
ul {
    padding: 0 0 0 0;
    margin: 0 0 0 0;
}

ul li {
    list-style: none;
    margin-bottom: 25px;
}

ul li img {
    cursor: pointer;
}
</style>
<div class="clearfix"></div>
<section class="content-header">
    <h1>Konten Populer</h1>
    <ol class="breadcrumb">
        <li class="active">Riset</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <?php //echo $this->Html->link('Tambah Konten', ['controller' => 'Cargos', 'action' => 'add']); ?>
                    <div class="col-xs-4 pull-right">
                    </div><!--/.col-xs-4 -->
                </div><!--/.box-header -->
                <div class="box-body">
                    <div class="dt-bootstrap">
                        <form class="form-inline" autocomplete="off">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="hashtag" placeholder="Masukkan Hashtag, Tanpa #">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" id="btnSearch">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span><!--/.input-group-btn -->
                                </div>
                            </div><!--/.form-group -->
                        </form>
                    </div><!--/.dt-bootstrap -->

                    <div class="dt-bootstrap" id="noHashtag">
                        <p style="font-weight: 400">Masukkan Hashtag terlebih dahulu</p>
                    </div><!--/.dt-bootstrap -->

                    <div class="dt-bootstrap">
                        <ul class="first"></ul>
                    </div><!--/.dt-bootstrap -->

                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>

<div id="modalWarning" class="modal modal-info fade" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hiddent="True">x</span></button>
                <h4 class="modal-title">Perhatian</h4>
            </div><!--/.modal-header -->
            <div class="modal-body">
                <p id="warningText">Proses ini akan memakan waktu sekitar 0-3 menit, silahkan menunggu.</p>
            </div><!--/.modal-body -->
            <div class="modal-footer">
                <button class="btn btn-outline pull-right" type="button" data-dismiss="modal">Tutup</button>
            </div><!--/.modal-footer -->
        </div><!--/.modal-content -->
    </div><!--/.modal-dialog -->
</div><!--/.modalWarning -->

<?php echo $this->Html->script('jquery.bsPhotoGallery.hashtagSeePosts'); ?>
<script>
$(function() {

    function sleep(seconds) {
        let milliseconds = seconds * 1000;
        let start = new Date().getTime();
        for (let i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
                break;
            }
        }
    }

    // Get Hashtag, if any
    let currentUrl = window.location.href;
    let splitCurrentUrl = currentUrl.split('hashtags/seeposts/');
    if (typeof splitCurrentUrl[1] != 'undefined') {
        $('#hashtag').val(splitCurrentUrl[1]);
        sleep(3);
        search();
    }

    function search() {
        let hashtag = $('#hashtag');
        if (hashtag.val() !== '') {
            $.ajax({
                url: '/hashtags/seepostsdig/' + hashtag.val(),
                beforeSend: function () {
                    $('#modalWarning').modal();
                }
            }).done(function (data) {
                if (data.related.length > 0) {
                    console.log(data);
                    $('#noHashtag').empty();
                    let htmTag = '<div class="container-fluid" style="word-wrap: break-word"><p>Related:&nbsp;';
                    data.related.forEach(function(elem) {
                        htmTag = htmTag + '<a href="/hashtags/seeposts/' + elem + '">' + elem + '</a>&nbsp;';
                    });
                    htmTag = htmTag + '</p></div>';
                    $('#noHashtag').append(htmTag);

                    $('.first').empty();
                    let ids = [];
                    data.posts.forEach(function(elem) {
                        let htm = '<li id="' + elem.id + '">';
                        htm = htm + '<p class="text" style="display: none;"><strong>' + elem.likespretty + '&nbsp;likes</strong><br />';
                        htm = htm + '<a href="https://www.instagram.com/' + elem.username + '" target="_blank">' + elem.username + '</a>&nbsp;' + elem.caption + '<br />';
                        htm = htm + '<strong>' + elem.takenatpretty + '</strong>';
                        htm = htm + '</p>';
                        htm = htm + '<img src="' + elem.wads.url + '" alt="' + elem.username + '">';
                        htm = htm + '</li>';
                        $('.first').append(htm);
                    });

                    $('ul.first').bsPhotoGallery({
                        'classes': 'col-lg-2 col-md-4 col-sm-3 col-xs-4 col-xxs-12',
                        'hasModal': true
                    });

                    $('li.bspHasModal').on('click', function(){
                        let liId = $(this).attr('id');
                        $('#bsp-repost').attr('href', '/hashtags/repostpopular/' + liId);
                        $('#bsp-view').attr('href', '/hashtags/viewpopular/' + liId);
                    });

                    $('#modalWarning').modal('hide');
                }
            });
        }
    }

    $('#hashtag').keypress(function (e) {
        if (e.which == 13) {
            search();
            return false; 
        }
    });

    $('#btnSearch').click(function () {
        search();
    });

    /*$('ul.first').bsPhotoGallery({
        'classes': 'col-lg-2 col-md-4 col-sm-3 col-xs-4 col-xxs-12',
        'hasModal': true
    });*/

    $('li.bspHasModal').on('click', function(){
        let liId = $(this).attr('id');
        $('#bsp-view').attr('href', '/cargos/view/' + liId);
    });
});
</script>
