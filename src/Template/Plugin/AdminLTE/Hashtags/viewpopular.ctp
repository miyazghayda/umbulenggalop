<section class="content-header">
    <h1>Lihat Konten</h1>
    <ol class="breadcrumb">
        <li>
            Riset
        </li>
        <li class="active">
            Konten pada Hashtag
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <?php echo $this->Html->link('Konten Populer', ['controller' => 'Hashtags', 'action' => 'seeposts']); ?>
                </div><!--/.box-header -->
                <div class="box-body">

                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
<?php
$i = 0;
foreach($replica['wads'] as $w) {
?>
                            <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i==0) echo 'active';?>"></li>
<?php
    $i++;
}
?>
                        </ol><!--/.carousel-indicators -->

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
<?php
$i = 0;
foreach($replica['wads'] as $w) {
?>
                            <div class="item <?php if($i==0) echo 'active'; ?>">
                                <img src="<?php echo $w['url']; ?>" alt="<?php echo $replica['caption']; ?>">
                                <div class="carousel-caption"><?php echo $replica['caption']; ?></div>
                            </div><!--/.item -->
<?php
    $i++;
}
?>
                        </div><!--/.carousel-inner -->

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div><!--/.carousel-example-generic -->

                    <div class="dt-bootstrap">
                        <blockquote>
                            <p style="font-weight: 400;"><?php echo $replica['caption']; ?></p>
                            <?php if ($replica['location_id'] > 1) {?>
                            <small style="font-weight: 400;">
                                <span><i class="fa fa-map-marker"></i></span>
                                <?php echo $replica['location']['name']; ?>
                            </small>
                            <?php }?>
                            <small style="font-weight: 400;">
                                <span><i class="fa fa-clock-o"></i></span>
                                <span id="schedule" style="font-weight: 700;"><?php echo $this->Time->format($replica['takenat'], 'dd-MM-yyyy HH:mm'); ?></span>
                            </small>
                            <small style="font-weight: 400;">
                                <span><i class="fa fa-user"></i></span>
                                <a href="https://www.instagram.com/<?php echo $replica['member']['username']; ?>" target="_blank"><?php echo $replica['member']['username']; ?></a>
                            </small>
                        </blockquote>
                    </div><!--/.dt-bootstrap -->
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>
<script type="text/javascript">
$(document).ready(function() {
    console.log($('#schedule').text());
    $('#schedule').text(moment($('#schedule').text(), 'DD-MM-YYYY HH:mm').format("dddd, DD MMMM YYYY HH:mm"));
});
</script>