<?php echo $this->Html->css('bootstrap-datetimepicker.min'); ?>

<section class="content-header">
    <h1>Repost Konten Populer</h1>
    <ol class="breadcrumb">
        <li>
            Konten
        </li>
        <li class="active">
            Ubah
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <?php echo $this->Html->link('Konten Populer', ['controller' => 'Hashtags', 'action' => 'seeposts']); ?>
                </div><!--/.box-header -->
                <div class="box-body">

                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
<?php
$i = 0;
foreach($replica['wads'] as $datum) {
?>
                            <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i==0) echo 'active';?>"></li>
<?php
    $i++;
}
?>
                        </ol><!--/.carousel-indicators -->

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
<?php
$i = 0;
foreach($replica['wads'] as $datum) {
?>
                            <div class="item <?php if($i==0) echo 'active'; ?>">
                                <img src="<?php echo $datum['url']; ?>" alt="<?php echo $replica['caption']; ?>">
                                <div class="carousel-caption"><?php echo $replica['caption']; ?></div>
                            </div><!--/.item -->
<?php
    $i++;
}
?>
                        </div><!--/.carousel-inner -->

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div><!--/.carousel-example-generic -->

                    <div class="dt-bootstrap" style="margin-top: 10px;">

                    <?php echo $this->Form->create('Hashtags.repostpopular', ['id' => 'addForm', 'data-toggle' => 'validator']); ?>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <select id="locations" name="location_id" class="form-control">
                                    <?php
                                    if ($replica['location_id'] > 1) {
                                        echo '<option value="' . $replica['location_id'] . '" selected="selected">' . $replica['location']['name'] . '</option>';
                                        echo '<option value="1">Tanpa Lokasi</option>';
                                    } else {
                                        echo '<option value="1" selected="selected">Tanpa Lokasi</option>';
                                    }
                                    ?>
                                </select>
                            </div><!--/.input-group -->
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                <?php echo $this->Form->text('scheduleShow', ['class' => 'form-control', 'id' => 'scheduleShow', 'required' => 'false', 'placeholder' => 'Jadwal upload', 'required' => 'required', 'data-required-error' => 'Harus diisi', 'value' => $this->Time->format(date('Y-m-d H:i:s'), 'yyyy-MM-dd HH:mm')]); ?>
                                <?php echo $this->Form->hidden('schedule', ['id' => 'schedule', 'value' => $this->Time->format(date('Y-m-d H:i:s'), 'yyyy-MM-dd HH:mm')]);?>
                            </div><!--/.input-group -->
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
                            <?php
                            $caption = 'from @' . $replica['member']['username'] . ' ' . $replica['caption'];
                            ?>
                            <?php echo $this->Form->textarea('caption', ['placeholder' => 'Caption, dapat dikosongkan', 'class' => 'form-control', 'id' => 'caption', 'required' => 'false', 'value' => $caption]); ?>
                            <span class="help-block with-errors"></span>
                        </div><!--/. form-group -->

                        <div class="form-group">
<?php echo $this->Form->submit('Simpan', ['class' => 'btn btn-primary btn-block']); ?>
                        </div><!--/. form-group -->

                    </div><!--/.dt-bootstrap -->
                </div><!--/.box-body -->
            </div><!--/.box -->
        </div><!--/.col-xs-12 -->
    </div><!--/.row -->
</section>

<?php
echo $this->Html->script('bootstrap-datetimepicker.min');

echo $this->Html->css('select2.min');
echo $this->Html->script('select2.min');
?>
<script>
$(function() {
let schedule = $('#scheduleShow').datetimepicker({
    locale: 'id',
    sideBySide: true,
    format: 'YYYY-MM-DD HH:mm'
});

schedule.on('dp.change', function(e) {
    let scheduleTime = $('#scheduleShow').val() + ':00';
    if (scheduleTime == ':00') scheduleTime = '';
    $('#schedule').val(scheduleTime);
});

$('#locations').select2({
    placeholder: 'Ketik Nama Lokasi',
    ajax: {
        delay: 1000,
        minimumInputhLength: 3,
        url: '/locations/search/' + $('#accountId').val(),
        dataType: 'json',
    }
});
});
</script>
