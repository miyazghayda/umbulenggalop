<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Preferences Controller
 *
 * @property \App\Model\Table\PreferencesTable $Preferences
 *
 * @method \App\Model\Entity\Preference[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PreferencesController extends AppController
{
    public $user;
    public $session;
    public $accountOnSession;
    public $paidAccounts;
    public $orAccounts;
    public $paidAccountIds;

    public function initialize() {
        parent::initialize();
        $this->Auth->allow(['checkidol']);

        $this->user = $this->Auth->user();
        $this->paidAccounts = $this->Preferences->Accounts->find()
             ->where(['user_id' => $this->user['id'], 'active' => 1, 'statusid' => 5])
             ->all();

        // Only show paid (5) account(s)
        $orAccounts = [];
        $paidAccountIds = [];

        foreach ($this->paidAccounts as $account) {
            array_push($orAccounts, ['account_id' => $account['id']]);
            //$orAccounts = array_merge($orAccounts, ['account_id' => $account['id']]);
            array_push($paidAccountIds, $account['id']);
        }
        $this->orAccounts = $orAccounts;
        $this->paidAccountIds = $paidAccountIds;

        $this->loadComponent('RequestHandler');
    }

    public function isAuthorized($user) {
        $action = $this->request->getParam('action');

        // All actions require an id
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }

        // Check that the cargo belongs to the current user.
        $data = $this->Preferences->findById($id)->first();

        if (in_array($data->account_id, $this->paidAccountIds)) {
            return true;
        } else {
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->session = $this->request->getSession();
        $session = $this->request->getSession();
        $this->accountOnSession = $session->read('Config.account');
    }

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $this->set('user', $this->user);
    }

    public function likecommentnewfollower() {
        $preference = $this->Preferences->find()
            ->where(['account_id' => $this->accountOnSession])
            ->first();

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if ($data['likebynewfollowing'] == false) {
                $data['commentbynewfollowing'] = false;
                $data['newfollowerposttolike'] = 0;
                $data['newfollowerposttocomment'] = 0;
            } else {
                if ($data['newfollowerposttocomment'] == 0) {
                    $data['commentbynewfollowing'] = false;
                } else {
                    $data['commentbynewfollowing'] = true;
                }
            }
            $preference = $this->Preferences->patchEntity($preference, $data);
            if ($this->Preferences->save($preference)) {
                $this->Flash->success(__('Berhasil menyimpan'));
            }
        }

        $this->set(compact('preference'));
    }

    public function dmnewfollower() {
        $preference = $this->Preferences->find()
            ->where(['account_id' => $this->accountOnSession])
            ->first();

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if ($data['dmbynewfollowing'] == false) {
                $data['newfollowertodm'] = 0;
            } else {
                if ($data['newfollowertodm'] == 0) {
                    $data['dmbynewfollowing'] = false;
                } else {
                    $data['newfollowertodm'] = (int)$data['newfollowertodm'];
                }
            }
            $preference = $this->Preferences->patchEntity($preference, $data);
            if ($this->Preferences->save($preference)) {
                $this->Flash->success(__('Berhasil menyimpan'));
            }
        }

        $this->set(compact('preference'));
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Accounts']
        ];
        $preferences = $this->paginate($this->Preferences);

        $this->set(compact('preferences'));
    }

    /**
     * View method
     *
     * @param string|null $id Preference id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $preference = $this->Preferences->get($id, [
            'contain' => ['Accounts']
        ]);

        $this->set('preference', $preference);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $preference = $this->Preferences->newEntity();
        if ($this->request->is('post')) {
            $preference = $this->Preferences->patchEntity($preference, $this->request->getData());
            if ($this->Preferences->save($preference)) {
                $this->Flash->success(__('The preference has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The preference could not be saved. Please, try again.'));
        }
        $accounts = $this->Preferences->Accounts->find('list', ['limit' => 200]);
        $this->set(compact('preference', 'accounts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Preference id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $preference = $this->Preferences->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $preference = $this->Preferences->patchEntity($preference, $this->request->getData());
            if ($this->Preferences->save($preference)) {
                $this->Flash->success(__('The preference has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The preference could not be saved. Please, try again.'));
        }
        $accounts = $this->Preferences->Accounts->find('list', ['limit' => 200]);
        $this->set(compact('preference', 'accounts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Preference id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $preference = $this->Preferences->get($id);
        if ($this->Preferences->delete($preference)) {
            $this->Flash->success(__('The preference has been deleted.'));
        } else {
            $this->Flash->error(__('The preference could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
