<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\I18n\Time;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use InstagramScraper\Instagram;
use InstagramScraper\Exception\InstagramException;
use InstagramScraper\Exception\InstagramNotFoundException;

/**
 * Accountlists Controller
 *
 * @property \App\Model\Table\AccountlistsTable $Accountlists
 *
 * @method \App\Model\Entity\Accountlist[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AccountlistsController extends AppController
{
    public $user;
    public $session;
    public $accountOnSession;
    public $paidAccounts;
    public $orAccounts;
    public $paidAccountIds;

    public function initialize() {
        parent::initialize();
        $this->Auth->allow(['checkidol']);

        $this->user = $this->Auth->user();
        $this->paidAccounts = $this->Accountlists->Accounts->find()
             ->where(['user_id' => $this->user['id'], 'active' => 1, 'statusid' => 5])
             ->all();

        // Only show paid (5) account(s)
        $orAccounts = [];
        $paidAccountIds = [];

        foreach ($this->paidAccounts as $account) {
            array_push($orAccounts, ['account_id' => $account['id']]);
            //$orAccounts = array_merge($orAccounts, ['account_id' => $account['id']]);
            array_push($paidAccountIds, $account['id']);
        }
        $this->orAccounts = $orAccounts;
        $this->paidAccountIds = $paidAccountIds;

        $this->loadComponent('RequestHandler');
    }

    public function isAuthorized($user) {
        $action = $this->request->getParam('action');

        // All actions require an id
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }

        // Check that the cargo belongs to the current user.
        $cargo = $this->Accountlists->findById($id)->first();

        if (in_array($cargo->account_id, $this->paidAccountIds)) {
            return true;
        } else {
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->session = $this->request->getSession();
        $session = $this->request->getSession();
        $this->accountOnSession = $session->read('Config.account');
    }

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $this->set('user', $this->user);
    }

    public function activateidol() {
        $preference = $this->Accountlists->Accounts->Preferences->find()
            ->where(['account_id' => $this->accountOnSession])
            ->first();

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if (empty($data['followidolfollower'])) {
                $data['followidolfollower'] = false;
            }            
            $preference = $this->Accountlists->Accounts->Preferences->patchEntity($preference, $data);
            if ($this->Accountlists->Accounts->Preferences->save($preference)) {
                $this->Flash->success(__('Berhasil menyimpan'));
            }
        }

        $this->set(compact('preference'));
    }
 
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Accounts', 'Members']
        ];
        $accountlists = $this->paginate($this->Accountlists);

        $this->set(compact('accountlists'));
    }

    /**
     * View method
     *
     * @param string|null $id Accountlist id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $accountlist = $this->Accountlists->get($id, [
            'contain' => ['Accounts', 'Members']
        ]);

        $this->set('accountlist', $accountlist);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $accountlist = $this->Accountlists->newEntity();
        if ($this->request->is('post')) {
            $accountlist = $this->Accountlists->patchEntity($accountlist, $this->request->getData());
            if ($this->Accountlists->save($accountlist)) {
                $this->Flash->success(__('The accountlist has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The accountlist could not be saved. Please, try again.'));
        }
        $accounts = $this->Accountlists->Accounts->find('list', ['limit' => 200]);
        $members = $this->Accountlists->Members->find('list', ['limit' => 200]);
        $this->set(compact('accountlist', 'accounts', 'members'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Accountlist id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $accountlist = $this->Accountlists->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $accountlist = $this->Accountlists->patchEntity($accountlist, $this->request->getData());
            if ($this->Accountlists->save($accountlist)) {
                $this->Flash->success(__('The accountlist has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The accountlist could not be saved. Please, try again.'));
        }
        $accounts = $this->Accountlists->Accounts->find('list', ['limit' => 200]);
        $members = $this->Accountlists->Members->find('list', ['limit' => 200]);
        $this->set(compact('accountlist', 'accounts', 'members'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Accountlist id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $accountlist = $this->Accountlists->get($id);
        if ($this->Accountlists->delete($accountlist)) {
            $this->Flash->success(__('The accountlist has been deleted.'));
        } else {
            $this->Flash->error(__('The accountlist could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function downloadcsv($username = null) { 
        if ($username !== null) {
            $filePath = WWW_ROOT . 'files' . DS . 'csv' . DS . 'celebrities' . DS . $username . '.csv';
            $this->response->withFile($filePath, [
                'download' => true,
                'name' => $username . '.csv'
            ]);
            return $this->response;
        }
    }

    public function idol($id = null)
    {
        if ($this->accountOnSession > 0) {
            $accounts = $this->Accountlists->find()
            ->where(['Accountlists.typeid' => 1, 'Accountlists.active' => true, 'Accountlists.account_id' => $this->accountOnSession])
            ->contain(['filters', 'members', 'accounts'])
            ->all();
        } else {
            $accounts = [];
        }
        //$accounts = [];
        $data = [];
        foreach ($accounts as $a) {
            //print_r()
            $file = new File(WWW_ROOT . 'files' . DS . 'csv' . DS . 'celebrities' . DS . $a['Members']['username'] . '.csv');
            $fileExists = false;
            if ($file->exists()) {
                $fileExists = true;
            }
            array_push($data, [
                'username' => $a['Members']['username'],
                'fullname' => $a['Members']['fullname'],
                'followers' => $a['Members']['followers'],
                'file' => $fileExists,
            ]);
        }
        $accounts = $data;
        $this->set(compact('accounts'));
    }
    public function idolold($id = null)
    {
        // If id specified
        $orAccounts = [];
        foreach ($this->orAccounts as $key => $value) {
            array_push($orAccounts, ['Accountlists.account_id' => $value['account_id']]);
        }

        if (count($orAccounts) > 0) {
            $accounts = $this->Accountlists->find()
            ->where(['Accountlists.typeid' => 1, 'Accountlists.active' => true, 'OR' => $orAccounts])
            ->contain(['filters', 'members', 'accounts'])
            ->all();
        } else {
            $accounts = [];
        }
        $this->set(compact('accounts'));
    }

    public function addidol($id = null) {
        $idol = $this->Accountlists->newEntity();
        if ($this->request->is('post') && !empty($this->request->getData()['pk'])) {
            $data = $this->request->getData();
            $data['account_id'] = $this->accountOnSession;
            $data['closed'] === 'false' ? $data['closed'] = false : $data['closed'] = true;
            // get member_id, if exists update
            if ($this->Accountlists->Members->exists(['pk' => $data['pk'], 'active' => true])) {
                $member = $this->Accountlists->Members->find()
                    ->where(['pk' => $data['pk'], 'active' => true])
                    ->first();
                $member->username = $data['username'];
                $member->fullname = $data['fullname'];
                $member->decription = $data['description'];
                $member->followers = $data['followers'];
                $member->followings = $data['followings'];
                $member->contents = $data['contents'];
                $member->profpicurl = $data['profpicurl'];
                $member->closed = $data['closed'];
                $member->profpicurlfixed = true;
            } else {
                $datum = [
                    'pk' => $data['pk'],
                    'username' => $data['username'],
                    'fullname' => $data['fullname'],
                    'description' => $data['description'],
                    'profpicurl' => $data['profpicurl'],
                    'followers' => $data['followers'],
                    'followings' => $data['followings'],
                    'contents' => $data['contents'],
                    'closed' => $data['closed'],
                    'profpicurlfixed' => true,
                    'active' => true,
                ];
                $member = $this->Accountlists->Members->newEntity();
                $member = $this->Accountlists->Members->patchEntity($member, $datum);
                //$res = $this->Accountlists->Members->save($member);
                //$member_id = $res->id;
            }
            $res = $this->Accountlists->Members->save($member);
            $member_id = $res->id;

            // Insert/Update Filter
            $filter_id = 1;
            if ((int)$data['filter'] === 1) {
                //echo $data['filter'];
                $data['fullnameblacklist'] = str_replace(' ', '', $data['fullnameblacklist']);
                $data['biographyblacklist'] = str_replace(' ', '', $data['biographyblacklist']);
                empty($data['nmonths']) ? $data['nmonths'] = 0 : $data['nmonths'] = (int)$data['nmonths'];

                $datum = [
                    'account_id' => $data['account_id'],
                    'typeid' => 2,
                    'fullnameblacklist' => $data['fullnameblacklist'],
                    'biographyblacklist' => $data['biographyblacklist'],
                    'nmonths' => $data['nmonths'],
                    'accounttypeid' => $data['accounttypeid'],
                    'active' => true,
                ];

                // If filter exist, use id, for other condition, create first
                if ($this->Accountlists->Filters->exists($datum)) {
                    $filter = $this->Accountlists->Filters->find()
                        ->where($datum)
                        ->first();
                    $filter_id = $filter->id;
                } else {
                    $filter = $this->Accountlists->Filters->newEntity();
                    $filter = $this->Accountlists->Filters->patchEntity($filter, $datum);
                    if ($this->Accountlists->Filters->save($filter)) $filter_id = $filter->id;
                }
            }
            // Insert Celebrity
            $celebrity_id = 1;
            $celebrity_allfollowersaved = false;
            if ($this->Accountlists->Celebrities->exists(['member_id' => $member_id, 'active' => true])) {
                $celebrity = $this->Accountlists->Celebrities->find()
                    ->where(['member_id' => $member_id, 'active' => true])
                    ->first();
                $celebrity_id = $celebrity->id;
                $celebrity_allfollowersaved = $celebrity->allfollowersaved;
            } else {
                $datum = [
                    'member_id' => $member_id,
                    'followers' => $data['followers'],
                    'followersaved' => 0,
                    'allfollowersaved' => false,
                    'nextmaxid' => null,
                    'active' => true
                ];
                $celebrity = $this->Accountlists->Celebrities->newEntity();
                $celebrity = $this->Accountlists->Celebrities->patchEntity($celebrity, $datum);
                if ($this->Accountlists->Celebrities->save($celebrity)) $celebrity_id = $celebrity->id;
            }
            // Insert Accountlist
            if ($this->Accountlists->exists(['account_id' => $data['account_id'], 'member_id' => $member_id, 'typeid' => 1, 'active' => true])) {
                $accountlist = $this->Accountlists->find()
                    ->where(['account_id' => $data['account_id'], 'member_id' => $member_id, 'typeid' => 1, 'active' => true])
                    ->first();
                $accountlist->filter_id = $filter_id;
                $accountlist->vassal_id = 1;
                $accountlist->celebrity_id = $celebrity_id;
                $res = $this->Accountlists->save($accountlist);
            } else {
                $datum = [
                    'account_id' => $data['account_id'],
                    'member_id' => $member_id,
                    'filter_id' => $filter_id,
                    'vassal_id' => 1,
                    'celebrity_id' => $celebrity_id,
                    'typeid' => 1,
                    'proffixed' => true,
                    'allfollowersaved' => $celebrity_allfollowersaved,
                    'nextmaxid' => null,
                    'active' => true
                ];
                $accountlist = $this->Accountlists->newEntity();
                $accountlist = $this->Accountlists->patchEntity($accountlist, $datum);
                $res = $this->Accountlists->save($accountlist);
            }

            // Update account preference
            $preference = $this->Accountlists->Accounts->Preferences->find()
                ->where(['account_id' => $data['account_id'], 'active' => true])
                ->first();
            $preference->followidolfollower = true;
            $this->Accountlists->Accounts->Preferences->save($preference);

            //$res = $this->Accountlists->Accounts->Preferences->save($preference);
            
            $this->Flash->success(__('Berhasil menyimpan data.'));

            return $this->redirect(['action' => 'idol']);
        }
        if ($this->accountOnSession > 0) {
            $this->set(compact('idol'));
        } else {
            $this->redirect(['action' => 'idol']);
        }
    }
    public function addidolold($id = null) {
        $idol = $this->Accountlists->newEntity();
        if ($this->request->is('post') && !empty($this->request->getData()['pk'])) {
            $data = $this->request->getData();
            $data['closed'] === 'false' ? $data['closed'] = false : $data['closed'] = true;
            // get member_id, if exists update
            if ($this->Accountlists->Members->exists(['pk' => $data['pk'], 'active' => true])) {
                $member = $this->Accountlists->Members->find()
                    ->where(['pk' => $data['pk'], 'active' => true])
                    ->first();
                $member->username = $data['username'];
                $member->fullname = $data['fullname'];
                $member->decription = $data['description'];
                $member->followers = $data['followers'];
                $member->followings = $data['followings'];
                $member->contents = $data['contents'];
                $member->profpicurl = $data['profpicurl'];
                $member->closed = $data['closed'];
                $member->profpicurlfixed = true;
            } else {
                $datum = [
                    'pk' => $data['pk'],
                    'username' => $data['username'],
                    'fullname' => $data['fullname'],
                    'description' => $data['description'],
                    'profpicurl' => $data['profpicurl'],
                    'followers' => $data['followers'],
                    'followings' => $data['followings'],
                    'contents' => $data['contents'],
                    'closed' => $data['closed'],
                    'profpicurlfixed' => true,
                    'active' => true,
                ];
                $member = $this->Accountlists->Members->newEntity();
                $member = $this->Accountlists->Members->patchEntity($member, $datum);
                //$res = $this->Accountlists->Members->save($member);
                //$member_id = $res->id;
            }
            $res = $this->Accountlists->Members->save($member);
            $member_id = $res->id;

            // Insert/Update Filter
            $filter_id = 1;
            if ((int)$data['filter'] === 1) {
                //echo $data['filter'];
                $data['fullnameblacklist'] = str_replace(' ', '', $data['fullnameblacklist']);
                $data['biographyblacklist'] = str_replace(' ', '', $data['biographyblacklist']);
                empty($data['nmonths']) ? $data['nmonths'] = 0 : $data['nmonths'] = (int)$data['nmonths'];

                $datum = [
                    'account_id' => $data['account_id'],
                    'typeid' => 2,
                    'fullnameblacklist' => $data['fullnameblacklist'],
                    'biographyblacklist' => $data['biographyblacklist'],
                    'nmonths' => $data['nmonths'],
                    'accounttypeid' => $data['accounttypeid'],
                    'active' => true,
                ];

                // If filter exist, use id, for other condition, create first
                if ($this->Accountlists->Filters->exists($datum)) {
                    $filter = $this->Accountlists->Filters->find()
                        ->where($datum)
                        ->first();
                    $filter_id = $filter->id;
                } else {
                    $filter = $this->Accountlists->Filters->newEntity();
                    $filter = $this->Accountlists->Filters->patchEntity($filter, $datum);
                    if ($this->Accountlists->Filters->save($filter)) $filter_id = $filter->id;
                }
            }
            // Insert Celebrity
            $celebrity_id = 1;
            $celebrity_allfollowersaved = false;
            if ($this->Accountlists->Celebrities->exists(['member_id' => $member_id, 'active' => true])) {
                $celebrity = $this->Accountlists->Celebrities->find()
                    ->where(['member_id' => $member_id, 'active' => true])
                    ->first();
                $celebrity_id = $celebrity->id;
                $celebrity_allfollowersaved = $celebrity->allfollowersaved;
            } else {
                $datum = [
                    'member_id' => $member_id,
                    'followers' => $data['followers'],
                    'followersaved' => 0,
                    'allfollowersaved' => false,
                    'nextmaxid' => null,
                    'active' => true
                ];
                $celebrity = $this->Accountlists->Celebrities->newEntity();
                $celebrity = $this->Accountlists->Celebrities->patchEntity($celebrity, $datum);
                if ($this->Accountlists->Celebrities->save($celebrity)) $celebrity_id = $celebrity->id;
            }
            // Insert Accountlist
            if ($this->Accountlists->exists(['account_id' => $data['account_id'], 'member_id' => $member_id, 'typeid' => 1, 'active' => true])) {
                $accountlist = $this->Accountlists->find()
                    ->where(['account_id' => $data['account_id'], 'member_id' => $member_id, 'typeid' => 1, 'active' => true])
                    ->first();
                $accountlist->filter_id = $filter_id;
                $accountlist->vassal_id = 1;
                $accountlist->celebrity_id = $celebrity_id;
                $res = $this->Accountlists->save($accountlist);
            } else {
                $datum = [
                    'account_id' => $data['account_id'],
                    'member_id' => $member_id,
                    'filter_id' => $filter_id,
                    'vassal_id' => 1,
                    'celebrity_id' => $celebrity_id,
                    'typeid' => 1,
                    'proffixed' => true,
                    'allfollowersaved' => $celebrity_allfollowersaved,
                    'nextmaxid' => null,
                    'active' => true
                ];
                $accountlist = $this->Accountlists->newEntity();
                $accountlist = $this->Accountlists->patchEntity($accountlist, $datum);
                $res = $this->Accountlists->save($accountlist);
            }

            // Update account preference
            $preference = $this->Accountlists->Accounts->Preferences->find()
                ->where(['account_id' => $data['account_id'], 'active' => true])
                ->first();
            $preference->followidolfollower = true;
            $this->Accountlists->Accounts->Preferences->save($preference);

            //$res = $this->Accountlists->Accounts->Preferences->save($preference);
            
            $this->Flash->success(__('Berhasil menyimpan data.'));

            return $this->redirect(['action' => 'idol']);
        }
        $orAccounts = [];
        foreach ($this->paidAccountIds as $id) array_push($orAccounts, ['id' => $id]);

        if (count($orAccounts) > 0) {
            $accounts = $this->Accountlists->Accounts->find('list', [
                'keyField' => 'id',
                'valueField' => 'username'
            ])
                             ->where(['OR' => $orAccounts])
                             ->order(['username' => 'ASC'])
                             ->toArray();

            $this->set(compact('idol', 'accounts'));
        } else {
            $this->redirect(['action' => 'idol']);
        }
    }


    public function checkidol($username = null) {
        $this->autoRender = false;
        //$data = [];
        $data['response'] = 'no';
        if (!empty($username) && $this->request->is('ajax')) {
            $ig = new Instagram;
            try {
                $account = $ig->getAccount($username);
                //echo $account->getFollowedByCount();
                echo json_encode([
                    'pk' => $account->getId(),
                    'username' => $account->getUsername(),
                    'fullname' => $account->getFullName(),
                    'description' => $account->getBiography(),
                    'followers' => $account->getFollowedByCount(),
                    'contents' => $account->getMediaCount(),
                    'following' => $account->getFollowsCount(),
                    'closed' => $account->isPrivate(),
                    'profpicurl' => $account->getProfilePicUrl()
                ]);
            } catch (InstagramNotFoundException $e) {
                echo json_encode([
                    'error' => 404 
                ]);
            }
        }
    }
}
