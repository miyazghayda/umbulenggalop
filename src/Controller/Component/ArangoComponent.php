<?php
namespace App\Controller\Component;


use Cake\Controller\Component;
use ArangoDBClient\Connection;
use ArangoDBClient\ConnectionOptions;
use ArangoDBClient\Collection;
use ArangoDBClient\CollectionHandler;
use ArangoDBClient\Document;
use ArangoDBClient\DocumentHandler;
use ArangoDBClient\UpdatePolicy;
use ArangoDBClient\Statement;

/**
 * 
 */
class ArangoComponent extends Component
{
    public $Conn;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $host = '206.189.46.73';
        $user = 'user';
        $password = 'jayapura';
        $database = 'automateit';

        if (array_key_exists('host', $config) && $config['host'] != null) $host = $config['host'];
        if (array_key_exists('user', $config) && $config['user'] != null) $user = $config['user'];
        if (array_key_exists('password', $config) && $config['password'] != null) $password = $config['password'];
        if (array_key_exists('database', $config) && $config['database'] != null) $database = $config['database'];

        $connectionOptions = [
            // server endpoint to connect to
            ConnectionOptions::OPTION_ENDPOINT => 'tcp://' . $host . ':8529',
            // authorization type to use (currently supported: 'Basic')
            ConnectionOptions::OPTION_AUTH_TYPE => 'Basic',
            // user for basic authorization
            ConnectionOptions::OPTION_AUTH_USER => $user,
            // password for basic authorization
            ConnectionOptions::OPTION_AUTH_PASSWD => $password,
            // connection persistence on server. can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
            ConnectionOptions::OPTION_CONNECTION => 'Close',
            // connect timeout in seconds
            ConnectionOptions::OPTION_TIMEOUT => 3,
            // whether or not to reconnect when a keep-alive connection has timed out on server
            ConnectionOptions::OPTION_RECONNECT => true,
            // optionally create new collections when inserting documents
            ConnectionOptions::OPTION_CREATE => true,
            // optionally create new collections when inserting documents
            ConnectionOptions::OPTION_UPDATE_POLICY => UpdatePolicy::LAST,
            ConnectionOptions::OPTION_DATABASE => $database,
        ];

        // open connection
        $this->Conn = new Connection($connectionOptions);
    }

    public function query($query = null, $output = null)
    {
        $data = null;
        if ($query != null) {
            $statement = new Statement(
                $this->Conn,
                [
                    'query' => $query,
                    'count' => true,
                    'batchSize' => 1,
                    'sanitize' => true
                ]
            );
            $cursor = $statement->execute();

            //$data = $cursor->getAll();
            if ($output = 'one_document') {
                if (count($cursor->getAll()) > 0) {
                    $data = $cursor->getAll()[0]->toJson();
                }
            }
        }
        return $data;
    }
}