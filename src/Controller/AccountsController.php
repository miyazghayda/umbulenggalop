<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Filesystem\File;
use Cake\I18n\Time;
use Cake\Event\Event;

/**
 * Accounts Controller
 *
 * @property \App\Model\Table\AccountsTable $Accounts
 *
 * @method \App\Model\Entity\Account[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AccountsController extends AppController
{
    public $user;
    public $session;
    public $accountOnSession;

    public function initialize()
    {
        parent::initialize();
        $this->user = $this->Auth->user();
        $this->Auth->allow(['tohandleworkers', 'testarango']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->session = $this->request->getSession();
        $session = $this->request->getSession();
        $this->accountOnSession = $session->read('Config.account');
    }

    public function isAuthorizedNew($user)
    {
        $action = $this->request->getParam('action');

        // All actions require an id
        //$id = $this->request->getParam('pass.0');
        //$session = $this->request->session();
        //$id = $this->session->read('Config.account');
        $id = $this->accountOnSession;
        if (!$id) {
            return false;
        }

        // Check that the account belongs to the current user.
        $account = $this->Accounts->findById($id)->first();

        return $account->user_id === $user['id'];
    }

    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');

        // All actions require an id
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }

        // Check that the account belongs to the current user.
        $account = $this->Accounts->findById($id)->first();

        return $account->user_id === $user['id'];
    }

    // Pick an account
    public function pick()
    {
        $this->viewBuilder()->setLayout('pick');
        $data = $this->Accounts->find()
            ->select(['id', 'username', 'messy'])
            ->where(['user_id' => $this->user['id'], 'statusid' => 5, 'active' => true]);

        if ($data->count() == 0) {
            return $this->redirect(['action' => 'addnew']);
        } elseif ($data->count() == 1) {
            $data = $data->first();
            return $this->redirect(['action' => 'picked', $data['messy']]);
        } else {
            $data = $data->order(['username' => 'ASC'])->all();
            $this->set(compact(['data']));
        }
    }

    public function picked($messy = null)
    {
        if (!empty($messy)) {
            $data = $this->Accounts->find()
                ->select(['id', 'username'])
                ->where(['messy' => $messy, 'user_id' => $this->user['id'], 'statusid' => 5, 'active' => true]);
            $dataCount = $data->count();
            if ($dataCount == 0) {
                return $this->redirect('pick');
            } elseif ($dataCount == 1) {
                $data = $data->first();
                //$session = $this->request->session();
                $this->session->write('Config.account', $data->id);
                $this->session->write('Config.accountUsername', $data->username);
                return $this->redirect(['action' => '/']);
            }
        } else {
            return $this->redirect(['action' => 'pick']);
        }
    }

    /**
     * Add new method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addnew()
    {
        $this->viewBuilder()->setLayout('addaccount');
        $account = $this->Accounts->newentity();
        if ($this->request->is('post')) {
            $this->request->getData()['username'] = str_replace('@', '', $this->request->getData()['username']);
            $account = $this->Accounts->find()
                ->select(['id'])
                ->where(['username' => trim($this->request->getData()['username']), 'statusid' => 1, 'active' => false]);
            if ($account->count() > 0) {
                $a = $account->first();
                $query = $this->Accounts->query();
                $query->update()
                    ->set([
                        'active' => true,
                        'statusid' => 5,
                        'note' => 'Login berhasil, akun dapat digunakan',
                        'proxy_id' => $this->request->getData()['proxy_id']
                    ])
                    ->where([
                        'username' => trim($this->request->getData()['username']),
                        'statusid' => 1,
                        'active' => false
                    ])
                    ->execute();
                
                // Add Table preferences row related to this account
                $preference = $this->Accounts->Preferences->newEntity();

                $dataPreference = [
                    'account_id' => $a->id,
                    'maxlikeperday' => 500,
                    'maxcommentperday' => 500,
                    'maxfollowperday' => 300,
                    'maxpostperday' => 1,
                    'followidolfollower' => 0,
                    'followbylocation' => 0,
                    'followbyhashtag' => 0,
                    'likefeed' => 0,
                    'likebylocation' => 0,
                    'likebyhashtag' => 0,
                    'commentfeed' => 0,
                    'commentbyhashtag' => 0,
                    'commentbylocation' => 0,
                    'followtoday' => 0,
                    'liketoday' => 0,
                    'commenttoday' => 0,
                    'posttoday' => 0,
                    'hashtagtofollowtoday' => 0,
                    'gethashtagtofollowtoday' => true,
                    'active' => true,
                ];
                $preference = $this->Accounts->Preferences->patchEntity($preference, $dataPreference);
                if ($this->Accounts->Preferences->save($preference)) {
                    $this->Flash->success(__('Akun berhasil ditambahkan.'));
                    return $this->redirect(['action' => 'pick']);
                }
            } else {
                $data = [
                    'user_id' => $this->Auth->user('id'),
                    'proxy_id' => $this->request->getData()['proxy_id'],
                    'pk' => 0,
                    'profpicurl' => null,
                    'username' => $this->request->getData()['username'],
                    'password' => $this->request->getData()['password'],
                    'fullname' => null,
                    'description' => null,
                    'followers' => 0,
                    'followings' => 0,
                    'contents' => 0,
                    'followtoday' => 0,
                    'liketoday' => 0,
                    'posttoday' => 0,
                    'commenttoday' => 0,
                    'started' => Time::now()->i18nFormat('yyyy-MM-dd'),
                    'ended' => Time::now()->i18nFormat('yyyy-MM-dd'),
                    'paid' => 0,
                    'closed' => 0,
                    'statusid' => 5,
                    'note' => 'Login berhasil, akun dapat digunakan',
                    'profpicurlfixed' => false,
                    'messy' => md5($this->request->getData()['username']),
                    'active' => true,
                ];
                $account = $this->Accounts->patchEntity($account, $data);
                if ($this->Accounts->save($account)) {
                    // Add Table preferences row related to this account
                    $preference = $this->Accounts->Preferences->newEntity();

                    $dataPreference = [
                        'account_id' => $account->id,
                        'maxlikeperday' => 500,
                        'maxcommentperday' => 500,
                        'maxfollowperday' => 300,
                        'maxpostperday' => 1,
                        'followidolfollower' => 0,
                        'followbylocation' => 0,
                        'followbyhashtag' => 0,
                        'likefeed' => 0,
                        'likebylocation' => 0,
                        'likebyhashtag' => 0,
                        'commentfeed' => 0,
                        'commentbyhashtag' => 0,
                        'commentbylocation' => 0,
                        'followtoday' => 0,
                        'liketoday' => 0,
                        'commenttoday' => 0,
                        'posttoday' => 0,
                        'hashtagtofollowtoday' => 0,
                        'gethashtagtofollowtoday' => true,
                        'active' => true,
                    ];
                    $preference = $this->Accounts->Preferences->patchEntity($preference, $dataPreference);
                    if ($this->Accounts->Preferences->save($preference)) {
                        $this->Flash->success(__('Akun berhasil ditambahkan.'));
                        return $this->redirect(['action' => 'pick']);
                        //return $this->redirect(['action' => 'index']);
                    }
                }
                $this->Flash->error(__('Ups, terjadi kesalahan. Silahkan mengulangi.'));
            }
        }

        $user = $this->Auth->user();
        $this->set(compact('account', 'user'));
    }

    public function check($uname = null, $pass = null)
    {
        $uname = str_replace('@', '', $uname);
        $uname = strtolower(trim($uname));

        $pass = trim($pass);
        $data = [];
        if (!empty($uname) && !empty($pass)) {
            $account = $this->Accounts->find()
                ->select(['id'])
                ->where(['username' => $uname, 'active' => true]);
            if ($account->count() > 0) {
                $data = ['status' => 0, 'text' => 'Akun telah digunakan'];
            } else {
                $checkTempAcc = $this->Accounts->find()
                    ->select(['id'])
                    ->where(['username' => $uname, 'password' => $pass, 'active' => false])
                    ->count();
                if ($checkTempAcc < 1) {
                    // save temporary account
                    $account = $this->Accounts->newEntity();
                    $data = [
                        'user_id' => $this->Auth->user('id'),
                        'proxy_id' => 1,
                        'pk' => 0,
                        'profpicurl' => null,
                        'username' => $uname,
                        'password' => $pass,
                        'fullname' => null,
                        'description' => null,
                        'followers' => 0,
                        'followings' => 0,
                        'contents' => 0,
                        'followtoday' => 0,
                        'liketoday' => 0,
                        'posttoday' => 0,
                        'commenttoday' => 0,
                        'started' => Time::now()->i18nFormat('yyyy-MM-dd'),
                        'ended' => Time::now()->i18nFormat('yyyy-MM-dd'),
                        'paid' => 0,
                        'closed' => 0,
                        'statusid' => 1,
                        'note' => 'Belum diuji login',
                        'profpicurlfixed' => false,
                        'messy' => md5($uname),
                        'active' => false,
                    ];
                    $account = $this->Accounts->patchEntity($account, $data);
                    $this->Accounts->save($account);
                }

                // get active proxy
                $proxy = 'http://0.0.0.0';
                $proxyId = 1;
                $proxies = $this->Accounts->Proxies->find()
                    ->select(['id', 'name'])
                    ->where(['active' => true, 'id IS NOT' => 1])
                    ->order(['id' => 'ASC'])
                    ->all();
                foreach ($proxies as $p) {
                    $proxyCount = $this->Accounts->find()
                        ->select(['id'])
                        ->where(['proxy_id' => $p['id'], 'statusid' => 5, 'active' => true]);
                    if ($proxyCount->count() < 10) {
                        $proxy = 'http://' . $p['name'];
                        $proxyId = $p['id'];
                        break;
                    }
                }
                //echo $proxy;

                $res = shell_exec(ROOT . DS . 'bin/cake checklogin ' . $uname . ' ' . $pass . ' ' . $proxy);
                if (!empty($res)) {
                    // if succeed
                    if ($res == '1') {
                        $data = ['status' => 1, 'text' => 'Login Berhasil', 'proxy_id' => $proxyId];
                        //shell_exec('mkdir /var/www/html/automateit2/vendor/mgp25/instagram-php/sessions/' . $uname);
                        shell_exec('cp -r ' . ROOT . DS . 'vendor/mgp25/instagram-php/sessions/' . $uname . ' /var/www/html/automateit2/vendor/mgp25/instagram-php/sessions');
                    } else {
                        if (strpos($res, 'incorrect') !== false || strpos(strtolower($res), 'challenge') !== false) {
                            $data['status'] = 0;
                        }
                        $res = str_replace('InstagramAPI\Response\LoginResponse:', '', $res);
                        $res = str_replace('InstagramAPI\Response\FacebookOTAResponse:', '', $res);
                        $data['text'] = trim($res);
                    }
                } else {
                    // if no data
                    $data = ['status' => '0', 'text' => 'Tidak ditemukan'];
                }
            }
        }
        $this->autoRender = false;
        $response = $this->response->withType('application/json')->withStringBody(json_encode($data));
        return $response;
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function indexold()
    {
        $user = $this->Auth->user();

        $accounts = $this->Accounts->find()
            ->where(['user_id' => $user['id'], 'active' => 1])
            ->all();

        $user = $this->Auth->user();

        $this->set(compact('accounts', 'user'));
    }

    /**
     * View method
     *
     * @param string|null $id Account id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function index($messy = null)
    {
        $this->viewBuilder()->setTemplate('view');
        if (!$this->isAuthorizedNew($this->user)) {
            $this->redirect(['action' => 'pick']);
        }

        //$session = $this->request->session();
        $id = $this->session->read('Config.account');
        $account = $this->Accounts->find()
            ->where(['id' => $id])
            ->contain(['Preferences'])
            ->first();
        /*$account = $this->Accounts->get($id, [
            'contain' => ['Preferences'],
        ]);*/

        $hashtaglists = $this->Accounts->Hashtaglists->find()
            ->where(['account_id' => $account['id'], 'active' => true, 'typeid' => 1, 'whitelist' => true])
            ->all()
            ->toArray();

        // Check if profile picture is exists
        $profilepicture = new File(WWW_ROOT . 'files' . DS . 'images' . DS . 'profilepicture' . DS . $account->id . '.jpg', false, 0644);
        $pp = false;
        if ($profilepicture->exists()) {
            $pp = true;
        }

        $profilepicture->close();

        $this->set('user', $this->user);
        $this->set(compact('account', 'pp', 'hashtaglists'));
    }
    /**
     * View method
     *
     * @param string|null $id Account id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if (!$this->isAuthorized($this->user)) {
            $this->redirect('index');
        }

        $account = $this->Accounts->get($id, [
            'contain' => ['Preferences'],
        ]);

        $hashtaglists = $this->Accounts->Hashtaglists->find()
            ->where(['account_id' => $account['id'], 'active' => true, 'typeid' => 1, 'whitelist' => true])
            ->all()
            ->toArray();

        // Check if profile picture is exists
        $profilepicture = new File(WWW_ROOT . 'files' . DS . 'images' . DS . 'profilepicture' . DS . $id . '.jpg', false, 0644);
        $pp = false;
        if ($profilepicture->exists()) {
            $pp = true;
        }

        $profilepicture->close();

        $this->set('user', $this->user);
        $this->set(compact('account', 'pp', 'hashtaglists'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $account = $this->Accounts->newEntity();
        if ($this->request->is('post')) {
            $data = [
                'user_id' => $this->Auth->user('id'),
                'proxy_id' => 1,
                'pk' => 0,
                'profpicurl' => null,
                'username' => $this->request->getData()['username'],
                'password' => $this->request->getData()['password'],
                'fullname' => null,
                'description' => null,
                'followers' => 0,
                'followings' => 0,
                'contents' => 0,
                'followtoday' => 0,
                'liketoday' => 0,
                'posttoday' => 0,
                'commenttoday' => 0,
                'started' => Time::now()->i18nFormat('yyyy-MM-dd'),
                'ended' => Time::now()->i18nFormat('yyyy-MM-dd'),
                'paid' => 0,
                'closed' => 0,
                'statusid' => 5,
                'note' => 'Login berhasil, akun dapat digunakan',
                'profpicurlfixed' => false,
                'messy' => md5($this->request->getData()['username']),
                'active' => true,
            ];
            $account = $this->Accounts->patchEntity($account, $data);
            if ($this->Accounts->save($account)) {
                // Add Table preferences row related to this account
                $preference = $this->Accounts->Preferences->newEntity();

                $dataPreference = [
                    'account_id' => $account->id,
                    'maxlikeperday' => 500,
                    'maxcommentperday' => 500,
                    'maxfollowperday' => 300,
                    'maxpostperday' => 1,
                    'followidolfollower' => 0,
                    'followbylocation' => 0,
                    'followbyhashtag' => 0,
                    'likefeed' => 0,
                    'likebylocation' => 0,
                    'likebyhashtag' => 0,
                    'commentfeed' => 0,
                    'commentbyhashtag' => 0,
                    'commentbylocation' => 0,
                    'followtoday' => 0,
                    'liketoday' => 0,
                    'commenttoday' => 0,
                    'posttoday' => 0,
                    'hashtagtofollowtoday' => 0,
                    'gethashtagtofollowtoday' => true,
                    'active' => true,
                ];
                $preference = $this->Accounts->Preferences->patchEntity($preference, $dataPreference);
                if ($this->Accounts->Preferences->save($preference)) {
                    $this->Flash->success(__('Akun berhasil ditambahkan, sistem akan menguji apakah username dan password dapat digunakan.'));
                    return $this->redirect(['action' => 'index']);
                }
            }
            $this->Flash->error(__('Ups, terjadi kesalahan. Silahkan mengulangi.'));
        }

        $user = $this->Auth->user();
        $this->set(compact('account', 'user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Account id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if (!$this->isAuthorized($this->user)) {
            $this->redirect('index');
        }

        $account = $this->Accounts->get($id, [
            'contain' => ['Preferences'],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();

            // Update preferences table
            $preference = $this->Accounts->Preferences->query();
            $preference->update()
                ->set([
                    'maxlikeperday' => $data['maxlikeperday'],
                    'maxfollowperday' => $data['maxfollowperday'],
                    'maxunfollowperday' => $data['maxunfollowperday'],
                    'followbyhashtag' => $data['followbyhashtag'],
                ])
                ->where(['account_id' => $account['id'], 'active' => true])
                ->execute();

            // If account choose to followbyhashtag
            if ($data['followbyhashtag'] && !empty($data['hashtagtofollow'])) {
                $hashtags = explode(',', $data['hashtagtofollow']);

                // Inactivate all hashtags
                $this->Accounts->Hashtaglists->query()
                    ->update()
                    ->set(['active' => false])
                    ->where(['account_id' => $account['id'], 'active' => true])
                    ->execute();

                foreach ($hashtags as $h) {
                    $h = strtolower(trim(str_replace('#', '', $h)));
                    $findHashtag = $this->Accounts->Hashtaglists->find()
                        ->where(['account_id' => $account['id'], 'caption' => $h]);
                    if ($findHashtag->count() == 0) {
                        $d = [
                            'account_id' => $account['id'],
                            'typeid' => 1,
                            'whitelist' => true,
                            'caption' => $h,
                            'active' => true,
                        ];
                        $hashtaglist = $this->Accounts->Hashtaglists->newEntity();
                        $hashtaglist = $this->Accounts->Hashtaglists->patchEntity($hashtaglist, $d);
                        $this->Accounts->Hashtaglists->save($hashtaglist);
                    } else {
                        $this->Accounts->Hashtaglists->query()
                            ->update()
                            ->set(['active' => true])
                            ->where(['account_id' => $account['id'], 'caption' => $h])
                            ->execute();
                    }
                }
            }

            $message = 'Setup Akun berhasil diubah.';
            // Update accounts table
            if ($account['password'] != $data['password']) {
                $data['statusid'] = 1;
                $data['note'] = 'Belum diuji login';

                $account = $this->Accounts->patchEntity($account, $data);
                if (!$this->Accounts->save($account)) {
                    $message = 'Ups! Gagal mengubah Setup Akun, silahkan ulangi.';
                }
            }

            $this->Flash->success(__($message));

            return $this->redirect(['action' => 'view', $id]);
        }
        $hashtagWhite = $this->Accounts->Hashtaglists->find()
            ->where(['account_id' => $account['id'], 'active' => 1, 'typeid' => 1, 'whitelist' => 1])->all();

        $hashtagWhiteString = '';
        foreach ($hashtagWhite as $h) {
            $hashtagWhiteString = $hashtagWhiteString . $h['caption'] . ',';
        }

        $this->set('user', $this->user);
        $this->set(compact('account', 'hashtagWhiteString', 'hashtagWhite'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Account id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($messy = null)
    {
        if (!empty($messy)) {
            $data = $this->Accounts->find()
                ->select(['id'])
                ->where(['messy' => $messy, 'user_id' => $this->user['id']]);
            $dataCount = $data->count();
            if ($dataCount == 0) {
                return $this->redirect('pick');
            } elseif ($dataCount == 1) {
                $data = $data->first();
                $account = $this->Accounts->get($data['id']);
                $data = ['active' => false];
                $account = $this->Accounts->patchEntity($account, $data);
                if ($this->Accounts->save($account)) {
                    $this->Flash->success(__('Berhasil menghapus akun.'));
                } else {
                    $this->Flash->success(__('Ups! Gagal menghapus akun, silahkan mengulangi.'));
                }
                return $this->redirect(['action' => 'pick']);
            }
        } else {
            return $this->redirect(['action' => 'pick']);
        }
        if (!$this->isAuthorized($this->user)) {
            $this->redirect('index');
        }

        /*$account = $this->Accounts->get($id, [
            'contain' => ['Preferences'],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = ['active' => false];
            $account = $this->Accounts->patchEntity($account, $data);
            if ($this->Accounts->save($account)) {
                $this->Flash->success(__('Berhasil menghapus akun.'));
            } else {
                $this->Flash->success(__('Ups! Gagal menghapus akun, silahkan mengulangi.'));
            }
            return $this->redirect(['action' => 'index']);
        }

        $this->set('user', $this->user);
        $this->set(compact('account'));*/
    }

    public function tohandleworkers($workerNo = 0, $pass = null)
    {
        $this->request->trustProxy = true;
        $ip = $this->request->clientIp();
        $data = [];
        if (!empty($ip) && filter_var($ip, FILTER_VALIDATE_IP)) {
            $this->loadComponent('Arango');

            if ($ip == '127.0.0.1') {
                $ip = '0.0.0.0';
            } else {
                $ip = $ip . ':6969';
            }


            if ($workerNo > 0 && $pass == 'j4y4pur4') {
                $proxy = $this->Accounts->Proxies->find()
                    ->where(['id' => $workerNo]);
            } else {
                $proxy = $this->Accounts->Proxies->find()
                    ->where(['name' => $ip]);
            }

            if ($proxy->count() == 1) {
                $proxy = $proxy->select(['id', 'name'])->first();
                $accounts = $this->Accounts->find()
                    ->select([
                        'id', 'username',
                        'password', 'pk',
                        'followers', 'followings',
                        'contents'
                    ])
                    ->where([
                        'proxy_id' => $proxy->id,
                        'statusid' => 5,
                        'active' => 1
                    ])
                    ->order(['id' => 'ASC'])
                    ->limit(10)
                    ->all();
                foreach ($accounts as $account) {
                    $query = 'FOR f IN followerof FILTER f._from == "members/' . $account['pk'] . '" ';
                    $query = $query . 'SORT f.gatherat DESC LIMIT 1 ';
                    $query = $query . 'FOR m IN members FILTER f._to == m._id ';
                    $query = $query . 'FOR u IN members FILTER f._from == u._id RETURN {pk: m._key, from_pk: u._key}';
                    $lastInsertedMember = json_decode($this->Arango->query($query, 'one_document'), true);
                    //print_r($lastInsertedMember);
                    $lastFollowerPk = 0;
                    if ($lastInsertedMember != null) $lastFollowerPk = $lastInsertedMember['pk'];

                    // update pk
                    $q = $this->Accounts->query();
                    $q->update()
                        ->set(['pk' => $lastInsertedMember['from_pk']])
                        ->where(['id' => $account['id']])
                        ->execute();

                    array_push($data, [
                        'id' => $account['id'],
                        'username' => $account['username'],
                        'password' => $account['password'],
                        'pk' => $account['pk'],
                        'followers' => $account['followers'],
                        'followings' => $account['followings'],
                        'contents' => $account['contents'],
                        'last_follower_pk' => $lastFollowerPk,
                        'proxy_id' => $proxy->id,
                        'proxy' => $proxy->name
                    ]);
                }
            }
        }
        $this->autoRender = false;
        $response = $this->response->withType('application/json')->withStringBody(json_encode($data));
        return $response;
    }

    public function testarango()
    {
        /*$arangoHost = '206.189.46.73';

        $connectionOptions = array(
    // server endpoint to connect to
            ConnectionOptions::OPTION_ENDPOINT => 'tcp://' . $arangoHost . ':8529',
    // authorization type to use (currently supported: 'Basic')
            ConnectionOptions::OPTION_AUTH_TYPE => 'Basic',
    // user for basic authorization
            ConnectionOptions::OPTION_AUTH_USER => 'user',
    // password for basic authorization
            ConnectionOptions::OPTION_AUTH_PASSWD => 'jayapura',
    // connection persistence on server. can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
            ConnectionOptions::OPTION_CONNECTION => 'Close',
    // connect timeout in seconds
            ConnectionOptions::OPTION_TIMEOUT => 3,
    // whether or not to reconnect when a keep-alive connection has timed out on server
            ConnectionOptions::OPTION_RECONNECT => true,
    // optionally create new collections when inserting documents
            ConnectionOptions::OPTION_CREATE => true,
    // optionally create new collections when inserting documents
            ConnectionOptions::OPTION_UPDATE_POLICY => UpdatePolicy::LAST,
            ConnectionOptions::OPTION_DATABASE => 'automateit',
        );

        // open connection
        $connection = new Connection($connectionOptions);

        // create a new collection
        $collectionName = 'members';
        $collection = new Collection($collectionName);
        $collectionHandler = new CollectionHandler($connection);

        if (!$collectionHandler->has($collectionName)) {
            $collectionId = $collectionHandler->create($collection);
        }

        //$documentHandler = new DocumentHandler($connection);

        $query = 'FOR m IN members FILTER m._key == "2240556769" RETURN m';

        $statement = new Statement(
            $connection,
            [
                'query' => $query,
                'count' => true,
                'batchSize' => 1,
                'sanitize' => true
            ]
            );
        $cursor = $statement->execute();

        //print_r($cursor->getAll()[0]);
        $data = [];
        if (count($cursor->getAll()) > 0) {
            $data = $cursor->getAll()[0]->toJson();
            /*$d = $cursor->getAll()[0];
            $data = [
                'pk' => $d->pk,
                'username' => $d->username,
            ];*
        }*/
        $this->loadComponent('Arango');
        $query = 'FOR m IN members FILTER m._key == "22405567690" RETURN m';

        $data = $this->Arango->query($query, 'one_document');
        //print_r($data);
        //if ($data == null) echo 'null';
      
        $this->autoRender = false;
        $response = $this->response->withType('application/json')->withStringBody($data);
        return $response;
    }
}
