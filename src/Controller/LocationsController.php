<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Locations Controller
 *
 * @property \App\Model\Table\LocationsTable $Locations
 *
 * @method \App\Model\Entity\Location[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LocationsController extends AppController
{
    public $user;
    public $session;
    public $accountOnSession;

    public function initialize()
    {
        parent::initialize();
        $this->user = $this->Auth->user();
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->session = $this->request->getSession();
        $session = $this->request->getSession();
        $this->accountOnSession = $session->read('Config.account');
    }

    /*public function isAuthorizedNew($user)
    {
        $action = $this->request->getParam('action');

        // All actions require an id
        //$id = $this->request->getParam('pass.0');
        //$session = $this->request->session();
        //$id = $this->session->read('Config.account');
        $id = $this->accountOnSession;
        if (!$id) {
            return false;
        }

        // Check that the account belongs to the current user.
        $account = $this->Accounts->findById($id)->first();

        return $account->user_id === $user['id'];
    }*/

    /*public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');

        // All actions require an id
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }

        // Check that the account belongs to the current user.
        $account = $this->Accounts->findById($id)->first();

        return $account->user_id === $user['id'];
    }*/
 
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $locations = $this->paginate($this->Locations);

        $this->set(compact('locations'));
    }

    /**
     * View method
     *
     * @param string|null $id Location id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $location = $this->Locations->get($id, [
            'contain' => ['Posts', 'Cargos', 'Replicas']
        ]);

        $this->set('location', $location);
    }

    //public function search($query = null, $id = null) {
    public function search($id = null) {
        $query = $this->request->getQuery('term');
        $data = [];
        if (!empty($query)) {
            $arrQuery = explode(' ', $query);
            if (count($arrQuery) > 1) {
                $query = implode('+', $arrQuery);
                $querySql = implode(' ', $arrQuery);
            } else {
                $query = $arrQuery[0];
                $querySql = $arrQuery[0];
            }
            $id = $this->accountOnSession;
            //if ($id == null) $id = 1;
            // on production server use this
            //if ($id == null) $id = 4;
            
            //$locations = shell_exec('php /var/www/html/automateit2/testlocation.php ' . $query);
            // search on local db
            $locations = $this->Locations->find()
                ->select(['id', 'name', 'address'])
                ->where(['LOWER(name) LIKE' => '%' . strtolower($querySql) . '%'])
                ->limit(12);

            // gather all data from db
            $results = $locations->all();
            foreach($results as $r) {
                $name = $r->name;
                if (!empty($r->address)) $name = $name . ' (' . $r->address . ')';
                array_push($data, ['id' => $r->id, 'text' => $name]);
            }

            if ($locations->count() < 12) {
                $location = shell_exec(ROOT . DS . 'bin/cake searchlocation ' . $query . ' ' . $id);
                if (!empty($location)) {
                    $location = str_replace('}{', '},{', $location);
                    $location = '[' . $location . ']';
                    $locations = json_decode($location, true);

                    // save to locations table
                    foreach ($locations as $l) {
                        $loc = $this->Locations->find()
                            ->select(['id', 'name', 'address'])
                            ->where(['fbplacesid' => (int)$l['external_id']]);
                            //->first();
                        if ($loc->count() > 0) {
                            $thisLoc = $loc->first();
                            $name = $thisLoc->name;
                            if (!empty($thisLoc->address)) $name = $name . ' (' . $thisLoc->address . ')';
                            array_push($data, [
                                'id' => $thisLoc->id,
                                'text' => $name]);
                        } else {
                            $newLocation = $this->Locations->newEntity();
                            $newLocation = $this->Locations->patchEntity($newLocation, [
                                'pk' => 0,
                                'fbplacesid' => $l['external_id'],
                                'lat' => $l['lat'],
                                'lng' => $l['lng'],
                                'address' => $l['address'],
                                'name' => $l['name'],
                                'shortname' => $l['name'],
                                'active' => true
                            ]);
                            if ($this->Locations->save($newLocation)) {
                                $name = $newLocation->name;
                                if (!empty($newLocation->address)) $name = $name . ' (' . $newLocation->address . ')';
                                array_push($data, [
                                    'id' => $newLocation->id,
                                    'text' => $name]);
                            }
                        }
                    }
                } else {
                    // if no data
                    $data = [['id' => 1, 'text' => 'Tidak ditemukan']];
                }
            }
        }
        $this->autoRender = false;
        //$this->response->getType('json');
        //$this->response->getBody(json_encode(['results'=> $data]));
        //return $this->response;
        $response = $this->response->withType('application/json')->withStringBody(json_encode(['results' => $data]));
        return $response;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $location = $this->Locations->newEntity();
        if ($this->request->is('post')) {
            $location = $this->Locations->patchEntity($location, $this->request->getData());
            if ($this->Locations->save($location)) {
                $this->Flash->success(__('The location has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The location could not be saved. Please, try again.'));
        }
        $this->set(compact('location'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Location id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $location = $this->Locations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $location = $this->Locations->patchEntity($location, $this->request->getData());
            if ($this->Locations->save($location)) {
                $this->Flash->success(__('The location has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The location could not be saved. Please, try again.'));
        }
        $this->set(compact('location'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Location id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $location = $this->Locations->get($id);
        if ($this->Locations->delete($location)) {
            $this->Flash->success(__('The location has been deleted.'));
        } else {
            $this->Flash->error(__('The location could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
