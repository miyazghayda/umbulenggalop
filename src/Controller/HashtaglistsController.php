<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Hashtaglists Controller
 *
 * @property \App\Model\Table\HashtaglistsTable $Hashtaglists
 *
 * @method \App\Model\Entity\Hashtaglist[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HashtaglistsController extends AppController
{

    public $user;
    public $session;
    public $accountOnSession;
    public $paidAccounts;
    public $orAccounts;
    public $paidAccountIds;

    public function initialize() {
        parent::initialize();
        $this->Auth->allow(['checkidol']);

        $this->user = $this->Auth->user();
        $this->paidAccounts = $this->Hashtaglists->Accounts->find()
             ->where(['user_id' => $this->user['id'], 'active' => 1, 'statusid' => 5])
             ->all();

        // Only show paid (5) account(s)
        $orAccounts = [];
        $paidAccountIds = [];

        foreach ($this->paidAccounts as $account) {
            array_push($orAccounts, ['account_id' => $account['id']]);
            //$orAccounts = array_merge($orAccounts, ['account_id' => $account['id']]);
            array_push($paidAccountIds, $account['id']);
        }
        $this->orAccounts = $orAccounts;
        $this->paidAccountIds = $paidAccountIds;

        $this->loadComponent('RequestHandler');
    }

    public function isAuthorized($user) {
        $action = $this->request->getParam('action');

        // All actions require an id
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }

        // Check that the cargo belongs to the current user.
        $data = $this->Hashtaglists->findById($id)->first();

        if (in_array($data->account_id, $this->paidAccountIds)) {
            return true;
        } else {
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->session = $this->request->getSession();
        $session = $this->request->getSession();
        $this->accountOnSession = $session->read('Config.account');
    }

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $this->set('user', $this->user);
    }

    public function activate() {
        $preference = $this->Hashtaglists->Accounts->Preferences->find()
            ->where(['account_id' => $this->accountOnSession])
            ->first();

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if (empty($data['followbyhashtag'])) {
                $data['followbyhashtag'] = false;
            }            
            $preference = $this->Hashtaglists->Accounts->Preferences->patchEntity($preference, $data);
            if ($this->Hashtaglists->Accounts->Preferences->save($preference)) {
                $this->Flash->success(__('Berhasil menyimpan'));
            }
        }

        $this->set(compact('preference'));
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($account_id = null)
    {
        if ($this->accountOnSession > 0) {
            $data = $this->Hashtaglists->find()
                ->where(['Hashtaglists.typeid' => 1,
                'Hashtaglists.account_id' => $this->accountOnSession,
                'Hashtaglists.whitelist' => 1,
                'Hashtaglists.active' => true])
                ->contain(['accounts'])
                ->all();
        } else {
            $data = [];
        }
        $this->set(compact('data'));
    }
    public function indexold($account_id = null)
    {
        // If id specified
        $orAccounts = [];
        foreach ($this->orAccounts as $key => $value) {
            array_push($orAccounts, ['Hashtaglists.account_id' => $value['account_id']]);
        }

        if (count($orAccounts) > 0) {
            if ($account_id > 0) {
                $data = $this->Hashtaglists->find()
                ->where(['Hashtaglists.typeid' => 1, 'Hashtaglists.account_id' => $account_id, 'Hashtaglists.whitelist' => 1, 'Hashtaglists.active' => true])
                ->contain(['accounts'])
                ->all();
            } else {
                $data = $this->Hashtaglists->find()
                ->where(['Hashtaglists.typeid' => 1, 'Hashtaglists.whitelist' => 1, 'Hashtaglists.active' => true, 'OR' => $orAccounts])
                ->contain(['accounts'])
                ->all();
            }
        } else {
            $data = [];
        }
        $accounts = $this->Hashtaglists->Accounts->find('list', [
            'keyField' => 'id',
            'valueField' => 'username',
        ])
            ->where(['user_id' => $this->user['id'], 'active' => 1, 'statusid' => 5])
            ->order(['username' => 'ASC'])
            ->toArray();
        $accounts[0] = 'Semua';

        $this->set(compact('data', 'accounts', 'account_id'));
    }
    /**
     * View method
     *
     * @param string|null $id Hashtaglist id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $hashtaglist = $this->Hashtaglists->get($id, [
            'contain' => ['Accounts']
        ]);

        $this->set('hashtaglist', $hashtaglist);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addwhite()
    {
        $hashtaglist = $this->Hashtaglists->newEntity();
        if ($this->request->is('post') && !empty($this->request->getData()['caption'])) {
            $data = $this->request->getData();
            $data['caption'] = trim(strtolower($data['caption']));
            $data['account_id'] = $this->accountOnSession;
            $data['filter_id'] = 1;
            $data['typeid'] = 1;
            $data['whitelist'] = true;
            $data['active'] = true;
            
            // Insert/Update Filter
            $filter_id = 1;
            if ((int)$data['filter'] === 1) {
                //echo $data['filter'];
                $data['fullnameblacklist'] = str_replace(' ', '', $data['fullnameblacklist']);
                $data['biographyblacklist'] = str_replace(' ', '', $data['biographyblacklist']);
                $data['hashtagblacklist'] = str_replace(' ', '', $data['hashtagblacklist']);

                $datum = [
                    'account_id' => $data['account_id'],
                    'typeid' => 1,
                    'hashtagblacklist' => $data['hashtagblacklist'],
                    'fullnameblacklist' => $data['fullnameblacklist'],
                    'biographyblacklist' => $data['biographyblacklist'],
                    'nmonths' => 0,
                    'accounttypeid' => 1,
                    'active' => true,
                ];

                // If filter exist, use id, for other condition, create first
                if ($this->Hashtaglists->Filters->exists($datum)) {
                    $filter = $this->Hashtaglists->Filters->find()
                        ->where($datum)
                        ->first();
                    $filter_id = $filter->id;
                } else {
                    $filter = $this->Hashtaglists->Filters->newEntity();
                    $filter = $this->Hashtaglists->Filters->patchEntity($filter, $datum);
                    if ($this->Hashtaglists->Filters->save($filter)) $filter_id = $filter->id;
                }
            }

            // Insert Hashtaglist
            if ($this->Hashtaglists->exists(['account_id' => $data['account_id'], 'caption' => $data['caption'], 'typeid' => 1, 'active' => true])) {
                $hashtaglist = $this->Hashtaglists->find()
                    ->where(['account_id' => $data['account_id'], 'caption' => $data['caption'], 'typeid' => 1, 'active' => true])
                    ->first();
                $hashtaglist->filter_id = $filter_id;
                $res = $this->Hashtaglists->save($hashtaglist);
            } else {
                $datum = [
                    'account_id' => $data['account_id'],
                    'filter_id' => $filter_id,
                    'typeid' => 1,
                    'whitelist' => true,
                    'caption' => $data['caption'],
                    'active' => true
                ];
                $hashtaglist = $this->Hashtaglists->newEntity();
                $hashtaglist = $this->Hashtaglists->patchEntity($hashtaglist, $datum);
                $res = $this->Hashtaglists->save($hashtaglist);
            }

            // Update account preference
            $preference = $this->Hashtaglists->Accounts->Preferences->find()
                ->where(['account_id' => $data['account_id'], 'active' => true])
                ->first();
            $preference->followbyhashtag = true;
            $this->Hashtaglists->Accounts->Preferences->save($preference);

            $this->Flash->success(__('Berhasil menyimpan data.'));

            return $this->redirect(['action' => 'index']);
        }
        //$accounts = $this->Hashtaglists->Accounts->find('list', ['limit' => 200]);
        $this->set(compact('hashtaglist'));
    }


    public function add()
    {
        $hashtaglist = $this->Hashtaglists->newEntity();
        if ($this->request->is('post')) {
            $hashtaglist = $this->Hashtaglists->patchEntity($hashtaglist, $this->request->getData());
            if ($this->Hashtaglists->save($hashtaglist)) {
                $this->Flash->success(__('The hashtaglist has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The hashtaglist could not be saved. Please, try again.'));
        }
        $accounts = $this->Hashtaglists->Accounts->find('list', ['limit' => 200]);
        $this->set(compact('hashtaglist', 'accounts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Hashtaglist id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $hashtaglist = $this->Hashtaglists->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $hashtaglist = $this->Hashtaglists->patchEntity($hashtaglist, $this->request->getData());
            if ($this->Hashtaglists->save($hashtaglist)) {
                $this->Flash->success(__('The hashtaglist has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The hashtaglist could not be saved. Please, try again.'));
        }
        $accounts = $this->Hashtaglists->Accounts->find('list', ['limit' => 200]);
        $this->set(compact('hashtaglist', 'accounts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Hashtaglist id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $hashtaglist = $this->Hashtaglists->get($id);
        if ($this->Hashtaglists->delete($hashtaglist)) {
            $this->Flash->success(__('The hashtaglist has been deleted.'));
        } else {
            $this->Flash->error(__('The hashtaglist could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
