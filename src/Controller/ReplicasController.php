<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;

/**
 * Replicas Controller
 *
 * @property \App\Model\Table\ReplicasTable $Replicas
 *
 * @method \App\Model\Entity\Replica[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReplicasController extends AppController
{
    public $user;
    public $session;
    public $accountOnSession;
    public $paidAccounts;
    public $orAccounts;
    public $paidAccountIds;
    public $paginate = [
        'limit' => 24,
        'order' => [
            'Posts.takenat' => 'ASC'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->user = $this->Auth->user();
        $this->paidAccounts = $this->Replicas->Accounts->find()
            ->where(['user_id' => $this->user['id'], 'active' => 1, 'statusid' => 5])
            ->all();

        // Only show paid (5) account(s)
        $orAccounts = [];
        $paidAccountIds = [];

        foreach ($this->paidAccounts as $account) {
            array_push($orAccounts, ['account_id' => $account['id']]);
            array_push($paidAccountIds, $account['id']);
        }
        $this->orAccounts = $orAccounts;
        $this->paidAccountIds = $paidAccountIds;
    }

    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');

        // All actions require an id
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }

        // Check that the cargo belongs to the current user.
        $data = $this->Replicas->findById($id)->first();

        if (in_array($data->account_id, $this->paidAccountIds)) {
            return true;
        } else {
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->session = $this->request->getSession();
        $session = $this->request->getSession();
        $this->accountOnSession = $session->read('Config.account');
    }

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $this->set('user', $this->user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if ($this->accountOnSession > 0) {
            $query = $this->Replicas->find()
                           ->where(['Replicas.uploaded' => true, 'Replicas.active' => 1, 'Replicas.account_id' => $this->accountOnSession])
                           ->order(['schedule' => 'ASC', 'Replicas.takenat' => 'ASC'])
                           ->contain(['Posts', 'Posts.Wads']);
        } else {
            //$cargos = [];
            $query = [];
        }
        $this->set('cargos', $this->paginate($query));
    }
    public function indexold($id = null)
    {
        $orAccounts = $this->orAccounts;
        // If id specified
        if ($id > 0 && in_array($id, $this->paidAccountIds)) $orAccounts = ['account_id' => $id];

        if (count($orAccounts) > 0) {
            $query = $this->Replicas->find()
                           ->where(['Replicas.uploaded' => true, 'Replicas.active' => 1, 'OR' => $orAccounts])
                           ->order(['schedule' => 'ASC', 'Replicas.takenat' => 'ASC'])
                           ->contain(['Posts', 'Posts.Wads']);
        } else {
            //$cargos = [];
            $query = [];
        }
        $accounts = $this->Replicas->Accounts->find('list', [
            'keyField' => 'id',
            'valueField' => 'username'
        ])
                         ->where(['user_id' => $this->user['id'], 'statusid' => 5, 'Accounts.active' => 1])
                         ->order(['username' => 'ASC'])
                         ->toArray();
        $accounts[0] = 'Semua';

        //$this->set(compact('cargos', 'accounts'));
        $this->set(compact('accounts'));
        $this->set('cargos', $this->paginate($query));
    }
    public function queue() {
        if ($this->accountOnSession > 0) {
            $query = $this->Replicas->find()
                           ->where(['Replicas.uploaded' => false, 'Replicas.active' => 1, 'Replicas.account_id' => $this->accountOnSession])
                           ->order(['schedule' => 'ASC', 'Replicas.takenat' => 'ASC'])
                           ->contain(['Posts', 'Posts.Wads']);
        } else {
            $query = [];
        }
        //$this->set(compact('accounts'));
        $this->set('cargos', $this->paginate($query));
    }
    public function queueold($id = null) {
        $orAccounts = $this->orAccounts;
        // If id specified
        if ($id > 0 && in_array($id, $this->paidAccountIds)) $orAccounts = ['account_id' => $id];

        if (count($orAccounts) > 0) {
            $query = $this->Replicas->find()
                           ->where(['Replicas.uploaded' => false, 'Replicas.active' => 1, 'OR' => $orAccounts])
                           ->order(['schedule' => 'ASC', 'Replicas.takenat' => 'ASC'])
                           ->contain(['Posts', 'Posts.Wads']);

            /*$cargos = $this->Replicas->find()
                           ->where(['Replicas.uploaded' => false, 'Replicas.active' => 1, 'OR' => $orAccounts])
                           ->order(['schedule' => 'ASC', 'Replicas.takenat' => 'ASC'])
                           ->contain(['Posts', 'Posts.Wads'])
                           ->limit(24)
                           ->all();*/
        } else {
            //$cargos = [];
            $query = [];
        }
        $accounts = $this->Replicas->Accounts->find('list', [
            'keyField' => 'id',
            'valueField' => 'username'
        ])
                         ->where(['user_id' => $this->user['id'], 'statusid' => 5, 'Accounts.active' => 1])
                         ->order(['username' => 'ASC'])
                         ->toArray();
        $accounts[0] = 'Semua';

        //$this->set(compact('cargos', 'accounts'));
        $this->set(compact('accounts'));
        $this->set('cargos', $this->paginate($query));
    }
    /**
     * View method
     *
     * @param string|null $id Replica id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if (!$this->isAuthorized($this->user)) {
            $this->redirect(['action' => 'queue']);
        }
        
        $replica = $this->Replicas->get($id, [
            'contain' => ['Accounts', 'Members', 'Posts', 'Posts.Wads', 'Locations']
        ]);

        $this->set('replica', $replica);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $replica = $this->Replicas->newEntity();
        if ($this->request->is('post')) {
            $replica = $this->Replicas->patchEntity($replica, $this->request->getData());
            if ($this->Replicas->save($replica)) {
                $this->Flash->success(__('The replica has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The replica could not be saved. Please, try again.'));
        }
        $accounts = $this->Replicas->Accounts->find('list', ['limit' => 200]);
        $members = $this->Replicas->Members->find('list', ['limit' => 200]);
        $posts = $this->Replicas->Posts->find('list', ['limit' => 200]);
        $this->set(compact('replica', 'accounts', 'members', 'posts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Replica id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if (!$this->isAuthorized($this->user)) {
            $this->redirect(['action' => 'queue']);
        }
        
        $replica = $this->Replicas->get($id, [
            'contain' => ['Accounts', 'Members', 'Posts', 'Posts.Wads']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $replica = $this->Replicas->patchEntity($replica, $this->request->getData());
            if ($this->Replicas->save($replica)) {
                $this->Flash->success(__('Konten berhasil diubah.'));

                return $this->redirect(['action' => 'queue']);
            }
            $this->Flash->error(__('Ups, terjadi kesalahan. Silahkan mengulangi.'));
        }
        $orAccounts = [];
        foreach ($this->paidAccountIds as $id) array_push($orAccounts, ['id' => $id]);

        if (count($orAccounts) > 0) {
            $accounts = $this->Replicas->Accounts->find('list', [
                'keyField' => 'id',
                'valueField' => 'username'
            ])
                             ->where(['OR' => $orAccounts])
                             ->order(['username' => 'ASC'])
                             ->toArray();

            $this->set('user', $this->user);
            $this->set(compact('replica', 'accounts'));
        } else {
            $this->redirect(['action' => 'queue']);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Replica id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if (!$this->isAuthorized($this->user)) {
            $this->redirect(['action' => 'queue']);
        }

        $replica = $this->Replicas->get($id, [
            'contain' => ['Accounts', 'Members', 'Posts', 'Posts.Wads']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = ['active' => false];
            
            $replica = $this->Replicas->patchEntity($replica, $data);
            if ($this->Replicas->save($replica)) {
                // update post after deleted
                $afterData = $this->Replicas->find()
                    ->select(['id', 'schedule'])
                    ->where([
                        'account_id' => $this->accountOnSession,
                        'member_id' => $replica['member_id'],
                        'schedule >' => $replica['schedule']->i18nFormat('yyyy-MM-dd HH:mm:ss'),
                        'active' => true])
                    ->order(['schedule' => 'ASC'])
                    ->all();
                $lastTime = $replica['schedule'];
                foreach ($afterData as $ad) {
                    $lastTimeNew = $ad['schedule'];
                    $query = $this->Replicas->query();
                    $query->update()
                        ->set(['schedule' => $lastTime])
                        ->where(['id' => $ad['id']])
                        ->execute();
                    $lastTime = $lastTimeNew;
                }


                $this->Flash->success(__('Berhasil menghapus konten.'));
            } else {
                $this->Flash->success(__('Ups! Gagal menghapus konten, silahkan mengulangi.'));
            }
            return $this->redirect(['action' => 'queue']);
        }

        $this->set(compact('replica'));
    }

    public function bulkdelete($id = null) {
        $ret = 0;
        if (!empty($id)) {
            $id = (int)$id;
            $data = $this->Replicas->findById($id)->first();
            if (in_array($data->account_id, $this->paidAccountIds)) {
                $replica = $this->Replicas->get($id);
                $d = ['active' => false];
                $replica = $this->Replicas->patchEntity($replica, $d);
                if ($this->Replicas->save($replica)) {
                    $ret = 1;
                }
            } else {
                $ret = 0;
            }
        }
        $this->autoRender = false;
        $response = $this->response->withType('application/json')->withStringBody(json_encode($ret));
        return $response;
        //$this->response->getType('json');
        //$this->response->getBody($ret);
        //$this->response->body(json_encode(['results'=> $data]));
        //return $this->response;
    }
}