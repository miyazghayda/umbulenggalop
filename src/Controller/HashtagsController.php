<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\I18n\Number;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Hashtags Controller
 *
 *
 * @method \App\Model\Entity\Hashtag[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HashtagsController extends AppController
{
    public $user;
    public $session;
    public $accountOnSession;
    public $Posts;
    public $Members;
    public $Locations;
    public $Wads;
    public $Replicas;

    public function initialize()
    {
        parent::initialize();
        $this->user = $this->Auth->user();
        $this->Posts = TableRegistry::get('Posts');
        $this->Members = TableRegistry::get('Members');
        $this->Locations = TableRegistry::get('Locations');
        $this->Wads = TableRegistry::get('Wads');
        $this->Replicas = TableRegistry::get('Replicas');
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->session = $this->request->getSession();
        $session = $this->request->getSession();
        $this->accountOnSession = $session->read('Config.account');
    }

    public function isAuthorizedNew($user)
    {
        $action = $this->request->getParam('action');

        // All actions require an id
        //$id = $this->request->getParam('pass.0');
        //$session = $this->request->session();
        //$id = $this->session->read('Config.account');
        $id = $this->accountOnSession;
        if (!$id) {
            return false;
        }

        // Check that the account belongs to the current user.
        $account = $this->Accounts->findById($id)->first();

        return $account->user_id === $user['id'];
    }

    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');

        // All actions require an id
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }

        // Check that the account belongs to the current user.
        $account = $this->Accounts->findById($id)->first();

        return $account->user_id === $user['id'];
    }

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $this->set('user', $this->user);
    }

    public function seeposts($hashtag = null) {

    }
    
    private function cache_get($cachePath) {
        @include $cachePath;
        return isset($val) ? $val : false;
    }

    public function seepostsdig($hashtag = null) {
        //$query = trim($this->request->getQuery('term'));
        //$account_id = trim($this->request->getQuery('account_id'));
        $query = trim($hashtag);
        $account_id = $this->accountOnSession;
        //$account_id = (int)trim($account_id);
        $query = str_replace('#', '', $query);
        $query = str_replace(' ', '', $query);
        //$query = 'exploreindonesia';
        //$account_id = 1;
        $data['related'] = '';
        $data['posts'] = [];
        $many = 100;

        if (!empty($query)) {
            $posts = shell_exec(ROOT . DS . 'bin/cake seeposts ' . $account_id . ' ' . $query . ' ' . $many);
            if (!empty($posts)) {
                $cacheName = $posts;
                $retFile = $this->cache_get(TMP . 'cache' . DS . 'seeposts' . DS . $posts);
                //$fileHashtag = new File(TMP . 'seeposts' . DS . $posts . '_hashtagrelated.json');
                //$fileData = new File(TMP . 'seeposts' . DS . $posts . '.json');
                if ($retFile != false) {
                //if ($fileData->exists() && $fileHashtag->exists()) {
                    //$relatedTags = json_decode($fileHashtag->read(), true);
                    $relatedTags = $retFile['hashtag'];
                    $data['related'] = $relatedTags;
                    $posts = [];
                    //$medias = json_decode($fileData->read(), true);
                    $medias = json_decode($retFile['medias']);
                    foreach ($medias as $media) {
                        $takenat = Time::createFromTimestamp($media->taken_at);
                        $takenat->timezone = 'Asia/Jakarta';
                        $locationId = 1;
                        $locationName = 'Tanpa Nama';
                        //if (array_key_exists('location', $media)) {
                        if (isset($media->location)) {
                            $locationId = $media->location->pk;
                            $locationName = $media->location->name;
                        }

                        $w = [];
                        if ($media->media_type == 1) {// photo
                            $w['id'] = 0;
                            $w['url'] = $media->image_versions2->candidates[0]->url;
                        } elseif ($media->media_type == 2) {// video
                            $w['id'] = 0;
                            $w['url'] = $media->video_versions[0]->url;
                        } elseif ($media->media_type == 8) {// carousel
                            $w['id'] = 0;
                            $m = $media->carousel_media[0];
                            if ($m->media_type == 1) {
                                $w['url'] = $m->image_versions2->candidates[0]->url;
                            } elseif ($m->media_type == 2) {
                                $w['url'] = $m->video_versions[0]->url;
                            }
                        }

                        $d = [
                            'id' => $cacheName . '_' . $media->pk,
                            'pk' => $media->pk,
                            'sourceid' => $media->id,
                            'typeid' => $media->media_type,
                            'caption' => $media->caption->text,
                            'likes' => $media->like_count,
                            'likespretty' => Number::format($media->like_count, ['locale' => 'id_ID']),
                            'comments' => $media->comment_count,
                            'commentspretty' => Number::format($media->comment_count, ['locale' => 'id_ID']),
                            'takenat' => $media->taken_at,
                            'takenatpretty' => $takenat->i18nFormat('dd MMMM yyyy HH:mm', null, 'id_ID'),
                            'username' => $media->user->username,
                            'fullname' => $media->user->full_name,
                            'locationid' => $locationId,
                            'location' => $locationName,
                            'wads' => $w,
                        ];
                        array_push($data['posts'], $d);
                    }
                }
                /*$fromCommands = explode('|', $posts);
                $relatedTags = explode(',', $fromCommands[1]);
                $postIds = explode(',', $fromCommands[0]);
                array_pop($postIds);
                array_unique($postIds);
                array_pop($relatedTags);
                $data['related'] = $relatedTags;
                $posts = [];
                $tempIds = [];
                foreach ($postIds as $p) {
                    $post = $this->Posts->find()
                        ->where(['Posts.id' => $p])
                        ->contain(['Members', 'Locations'])
                        ->first();
                        if (!in_array($post['pk'], $tempIds)) {
                            array_push($tempIds, $post['pk']);
                            $takenat = Time::createFromTimestamp($post['takenat']);
                            $takenat->timezone = 'Asia/Jakarta';
                            $d = [
                                'id' => $post['id'],
                                'pk' => $post['pk'],
                                'sourceid' => $post['sourceid'],
                                'typeid' => $post['typeid'],
                                'caption' => $post['caption'],
                                'likes' => $post['likes'],
                                'likespretty' => Number::format($post['likes'], ['locale' => 'id_ID']),
                                'comments' => $post['comments'],
                                'commentspretty' => Number::format($post['comments'], ['locale' => 'id_ID']),
                                'takenat' => $post['takenat'],
                                'takenatpretty' => $takenat->i18nFormat('dd MMMM yyyy HH:mm', null, 'id_ID'),
                                'username' => $post['member']['username'],
                                'fullname' => $post['member']['fullname'],
                                'locationid' => $post['location']['id'],
                                'location' => $post['location']['name'],
                            ];
                            $wads = $this->Wads->find()
                                ->where(['post_id' => $p, 'active' => true])
                                ->order(['sequence' => 'ASC'])
                                ->group('url')
                                ->all();
                            $w = [];
                            foreach ($wads as $wa) {
                                array_push($w, [
                                    'id' => $wa->id,
                                    'url' => $wa->url]);
                            }
                            $d['wads'] = $w;
                            array_push($posts, $d);
                    }// if not in temp array
                }// foreach postsids
                $data['posts'] = $posts;*/
            }
        }
        $this->autoRender = false;
        $response = $this->response->withType('application/json')->withStringBody(json_encode($data));
        return $response;
    }

    public function viewpopular($id = null) {
        if ($id != null) {
            $idArray = explode('_', $id);
            $cacheName = $idArray[0];
            $pk = $idArray[1];
            
            $retFile = $this->cache_get(CACHE . 'seeposts' . DS . $cacheName);
            if ($retFile != false) {
                $medias = json_decode($retFile['medias']);
                $replica = null;
                foreach ($medias as $media) {
                    if ($media->pk == $pk) {
                        $replica = $media;
                        break;
                    }
                }
                $media = $replica;
                $replica = [];

                $takenat = Time::createFromTimestamp($media->taken_at);
                $takenat->timezone = 'Asia/Jakarta';
                $replica['location_id'] = 1;
                $replica['location']['name'] = 'Tanpa Nama';
                if (isset($media->location)) {
                    $replica['location_id'] = $media->location->pk;
                    $replica['location']['name'] = $media->location->name;
                }

                $w = [];
                if ($media->media_type == 1) {// photo
                    array_push($w, ['id' => 0, 'url' => $media->image_versions2->candidates[0]->url]);
                } elseif ($media->media_type == 2) {// video
                    array_push($w, ['id' => 0, 'url' => $media->video_versions[0]->url]);
                } elseif ($media->media_type == 8) {// carousel
                    $m = $media->carousel_media[0];
                    foreach ($media->carousel_media as $m) {
                        if ($m->media_type == 1) {
                            array_push($w, ['id' => 0, 'url' => $m->image_versions2->candidates[0]->url]);
                        } elseif ($m->media_type == 2) {
                            array_push($w, ['id' => 0, 'url' => $m->video_versions[0]->url]);
                        }
                    }
                }
                $replica['wads'] = $w;
                $replica['caption'] = $media->caption->text;
                $replica['takenat'] = $media->taken_at;
                $replica['member']['username'] = $media->user->username;
                
                $this->set('replica', $replica);
            } else {
                return $this->redirect(['action' => 'seeposts']);
            }
        } else {
            return $this->redirect(['action' => 'seeposts']);
        }
    }

    public function repostpopular($id = null) {
        if ($id != null) {
            $idArray = explode('_', $id);
            $cacheName = $idArray[0];
            $pk = $idArray[1];
            
            $retFile = $this->cache_get(CACHE . 'seeposts' . DS . $cacheName);
            if ($retFile != false) {
                $medias = json_decode($retFile['medias']);
                $replica = null;
                foreach ($medias as $media) {
                    if ($media->pk == $pk) {
                        $replica = $media;
                        break;
                    }
                }
                $media = $replica;
                $replica = [];

                $takenat = Time::createFromTimestamp($media->taken_at);
                $takenat->timezone = 'Asia/Jakarta';
                $location_id = 1;
                $replica['location_id'] = 1;
                $replica['location']['name'] = 'Tanpa Nama';
                if (isset($media->location)) {
                    $location_id = $this->insertLocation($media->location);
                    //$replica['location_id'] = $media->location->pk;
                    $replica['location_id'] = $location_id;
                    $replica['location']['name'] = $media->location->name;
                }

                $w = [];
                if ($media->media_type == 1) {// photo
                    array_push($w, ['id' => 0, 'url' => $media->image_versions2->candidates[0]->url]);
                } elseif ($media->media_type == 2) {// video
                    array_push($w, ['id' => 0, 'url' => $media->video_versions[0]->url]);
                } elseif ($media->media_type == 8) {// carousel
                    $m = $media->carousel_media[0];
                    foreach ($media->carousel_media as $m) {
                        if ($m->media_type == 1) {
                            array_push($w, ['id' => 0, 'url' => $m->image_versions2->candidates[0]->url]);
                        } elseif ($m->media_type == 2) {
                            array_push($w, ['id' => 0, 'url' => $m->video_versions[0]->url]);
                        }
                    }
                }
                $replica['wads'] = $w;
                $replica['caption'] = $media->caption->text;
                $replica['takenat'] = $media->taken_at;
                $replica['member']['username'] = $media->user->username;
                $post = $replica;
                
                //$this->set('replica', $replica);
                
                $replica = $this->Replicas->newEntity();

                if ($this->request->is(['patch', 'post', 'put'])) {
                    // first insert cache to db
                    $item = $media;
                    // Insert Location
                    /*$location_id = 1;
                    if (isset($item->location)) {
                        $location_id = $this->insertLocation($item->location);
                    }*/

                    // Insert Member
                    $member_id = $this->insertMember($item->user);

                    // Insert Post
                    $post_id = $this->insertPost($item, $location_id, $member_id);

                    // Insert Wad
                    if ($post_id > 0 && $item->media_type == 1) {// Photo
                        $this->insertWad($item->image_versions2->candidates[0], $post_id, $item->media_type);
                    } elseif ($post_id > 0 && $item->media_type == 2) {// Video
                        $this->insertWad($item->video_versions[0], $post_id, $item->media_type);
                    } elseif ($post_id > 0 && $item->media_type == 8) {// Carousel
                        $this->insertWad($item->carousel_media, $post_id, $item->media_type);
                    }

                    $data = $this->request->getData();
                    $data['account_id'] = $this->accountOnSession;
                    $data['member_id'] = $member_id;
                    //$data['member_id'] = $post['member']['id'];
                    $data['post_id'] = $post_id;
                    $data['typeid'] = $item->media_type;
                    $data['takenat'] = $item->taken_at;
                    $data['uploaded'] = false;
                    $data['active'] = true;

                    if (strlen($data['schedule']) < 17) $data['schedule'] = $data['schedule'] . ':00';

                    if(!empty($data['schedule'])) {
                        $data['schedule'] = Time::createFromFormat('Y-m-d H:i:s', $data['schedule'], 'Asia/Jakarta')->i18nFormat('yyyy-MM-dd HH:mm:ss');
                    }

                    //print_r($data);
                    $replica = $this->Replicas->patchEntity($replica, $data);
                    if ($this->Replicas->save($replica)) {
                        $this->Flash->success(__('Konten berhasil diubah.'));

                        return $this->redirect(['action' => 'seeposts']);
                    }
                    $this->Flash->error(__('Ups, terjadi kesalahan. Silahkan mengulangi.'));
                }

                $this->set('replica', $post);

            } else {
                return $this->redirect(['action' => 'seeposts']);
            }

            /*$post = $this->Posts->get($id, [
                'contain' => ['Members', 'Wads', 'Locations']
            ]);*/

        } else {
            return $this->redirect(['action' => 'seeposts']);
        }
    }
    
    private function insertWad($datum = null, $post_id = 1, $typeid = 1) {
        $newData = [
            'post_id' => $post_id,
            'typeid' => $typeid,
            'sequence' => 0
        ];

        if ($typeid == 1 || $typeid == 2) {// Single photo or video
            $newData['url'] = $datum->url;
            $newData['width'] = $datum->width;
            $newData['height'] = $datum->height;
            $newData['urlfixed'] = false;
            $newData['active'] = true;
            
            //$conn = ConnectionManager::get('default');
            //$stmt = $conn->execute('SELECT id, count(id) AS idcount FROM wads WHERE post_id = ? AND url = ? AND active = 1', [$post_id, $datum->getUrl()]);
            //$check = $stmt->fetch('assoc');

            $check = $this->Wads->find()
                ->select(['id'])
                ->where(['post_id' => $post_id, 'url' => $datum->url, 'active' => true]);
            if ($check->count() < 1) {
            //if ($check['idcount'] < 1) {
                $data = $this->Wads->newEntity();
                $data = $this->Wads->patchEntity($data, $newData);
                if ($this->Wads->save($data)) {
                    return $data->id;
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        } elseif ($typeid == 8) {// Carousel
            $sequence = 0;
            foreach ($datum as $d) {
                $newData['url'] = '';
                $newData['width'] = 0;
                $newData['height'] = 0;
                $newData['typeid'] = $d->media_type;
                if ($d->media_type == 1) {// Photo
                    $reap = $d->image_versions2->candidates[0];
                } elseif ($d->media_type == 2) {// Video
                    $reap = $d->video_versions[0];
                }
                $newData['url'] = $reap->url;
                $newData['width'] = $reap->width;
                $newData['height'] = $reap->height;
                $newData['sequence'] = $sequence;
                $newData['urlfixed'] = false;
                $newData['active'] = true;
                $sequence++;
                $check = $this->Wads->find()
                    ->select(['id'])
                    ->where(['post_id' => $post_id, 'url' => $reap->url, 'active' => true]);
                if ($check->count() < 1) {
                /*j$conn = ConnectionManager::get('default');
                $stmt = $conn->execute('SELECT id, count(id) AS idcount FROM wads WHERE post_id = ? AND url = ? AND active = 1', [$post_id, $reap->getUrl()]);
                $check = $stmt->fetch('assoc');
                if ($check['idcount'] < 1) {*/
                    $data = $this->Wads->newEntity();
                    $data = $this->Wads->patchEntity($data, $newData);
                    if ($this->Wads->save($data)) {
                        return $data->id;
                    } else {
                        return 1;
                    }
                } else {
                    return 1;
                }
            }// foreach datum
        }// if type id single or carousel
    }// insert wads function

    private function insertPost($datum = null, $location_id = 1, $member_id = 1) {
        /*$conn = ConnectionManager::get('default');
        $stmt = $conn->execute('SELECT id, count(id) AS idcount FROM posts WHERE pk = ? AND active = 1', [$datum->getPk()]);
        $check = $stmt->fetch('assoc');
        //print_r($row);*/
        $check = $this->Posts->find()
            ->select(['id'])
            ->where(['pk' => $datum->pk, 'active' => true]);

        if ($check->count() > 0) {
            $data = $check->first();
            return $data->id;
        } else {
            (isset($datum->caption) && $datum->caption !== null) ? $caption = $datum->caption->text : $caption = '';
            (isset($datum->comment_count) && $datum->comment_count !== null) ? $comment = $datum->comment_count : $comment = 0;

            $post = $this->Posts->newEntity();
            $post->pk = $datum->pk;
            $post->sourceid = $datum->id;
            $post->location_id = $location_id;
            $post->member_id = $member_id;
            $post->typeid = $datum->media_type;
            $post->caption = $caption;
            $post->likes = $datum->like_count;
            $post->comments = $comment;
            $post->takenat = $datum->taken_at;
            $post->active = true;
            if ($this->Posts->save($post)) {
                return $post->id;
            } else {
                return 0;
            }
        }
    }

    private function insertMember($datum = null) {
        $check = $this->Members->find()
            ->select(['id'])
            ->where(['pk' => $datum->pk, 'active' => true]);

        if ($check->count() > 0) {
            $data = $check->first();
            return $data->id;
        } else {
            $account = $this->Members->newEntity();
            $account->pk = $datum->pk;
            $account->username = $datum->username;
            $account->fullname = $datum->full_name;
            $account->description = '';
            $account->profpicurl = $datum->profile_pic_url;
            $account->followers = 0;
            $account->followings = 0;
            $account->contents = 0;
            $account->closed = $datum->is_private;
            $account->profpicurlfixed = true;
            $account->genderdetection = false;
            $account->genderdetected = false;
            $account->active = true;
            if ($this->Members->save($account)) {
                return $account->id;
            } else {
                return 1;
            }
        }
    }

    private function insertLocation($datum = null) {
        $check = $this->Locations->find()
            ->select(['id'])
            ->where(['pk' => $datum->pk, 'active' => true]);

        if ($check->count() > 0) {
            $data = $check->first();
            return $data->id;
        } else {
            $location = $this->Locations->newEntity();
            $location->pk = $datum->pk;
            $location->fbplacesid = $datum->facebook_places_id;
            $location->lat = $datum->lat;
            $location->lng = $datum->lng;
            $location->address = $datum->address;
            $location->name = $datum->name;
            $location->shortname = $datum->short_name;
            $location->active = true;
            if ($this->Locations->save($location)) {
                return $location->id;
            } else {
                return 1;
            }
        }
    }

    public function search($hashtag = null) {

    }

    public function searchdig($hashtag = null) {
        $account_id = $this->accountOnSession;
        $query = trim($hashtag);
        $query = str_replace('#', '', $query);
        $query = str_replace(' ', '', $query);
        $data = [];

        if (!empty($query)) {
            $hashtags = shell_exec(ROOT . DS . 'bin/cake searchhashtag ' . $account_id . ' ' . $query);
            if (!empty($hashtags)) {
                $fromCommands = explode(',', $hashtags);
                array_pop($fromCommands);
                foreach ($fromCommands as $f) {
                    $datum = explode('|', $f);
                    //$data[$datum[1]] = $datum[0];
                    array_push($data, ['hashtag' => $datum[0], 'count' => $datum[1], 'countpretty' => Number::format($datum[1], ['locale' => 'id_ID'])]);
                }
                //krsort($data);
                //$datum = [];
                //foreach ($data as $key=>$value) {
                    //$datum[Number::format($key, ['locale' => 'id_ID'])] = $value;
                //}
                //foreach
                //Number::format($post['comments'], ['locale' => 'id_ID']),
                //$data = $datum;
            }
        }
        $this->autoRender = false;
        $response = $this->response->withType('application/json')->withStringBody(json_encode($data));
        return $response;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $hashtags = $this->paginate($this->Hashtags);

        $this->set(compact('hashtags'));
    }

    /**
     * View method
     *
     * @param string|null $id Hashtag id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $hashtag = $this->Hashtags->get($id, [
            'contain' => []
        ]);

        $this->set('hashtag', $hashtag);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $hashtag = $this->Hashtags->newEntity();
        if ($this->request->is('post')) {
            $hashtag = $this->Hashtags->patchEntity($hashtag, $this->request->getData());
            if ($this->Hashtags->save($hashtag)) {
                $this->Flash->success(__('The hashtag has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The hashtag could not be saved. Please, try again.'));
        }
        $this->set(compact('hashtag'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Hashtag id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $hashtag = $this->Hashtags->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $hashtag = $this->Hashtags->patchEntity($hashtag, $this->request->getData());
            if ($this->Hashtags->save($hashtag)) {
                $this->Flash->success(__('The hashtag has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The hashtag could not be saved. Please, try again.'));
        }
        $this->set(compact('hashtag'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Hashtag id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $hashtag = $this->Hashtags->get($id);
        if ($this->Hashtags->delete($hashtag)) {
            $this->Flash->success(__('The hashtag has been deleted.'));
        } else {
            $this->Flash->error(__('The hashtag could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
