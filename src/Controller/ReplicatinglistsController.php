<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;

/**
 * Replicatinglists Controller
 *
 * @property \App\Model\Table\ReplicatinglistsTable $Replicatinglists
 *
 * @method \App\Model\Entity\Replicatinglist[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReplicatinglistsController extends AppController
{
    public $user;
    public $session;
    public $accountOnSession;
    public $paidAccounts;
    public $orAccounts;
    public $paidAccountIds;

    public function initialize()
    {
        parent::initialize();
        $this->user = $this->Auth->user();
        $this->paidAccounts = $this->Replicatinglists->Accounts->find()
            ->where(['user_id' => $this->user['id'], 'active' => 1, 'statusid' => 5])
            ->all();

        // Only show paid (5) account(s)
        $orAccounts = [];
        $paidAccountIds = [];

        foreach ($this->paidAccounts as $account) {
            array_push($orAccounts, ['account_id' => $account['id']]);
            array_push($paidAccountIds, $account['id']);
        }
        $this->orAccounts = $orAccounts;
        $this->paidAccountIds = $paidAccountIds;
    }

    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');

        // All actions require an id
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }

        // Check that the cargo belongs to the current user.
        $data = $this->Replicatinglists->findById($id)->first();

        if (in_array($data->account_id, $this->paidAccountIds)) {
            return true;
        } else {
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->session = $this->request->getSession();
        $session = $this->request->getSession();
        $this->accountOnSession = $session->read('Config.account');
    }

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $this->set('user', $this->user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if ($this->accountOnSession > 0) {
            $replicatinglists = $this->Replicatinglists->find()
                ->where(['Replicatinglists.active' => 1, 'Replicatinglists.account_id' => $this->accountOnSession])
                ->contain(['members'])
                ->all();
        } else {
            $cargos = [];
        }
        $this->set(compact('replicatinglists'));
    }
    public function indexold($id = null)
    {
        $orAccounts = $this->orAccounts;
        // If id specified
        if ($id > 0 && in_array($id, $this->paidAccountIds)) {
            $orAccounts = ['account_id' => $id];
        }

        if (count($orAccounts) > 0) {
            $replicatinglists = $this->Replicatinglists->find()
                ->where(['Replicatinglists.active' => 1, 'OR' => $orAccounts])
                ->contain(['members'])
                ->all();
        } else {
            $cargos = [];
        }
        $accounts = $this->Replicatinglists->Accounts->find('list', [
            'keyField' => 'id',
            'valueField' => 'username',
        ])
            ->where(['user_id' => $this->user['id'], 'active' => 1, 'statusid' => 5])
            ->order(['username' => 'ASC'])
            ->toArray();
        $accounts[0] = 'Semua';

        $this->set(compact('replicatinglists', 'accounts'));
    }

    /**
     * View method
     *
     * @param string|null $id Replicatinglist id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if (!$this->isAuthorized($this->user)) {
            $this->redirect('index');
        }

        $account = $this->Replicatinglists->get($id, [
            'contain' => ['Members'],
        ]);

        $reposted = $this->Replicatinglists->Accounts->Replicas->find()
            ->where([
                'account_id' => $account['account_id'],
                'member_id' => $account['member_id'],
                'uploaded' => true,
                'active' => true,
            ])
            ->count();

        $unreposted = $this->Replicatinglists->Accounts->Replicas->find()
            ->where([
                'account_id' => $account['account_id'],
                'member_id' => $account['member_id'],
                'uploaded' => false,
                'active' => true,
            ])
            ->count();

            $uploadat = new Time($account['uploadat'], 'Asia/Jakarta');
            $uploadat = $uploadat->i18nFormat('HH:mm:ss');

        // Check if profile picture is exists
        $this->set(compact('account', 'unreposted', 'reposted', 'uploadat'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $replicatinglist = $this->Replicatinglists->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['account_id'] = $this->accountOnSession;
            $data['member_id'] = 1;
            $data['memberfixed'] = false;
            $data['contents'] = 0;
            $data['loads'] = 0;
            $data['replicated'] = false;
            $data['uploadat'] = Time::createFromFormat('H:i:s', $data['uploadat'], 'Asia/Jakarta')->i18nFormat('HH:mm:ss');
            $data['active'] = true;

            $replicatinglist = $this->Replicatinglists->patchEntity($replicatinglist, $data);
            if ($this->Replicatinglists->save($replicatinglist)) {
                $this->Flash->success(__('Akun berhasil ditambahkan.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Ups, terjadi kesalahan. Silahkan mengulangi.'));
        }

        if ($this->accountOnSession > 0) {
            $this->set(compact('replicatinglist'));
        } else {
            $this->redirect(['action' => 'index']);
        }
    }
    public function addold()
    {
        $replicatinglist = $this->Replicatinglists->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['member_id'] = 1;
            $data['memberfixed'] = false;
            $data['contents'] = 0;
            $data['loads'] = 0;
            $data['replicated'] = false;
            $data['uploadat'] = Time::createFromFormat('H:i:s', $data['uploadat'], 'Asia/Jakarta')->i18nFormat('HH:mm:ss');
            $data['active'] = true;

            $replicatinglist = $this->Replicatinglists->patchEntity($replicatinglist, $data);
            if ($this->Replicatinglists->save($replicatinglist)) {
                $this->Flash->success(__('Akun berhasil ditambahkan.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Ups, terjadi kesalahan. Silahkan mengulangi.'));
        }
        $orAccounts = [];
        foreach ($this->paidAccountIds as $id) {
            array_push($orAccounts, ['id' => $id]);
        }

        if (count($orAccounts) > 0) {
            $accounts = $this->Replicatinglists->Accounts->find('list', [
                'keyField' => 'id',
                'valueField' => 'username',
            ])
                ->where(['OR' => $orAccounts])
                ->order(['username' => 'ASC'])
                ->toArray();

            $this->set(compact('replicatinglist', 'accounts'));
        } else {
            $this->redirect(['action' => 'index']);
        }
    }


    /**
     * Edit method
     *
     * @param string|null $id Replicatinglist id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if (!$this->isAuthorized($this->user)) {
            $this->redirect('index');
        }
        
        $replicatinglist = $this->Replicatinglists->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $uploadat = new Time($data['uploadat']);
            $data['uploadat'] = $uploadat->i18nFormat('HH:mm:ss');
            //$data['uploadat'] = Time::createFromFormat('H:i:s', $data['uploadat'], 'Asia/Jakarta')->i18nFormat('HH:mm:ss');
            $replicatinglist = $this->Replicatinglists->patchEntity($replicatinglist, $data);
            if ($this->Replicatinglists->save($replicatinglist)) {
                $this->Flash->success(__('The replicatinglist has been saved.'));

                //return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The replicatinglist could not be saved. Please, try again.'));
            }
            //print_r($replicatinglist);
        }
        $orAccounts = [];
        foreach ($this->paidAccountIds as $id) {
            array_push($orAccounts, ['id' => $id]);
        }

        if (count($orAccounts) > 0) {
            $accounts = $this->Replicatinglists->Accounts->find('list', [
                'keyField' => 'id',
                'valueField' => 'username',
            ])
                ->where(['OR' => $orAccounts])
                ->order(['username' => 'ASC'])
                ->toArray();

            $uploadat = new Time($replicatinglist['uploadat'], 'Asia/Jakarta');
            $uploadat = $uploadat->i18nFormat('HH:mm:ss');

            $this->set(compact('replicatinglist', 'accounts', 'uploadat'));
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Replicatinglist id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if (!$this->isAuthorized($this->user)) {
            $this->redirect('index');
        }

        $account = $this->Replicatinglists->get($id, [
            'contain' => ['Members'],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = ['active' => false];
            $account = $this->Replicatinglists->patchEntity($account, $data);
            if ($this->Replicatinglists->save($account)) {
                // Update Replica Table
                $query = $this->Replicatinglists->Accounts->Replicas->query();
                $query->update()
                    ->set(['active' => false])
                    ->where(['account_id' => $account['account_id'], 'member_id' => $account['member_id']])
                    ->execute();

                $this->Flash->success(__('Berhasil menghapus akun.'));
            } else {
                $this->Flash->success(__('Ups! Gagal menghapus akun, silahkan mengulangi.'));
            }
            return $this->redirect(['action' => 'index']);
        }

        $this->set(compact('account'));
    }
}
