<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Compositions Controller
 *
 * @property \App\Model\Table\CompositionsTable $Compositions
 *
 * @method \App\Model\Entity\Composition[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CompositionsController extends AppController
{
    public $user;
    public $session;
    public $accountOnSession;
    
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->user = $this->Auth->user();
        $this->paidAccounts = $this->Compositions->Accounts->find()
            ->where(['user_id' => $this->user['id'], 'active' => 1, 'statusid' => 5])
            ->all();

        // Only show paid (5) account(s)
        $orAccounts = [];
        $paidAccountIds = [];

        foreach ($this->paidAccounts as $account) {
            array_push($orAccounts, ['account_id' => $account['id']]);
            array_push($paidAccountIds, $account['id']);
        }
        $this->orAccounts = $orAccounts;
        $this->paidAccountIds = $paidAccountIds;
    }

    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');

        // All actions require an id
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }

        // Check that the cargo belongs to the current user.
        $data = $this->Compositions->findById($id)->first();

        if (in_array($data->account_id, $this->paidAccountIds)) {
            return true;
        } else {
            return false;
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->session = $this->request->getSession();
        $session = $this->request->getSession();
        $this->accountOnSession = $session->read('Config.account');
    }

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $this->set('user', $this->user);
    }

    public function comments()
    {
        if ($this->accountOnSession > 0) {
            $query = $this->Compositions->find()
                           ->where(['Compositions.typeid' => 1, 'Compositions.active' => 1, 'Compositions.account_id' => $this->accountOnSession])
                           ->order(['caption' => 'ASC']);
        } else {
            //$cargos = [];
            $query = [];
        }
        $this->set('data', $this->paginate($query));
    }
    
    public function dms()
    {
        if ($this->accountOnSession > 0) {
            $query = $this->Compositions->find()
                           ->where(['Compositions.typeid' => 2, 'Compositions.active' => 1, 'Compositions.account_id' => $this->accountOnSession])
                           ->order(['caption' => 'ASC']);
        } else {
            //$cargos = [];
            $query = [];
        }
        $this->set('data', $this->paginate($query));
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if ($this->accountOnSession > 0) {
            $query = $this->Compositions->find()
                           ->where(['Compositions.active' => 1, 'Compositions.account_id' => $this->accountOnSession])
                           ->order(['caption' => 'ASC']);
        } else {
            //$cargos = [];
            $query = [];
        }
        $this->set('data', $this->paginate($query));
    }

    /**
     * View method
     *
     * @param string|null $id Composition id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $composition = $this->Compositions->get($id, [
            'contain' => ['Accounts']
        ]);

        $this->set('composition', $composition);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addcomment()
    {
        $composition = $this->Compositions->newEntity();
        if ($this->request->is('post') && !empty($this->request->getData()['caption'])) {
            $data = $this->request->getData();
            $data['caption'] = trim($data['caption']);
            $data['account_id'] = $this->accountOnSession;
            $data['typeid'] = 1;
            $data['active'] = true;
            
            // Insert Hashtaglist
            if ($this->Compositions->exists(['account_id' => $data['account_id'], 'caption' => $data['caption'], 'typeid' => 1, 'active' => true]) == false) {
                $datum = [
                    'account_id' => $data['account_id'],
                    'caption' => $data['caption'],
                    'typeid' => 1,
                    'active' => true
                ];
                $composition = $this->Compositions->newEntity();
                $composition = $this->Compositions->patchEntity($composition, $datum);
                $res = $this->Compositions->save($composition);
            }

            $this->Flash->success(__('Berhasil menyimpan data.'));

            return $this->redirect(['action' => 'comments']);
        }
        $this->set(compact('composition'));
    }

    public function editcomment($id = null) {
        if (!$this->isAuthorized($this->user)) {
            $this->redirect('comments');
        }

        $composition = $this->Compositions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $composition = $this->Compositions->patchEntity($composition, $this->request->getData());
            if ($this->Compositions->save($composition)) {
                $this->Flash->success(__('Berhasil menyimpan data.'));

                return $this->redirect(['action' => 'comments']);
            }
            $this->Flash->error(__('The composition could not be saved. Please, try again.'));
        }
        $this->set(compact('composition'));
    }
     
    public function deletecomment($id = null) {
        if (!$this->isAuthorized($this->user)) {
            $this->redirect('comments');
        }

        $composition = $this->Compositions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = ['active' => false];
            $composition = $this->Compositions->patchEntity($composition, $data);
            if ($this->Compositions->save($composition)) {
                $this->Flash->success(__('Berhasil menyimpan data.'));

                return $this->redirect(['action' => 'comments']);
            }
            $this->Flash->error(__('The composition could not be saved. Please, try again.'));
        }
        $this->set(compact('composition'));
    }
    
    public function adddm()
    {
        $composition = $this->Compositions->newEntity();
        if ($this->request->is('post') && !empty($this->request->getData()['caption'])) {
            $data = $this->request->getData();
            $data['caption'] = trim($data['caption']);
            $data['account_id'] = $this->accountOnSession;
            $data['typeid'] = 2;
            $data['active'] = true;
            
            // Insert Hashtaglist
            if ($this->Compositions->exists(['account_id' => $data['account_id'], 'caption' => $data['caption'], 'typeid' => 2, 'active' => true]) == false) {
                $datum = [
                    'account_id' => $data['account_id'],
                    'caption' => $data['caption'],
                    'typeid' => 2,
                    'active' => true
                ];
                $composition = $this->Compositions->newEntity();
                $composition = $this->Compositions->patchEntity($composition, $datum);
                $res = $this->Compositions->save($composition);
            }

            $this->Flash->success(__('Berhasil menyimpan data.'));

            return $this->redirect(['action' => 'dms']);
        }
        $this->set(compact('composition'));
    }

    public function editdm($id = null) {
        if (!$this->isAuthorized($this->user)) {
            $this->redirect('comments');
        }

        $composition = $this->Compositions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $composition = $this->Compositions->patchEntity($composition, $this->request->getData());
            if ($this->Compositions->save($composition)) {
                $this->Flash->success(__('Berhasil menyimpan data.'));

                return $this->redirect(['action' => 'dms']);
            }
            $this->Flash->error(__('The composition could not be saved. Please, try again.'));
        }
        $this->set(compact('composition'));
    }
     
    public function deletedm($id = null) {
        if (!$this->isAuthorized($this->user)) {
            $this->redirect('dms');
        }

        $composition = $this->Compositions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = ['active' => false];
            $composition = $this->Compositions->patchEntity($composition, $data);
            if ($this->Compositions->save($composition)) {
                $this->Flash->success(__('Berhasil menyimpan data.'));

                return $this->redirect(['action' => 'dms']);
            }
            $this->Flash->error(__('The composition could not be saved. Please, try again.'));
        }
        $this->set(compact('composition'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Composition id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $composition = $this->Compositions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $composition = $this->Compositions->patchEntity($composition, $this->request->getData());
            if ($this->Compositions->save($composition)) {
                $this->Flash->success(__('The composition has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The composition could not be saved. Please, try again.'));
        }
        $accounts = $this->Compositions->Accounts->find('list', ['limit' => 200]);
        $this->set(compact('composition', 'accounts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Composition id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $composition = $this->Compositions->get($id);
        if ($this->Compositions->delete($composition)) {
            $this->Flash->success(__('The composition has been deleted.'));
        } else {
            $this->Flash->error(__('The composition could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
